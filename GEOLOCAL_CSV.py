# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:57:01 2021

@author: masn2601
"""

import pandas as pd
from uszipcode import SearchEngine
from geopy.distance import geodesic
import time
from math import asin, cos, radians, sin, sqrt
import pyodbc
import sqlalchemy

SERVER = ''
Database = 'Market_Planning'

engine = sqlalchemy.create_engine('mssql+pyodbc://'+SERVER+'/'+Database)





start_time = time.time()
search = SearchEngine(simple_zipcode=True)

TRAIL_PATH_SOURCE = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/Trails.csv'
TRAIL_PATH = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/SSV_Trails.csv'
CITY_PATH = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/biggest_city.csv'
DEALER_PATH_SOURCE = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/SSV_DEALERS.csv'
DEALER_PATH = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/DEALERS.csv'
COMPETITOR_PATH = 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/COMPETITORS.csv'

# def distance(pos1, pos2):
#     return geodesic(pos1, pos2).km

def distance(pos1, pos2):
    lat1, long1 = map(radians, pos1)
    lat2, long2 = map(radians, pos2)
    dist_lats = abs(lat2 - lat1) 
    dist_longs = abs(long2 - long1) 
    a = sin(dist_lats/2)**2 + cos(lat1) * cos(lat2) * sin(dist_longs/2)**2
    c = asin(sqrt(a)) * 2
    radius_earth = 6378 # the "Earth radius" R varies from 6356.752 km at the poles to 6378.137 km at the equator.
    return c * radius_earth

#Creates a database countaining the names of the biggest cities the state and the goeposition
def get_biggest_cities_csv(PATH, SEND_PATH):
    city_info = pd.read_csv(PATH)
    city_name = city_info['CITY_NAME']
    city_lat = []
    city_lon = []
    for row in range(len(city_name)):
        if city_name[row][-1] == ']':
            city_name[row] = city_name[row][:-3]
        city_lon.append('-'+city_info['GOELOC'][row][10:-2])
        city_lat.append(city_info['GOELOC'][row][:7])
    city_info2 = pd.DataFrame(columns = ['CITY_NAME', 'STATE', 'LAT', 'LON'])
    city_info2['CITY_NAME'] = city_info['CITY_NAME']
    city_info2['STATE'] = city_info['STATE_NAME']
    city_info2['LAT'] = city_lat
    city_info2['LON'] = city_lon
    city_info2.to_csv(SEND_PATH, index = False)
    city_info3 = pd.read_csv(SEND_PATH)                             #These lines are a patch for some weird unexplained occurence
    for row in range(len(city_info3)):                              #############################
        city_info3['STATE'][row] = city_info3['STATE'][row][1:]     #############################
    city_info3.to_csv(SEND_PATH, index = False)                     #############################
    return city_info3

#returns biggest 5 biggest cities by state
def get_biggest_cities_state(PATH, SEND_PATH):
    city_info = pd.read_csv(PATH)
    city_info2 = pd.DataFrame(columns = ['STATE', 'CITY_NAME', 'LAT', 'LON'])
    i = 0
    #print('================= Starting Cycle =================\n')
    for row in range(len(city_info)):
        #print('cycle '+ str(row))      
        for col in range(5):
            state = city_info.iloc[row][0]
            if city_info.iloc[row][col+1] != 'NONE':
                city = city_info.iloc[row][col+1]
                #print('\nState : '+state+'\tCity : '+city)
                latlon = search.by_city_and_state(city, state)
                info = {'STATE' : state, 'CITY_NAME' : city,'LAT' : latlon[0].lat, 'LON' : latlon[0].lng}
                city_info2 = city_info2.append(info, ignore_index = True)
                i +=1
            else:
                info = {'STATE' : state, 'CITY_NAME' : None,'LAT' : None, 'LON' : None}
                city_info2 = city_info2.append(info, ignore_index = True)
                i += 1
    city_info2 = city_info2.dropna()
    city_info2.to_csv(SEND_PATH, index = False)
    city_info3 = pd.read_csv(SEND_PATH)                             #These lines are a patch for some weird unexplained occurence
    for row in range(len(city_info3)):                              #############################
        city_info3['STATE'][row] = city_info3['STATE'][row][1:]     #############################
    city_info3.to_csv(SEND_PATH, index = False)                     #############################
    return city_info2

def city_filter(PATH1, PATH2, SEND_PATH, overwrite = False):
    city_info1 = pd.read_csv(PATH1)
    city_info2 = pd.read_csv(PATH2)
    city_info3 = city_info1
    for row1 in range(len(city_info2)):
        trig = 0
        for row2 in range(len(city_info1)):
            if city_info1['CITY_NAME'][row2].upper() == city_info2['CITY_NAME'][row1].upper():
                trig = 1
        if trig == 0:
            city_info3 = city_info3.append(city_info2.iloc[row1])
    if overwrite == True:
        city_info3.to_csv(SEND_PATH, index = False)
    return city_info3

def dealer_filter(PATH, SEND_PATH_D = '', SEND_PATH_C = '', overwrite = False):
    dealer_info = pd.read_csv(PATH, sep=';')
    dealer_info2 = pd.DataFrame(columns = [''])
    competitor_info = pd.DataFrame(columns = [''])

    for row in range(len(dealer_info)):
        dealer_info_T = {'UNIVERSAL_ID' : '', 'CUSTOMER_NUMBER' : '', 'POSTAL' : '',
                     'LAT' : '', 'LONG' : '', 'COMPETITOR' : '', 'STATE' : ''}
        if dealer_info['PRODUCT_LINE'][row] == 'SSV':
            dealer_info_T['UNIVERSAL_ID'] = dealer_info['Universal_ID'][row]
            if dealer_info['OEM'][row] != 'BRP':
                dealer_info_T['COMPETITOR'] = dealer_info['OEM'][row]
                dealer_info_T['POSTAL'] = dealer_info['Zip'][row]
                dealer_info_T['STATE'] = dealer_info['State_Code'][row]
                dealer_info_T['LAT'] = dealer_info['Latitude'][row].replace(',','.')
                dealer_info_T['LONG'] = dealer_info['Longitude'][row].replace(',','.')
                
                competitor_info = competitor_info.append(dealer_info_T, ignore_index = True)
            else:
                dealer_info_T['CUSTOMER_NUMBER'] = dealer_info['CUSTOMER_NUMBER'][row]   
                dealer_info_T['POSTAL'] = dealer_info['Zip'][row]
                dealer_info_T['STATE'] = dealer_info['State_Code'][row]
                dealer_info_T['LAT'] = dealer_info['Latitude'][row].replace(',','.')
                dealer_info_T['LONG'] = dealer_info['Longitude'][row].replace(',','.')
                
                dealer_info2 = dealer_info2.append(dealer_info_T, ignore_index = True)
            
    dealer_info2 = dealer_info2.drop(dealer_info2.columns[0], axis = 1)
    dealer_info2 = dealer_info2.drop(dealer_info2.columns[0], axis = 1)
    competitor_info = competitor_info.drop(competitor_info.columns[0], axis = 1)
    competitor_info = competitor_info.drop(competitor_info.columns[1], axis = 1)
    if overwrite == True:
        dealer_info2.to_csv(SEND_PATH_D, index = False)
        competitor_info.to_csv(SEND_PATH_C, index = False)
    return dealer_info2, competitor_info

#This function converts the trail info from a pdf into proper format for 
def trail_filter(PATH, SEND_PATH = '', overwrite = False):
    trail_info = pd.read_csv(PATH, ';')
    trail_info2 = pd.DataFrame(columns = [''])
    trail_info_T = {'NAME' : '', 'STATE' : '', 'ZIPCODE' : '', 'LAT' : '', 'LONG' : ''}
    for row in range(len(trail_info)):
        trail_info_T['NAME'] = trail_info['NAME'][row]
        trail_info_T['STATE'] = trail_info['STATE'][row]
        trail_info_T['ZIPCODE'] = trail_info['ZIP_CODE'][row]
        trail_info2 = trail_info2.append(trail_info_T, ignore_index = True)
        trail_info_T['LAT'] = '.'
        trail_info_T['LONG'] = '.'
    trail_info2 = trail_info2.drop(trail_info2.columns[0], axis=1)
    trail_info2 = trail_info2 = trail_info2.dropna()
    trail_info2 = trail_info2.reset_index(drop=True)
    for row in range(len(trail_info2)):
        try:
            zipcode = search.by_zipcode(int(trail_info2['ZIPCODE'][row]))
        except:
            zipcode.lat = 0
            zipcode.lng = 0
        if zipcode.lat == None:
            zipcode.lat = 0
            zipcode.lng = 0
        trail_info2['LAT'][row] = zipcode.lat
        trail_info2['LONG'][row] = zipcode.lng
    if overwrite == True:
        trail_info2.to_csv(SEND_PATH, index = False)
    return trail_info2

def geolocal_script(PATH_OWNER, PATH_DEALER, PATH_COMPETITOR, PATH_TRAIL, PATH_CITY):
    location_data = pd.DataFrame(columns= [''])
    
    location_data_T = {'DATE' : '', 'OWNER_NUMBER' : '', 'MODEL' : '', 'OWNER_POSTAL' : '', 'OWNER_STATE' : '', 'OWNER_LAT' : '', 'OWNER_LONG' : '',
                       'DD1_NAME' : '', 'DD1_POSTAL' : '', 'DD1_STATE' : '', 'DD1_LAT' : '', 'DD1_LONG' : '', 'DD1_DISTANCE' : '',
                       'DD2_NAME' : '', 'DD2_POSTAL' : '', 'DD2_STATE' : '', 'DD2_LAT' : '', 'DD2_LONG' : '', 'DD2_DISTANCE' : '',
                       'DD3_NAME' : '', 'DD3_POSTAL' : '', 'DD3_STATE' : '', 'DD3_LAT' : '', 'DD3_LONG' : '', 'DD3_DISTANCE' : '',
                       'DD4_NAME' : '', 'DD4_POSTAL' : '', 'DD4_STATE' : '', 'DD4_LAT' : '', 'DD4_LONG' : '', 'DD4_DISTANCE' : '',
                       'DD5_NAME' : '', 'DD5_POSTAL' : '', 'DD5_STATE' : '', 'DD5_LAT' : '', 'DD5_LONG' : '', 'DD5_DISTANCE' : '', 'AVG_DISTANCE_D' : '',
                       'CD1_NAME' : '', 'CD1_POSTAL' : '', 'CD1_STATE' : '', 'CD1_LAT' : '', 'CD1_LONG' : '', 'CD1_DISTANCE' : '',
                       'CD2_NAME' : '', 'CD2_POSTAL' : '', 'CD2_STATE' : '', 'CD2_LAT' : '', 'CD2_LONG' : '', 'CD2_DISTANCE' : '',
                       'CD3_NAME' : '', 'CD3_POSTAL' : '', 'CD3_STATE' : '', 'CD3_LAT' : '', 'CD3_LONG' : '', 'CD3_DISTANCE' : '',
                       'CD4_NAME' : '', 'CD4_POSTAL' : '', 'CD4_STATE' : '', 'CD4_LAT' : '', 'CD4_LONG' : '', 'CD4_DISTANCE' : '',
                       'CD5_NAME' : '', 'CD5_POSTAL' : '', 'CD5_STATE' : '', 'CD5_LAT' : '', 'CD5_LONG' : '', 'CD5_DISTANCE' : '', 'AVG_DISTANCE_C' : '',
                       'TD1_NAME' : '', 'TD1_POSTAL' : '', 'TD1_STATE' : '', 'TD1_LAT' : '', 'TD1_LONG' : '', 'TD1_DISTANCE' : '',
                       'TD2_NAME' : '', 'TD2_POSTAL' : '', 'TD2_STATE' : '', 'TD2_LAT' : '', 'TD2_LONG' : '', 'TD2_DISTANCE' : '',
                       'TD3_NAME' : '', 'TD3_POSTAL' : '', 'TD3_STATE' : '', 'TD3_LAT' : '', 'TD3_LONG' : '', 'TD3_DISTANCE' : '', 'AVG_DISTANCE_T' : '',
                       'CLOSEST_URBAN_NAME' : '', 'CLOSEST_URBAN_STATE' : '', 'CLOSEST_URBAN_DISTANCE' : ''}
    
    
    #This section retrieves and fills the table with the owners info
    owner = pd.read_csv(PATH_OWNER, sep= ';')
    rows = 0
    perc = int(len(owner)/100)
    print('######################### Starting Owner Processing ######################### \n')
    while rows < len(owner):
        trig = True
        while trig == True:
            location_data_T['DATE'] = str(owner['Reg_Date_YYYYMMDD'][rows])[:4]+'-'+str(owner['Reg_Date_YYYYMMDD'][rows])[4:6]+'-'+str(owner['Reg_Date_YYYYMMDD'][rows])[6:]
            location_data_T['OWNER_NUMBER'] = str(owner['Owner_No'][rows])
            #location_data_T['MODEL'] = str(owner['Model_number'])
        
            if len(owner['Postal_Code'][rows]) > 6:
                location_data_T['OWNER_POSTAL'] = owner['Postal_Code'][rows][:-5]
            else:    
                location_data_T['OWNER_POSTAL'] = owner['Postal_Code'][rows]
            
            location_data_T['OWNER_STATE'] = owner['Region'][rows]
        
            try:
                zipcode = search.by_zipcode(location_data_T['OWNER_POSTAL'])
            except:
                zipcode.lat = None
                zipcode.lng = None
                
            if zipcode.lat == None:
                owner = owner.drop(rows, axis = 0)
                owner = owner.reset_index(drop=True)
                trig = True
                rows += 1
            else:
                trig = False
                location_data_T['OWNER_LAT'] = zipcode.lat
                location_data_T['OWNER_LONG'] = zipcode.lng
                location_data = location_data.append(location_data_T, ignore_index = True)
                    
        rows += 1
        if rows % perc == 0:
            print(str(rows/perc)+'%')  
        
    
    location_data = location_data.drop(location_data.columns[0], axis = 1)
    
    print('######################### Owner Processing Done ######################### \n\n######################### Starting distance calculations #########################\n')
    
    #This Section adds the 5 closest dealers and their respective info
    #setup initial variables
    
    ##########################################################################################
    ##########################################################################################
    dealers = pd.read_csv(PATH_DEALER)
    
    print('######################### DEALERS DISTANCES #########################\n')
    for row1 in range(len(location_data)):
        if row1 % perc == 0:
            print(str(row1/perc)+'%')
        location_data_T['DD1_NAME'] = dealers['CUSTOMER_NUMBER'][0]
        location_data_T['DD1_POSTAL'] = dealers['POSTAL'][0]
        location_data_T['DD1_STATE'] = dealers['STATE'][0]
        location_data_T['DD1_LAT'] = dealers['LAT'][0]
        location_data_T['DD1_LONG'] = dealers['LONG'][0]
        location_data_T['DD1_DISTANCE'] = 100000
    
        location_data_T['DD2_NAME'] = dealers['CUSTOMER_NUMBER'][1]
        location_data_T['DD2_POSTAL'] = dealers['POSTAL'][1]
        location_data_T['DD2_STATE'] = dealers['STATE'][1]
        location_data_T['DD2_LAT'] = dealers['LAT'][1]
        location_data_T['DD2_LONG'] = dealers['LONG'][1]
        location_data_T['DD2_DISTANCE'] = 100000
        
        location_data_T['DD3_NAME'] = dealers['CUSTOMER_NUMBER'][2]
        location_data_T['DD3_POSTAL'] = dealers['POSTAL'][2]
        location_data_T['DD3_STATE'] = dealers['STATE'][2]
        location_data_T['DD3_LAT'] = dealers['LAT'][2]
        location_data_T['DD3_LONG'] = dealers['LONG'][2]
        location_data_T['DD3_DISTANCE'] = 100000
        
        location_data_T['DD4_NAME'] = dealers['CUSTOMER_NUMBER'][3]
        location_data_T['DD4_POSTAL'] = dealers['POSTAL'][3]
        location_data_T['DD4_STATE'] = dealers['STATE'][3]
        location_data_T['DD4_LAT'] = dealers['LAT'][3]
        location_data_T['DD4_LONG'] = dealers['LONG'][3]
        location_data_T['DD4_DISTANCE'] = 100000
        
        location_data_T['DD5_NAME'] = dealers['CUSTOMER_NUMBER'][4]
        location_data_T['DD5_POSTAL'] = dealers['POSTAL'][4]
        location_data_T['DD5_STATE'] = dealers['STATE'][4]
        location_data_T['DD5_LAT'] = dealers['LAT'][4]
        location_data_T['DD5_LONG'] = dealers['LONG'][4]
        location_data_T['DD5_DISTANCE'] = 100000
        
        for row2 in range(len(dealers)):
            lat_T = dealers['LAT'][row2]
            long_T = dealers['LONG'][row2]
            dist = distance([location_data['OWNER_LAT'][row1],location_data['OWNER_LONG'][row1]],[lat_T, long_T])
            if dist < location_data_T['DD1_DISTANCE']:
                
                location_data_T['DD5_NAME'] = location_data_T['DD4_NAME'] 
                location_data_T['DD5_POSTAL'] = location_data_T['DD4_POSTAL']
                location_data_T['DD5_STATE'] = location_data_T['DD4_STATE']
                location_data_T['DD5_LAT'] = location_data_T['DD4_LAT']
                location_data_T['DD5_LONG'] = location_data_T['DD4_LONG']
                location_data_T['DD5_DISTANCE'] = location_data_T['DD4_DISTANCE']
                
                location_data_T['DD4_NAME'] = location_data_T['DD3_NAME'] 
                location_data_T['DD4_POSTAL'] = location_data_T['DD3_POSTAL']
                location_data_T['DD4_STATE'] = location_data_T['DD3_STATE']
                location_data_T['DD4_LAT'] = location_data_T['DD3_LAT']
                location_data_T['DD4_LONG'] = location_data_T['DD3_LONG']
                location_data_T['DD4_DISTANCE'] = location_data_T['DD3_DISTANCE']
                
                location_data_T['DD3_NAME'] = location_data_T['DD2_NAME'] 
                location_data_T['DD3_POSTAL'] = location_data_T['DD2_POSTAL']
                location_data_T['DD3_STATE'] = location_data_T['DD2_STATE']
                location_data_T['DD3_LAT'] = location_data_T['DD2_LAT']
                location_data_T['DD3_LONG'] = location_data_T['DD2_LONG']
                location_data_T['DD3_DISTANCE'] = location_data_T['DD2_DISTANCE']
                
                location_data_T['DD2_NAME'] = location_data_T['DD1_NAME'] 
                location_data_T['DD2_POSTAL'] = location_data_T['DD1_POSTAL']
                location_data_T['DD2_STATE'] = location_data_T['DD1_STATE']
                location_data_T['DD2_LAT'] = location_data_T['DD1_LAT']
                location_data_T['DD2_LONG'] = location_data_T['DD1_LONG']
                location_data_T['DD2_DISTANCE'] = location_data_T['DD1_DISTANCE']
                
                location_data_T['DD1_NAME'] = dealers['CUSTOMER_NUMBER'][row2]
                location_data_T['DD1_POSTAL'] = dealers['POSTAL'][row2]
                location_data_T['DD1_STATE'] = dealers['STATE'][row2]
                location_data_T['DD1_LAT'] = dealers['LAT'][row2]
                location_data_T['DD1_LONG'] = dealers['LONG'][row2]
                location_data_T['DD1_DISTANCE'] = dist
                
            elif dist < location_data_T['DD2_DISTANCE']:
                
                location_data_T['DD5_NAME'] = location_data_T['DD4_NAME'] 
                location_data_T['DD5_POSTAL'] = location_data_T['DD4_POSTAL']
                location_data_T['DD5_STATE'] = location_data_T['DD4_STATE']
                location_data_T['DD5_LAT'] = location_data_T['DD4_LAT']
                location_data_T['DD5_LONG'] = location_data_T['DD4_LONG']
                location_data_T['DD5_DISTANCE'] = location_data_T['DD4_DISTANCE']
                
                location_data_T['DD4_NAME'] = location_data_T['DD3_NAME'] 
                location_data_T['DD4_POSTAL'] = location_data_T['DD3_POSTAL']
                location_data_T['DD4_STATE'] = location_data_T['DD3_STATE']
                location_data_T['DD4_LAT'] = location_data_T['DD3_LAT']
                location_data_T['DD4_LONG'] = location_data_T['DD3_LONG']
                location_data_T['DD4_DISTANCE'] = location_data_T['DD3_DISTANCE']
                
                location_data_T['DD3_NAME'] = location_data_T['DD2_NAME'] 
                location_data_T['DD3_POSTAL'] = location_data_T['DD2_POSTAL']
                location_data_T['DD3_STATE'] = location_data_T['DD2_STATE']
                location_data_T['DD3_LAT'] = location_data_T['DD2_LAT']
                location_data_T['DD3_LONG'] = location_data_T['DD2_LONG']
                location_data_T['DD3_DISTANCE'] = location_data_T['DD2_DISTANCE']
                
                location_data_T['DD2_NAME'] = dealers['CUSTOMER_NUMBER'][row2]
                location_data_T['DD2_POSTAL'] = dealers['POSTAL'][row2]
                location_data_T['DD2_STATE'] = dealers['STATE'][row2]
                location_data_T['DD2_LAT'] = dealers['LAT'][row2]
                location_data_T['DD2_LONG'] = dealers['LONG'][row2]
                location_data_T['DD2_DISTANCE'] = dist
                
            elif dist < location_data_T['DD3_DISTANCE']:
                
                location_data_T['DD5_NAME'] = location_data_T['DD4_NAME'] 
                location_data_T['DD5_POSTAL'] = location_data_T['DD4_POSTAL']
                location_data_T['DD5_STATE'] = location_data_T['DD4_STATE']
                location_data_T['DD5_LAT'] = location_data_T['DD4_LAT']
                location_data_T['DD5_LONG'] = location_data_T['DD4_LONG']
                location_data_T['DD5_DISTANCE'] = location_data_T['DD4_DISTANCE']
                
                location_data_T['DD4_NAME'] = location_data_T['DD3_NAME'] 
                location_data_T['DD4_POSTAL'] = location_data_T['DD3_POSTAL']
                location_data_T['DD4_STATE'] = location_data_T['DD3_STATE']
                location_data_T['DD4_LAT'] = location_data_T['DD3_LAT']
                location_data_T['DD4_LONG'] = location_data_T['DD3_LONG']
                location_data_T['DD4_DISTANCE'] = location_data_T['DD3_DISTANCE']
                
                location_data_T['DD3_NAME'] = dealers['CUSTOMER_NUMBER'][row2]
                location_data_T['DD3_POSTAL'] = dealers['POSTAL'][row2]
                location_data_T['DD3_STATE'] = dealers['STATE'][row2]
                location_data_T['DD3_LAT'] = dealers['LAT'][row2]
                location_data_T['DD3_LONG'] = dealers['LONG'][row2]
                location_data_T['DD3_DISTANCE'] = dist
                
            elif dist < location_data_T['DD4_DISTANCE']:
                
                location_data_T['DD5_NAME'] = location_data_T['DD4_NAME'] 
                location_data_T['DD5_POSTAL'] = location_data_T['DD4_POSTAL']
                location_data_T['DD5_STATE'] = location_data_T['DD4_STATE']
                location_data_T['DD5_LAT'] = location_data_T['DD4_LAT']
                location_data_T['DD5_LONG'] = location_data_T['DD4_LONG']
                location_data_T['DD5_DISTANCE'] = location_data_T['DD4_DISTANCE']
                
                location_data_T['DD4_NAME'] = dealers['CUSTOMER_NUMBER'][row2]
                location_data_T['DD4_POSTAL'] = dealers['POSTAL'][row2]
                location_data_T['DD4_STATE'] = dealers['STATE'][row2]
                location_data_T['DD4_LAT'] = dealers['LAT'][row2]
                location_data_T['DD4_LONG'] = dealers['LONG'][row2]
                location_data_T['DD4_DISTANCE'] = dist
                
            elif dist < location_data_T['DD5_DISTANCE']:
                
                location_data_T['DD5_NAME'] = dealers['CUSTOMER_NUMBER'][row2]
                location_data_T['DD5_POSTAL'] = dealers['POSTAL'][row2]
                location_data_T['DD5_STATE'] = dealers['STATE'][row2]
                location_data_T['DD5_LAT'] = dealers['LAT'][row2]
                location_data_T['DD5_LONG'] = dealers['LONG'][row2]
                location_data_T['DD5_DISTANCE'] = dist
        
        
        location_data['DD5_NAME'][row1] = location_data_T['DD5_NAME'] 
        location_data['DD5_POSTAL'][row1] = location_data_T['DD5_POSTAL']
        location_data['DD5_STATE'][row1] = location_data_T['DD5_STATE']
        location_data['DD5_LAT'][row1] = location_data_T['DD5_LAT']
        location_data['DD5_LONG'][row1] = location_data_T['DD5_LONG']
        location_data['DD5_DISTANCE'][row1] = location_data_T['DD5_DISTANCE']
        
        location_data['DD4_NAME'][row1] = location_data_T['DD4_NAME'] 
        location_data['DD4_POSTAL'][row1] = location_data_T['DD4_POSTAL']
        location_data['DD4_STATE'][row1] = location_data_T['DD4_STATE']
        location_data['DD4_LAT'][row1] = location_data_T['DD4_LAT']
        location_data['DD4_LONG'][row1] = location_data_T['DD4_LONG']
        location_data['DD4_DISTANCE'][row1] = location_data_T['DD4_DISTANCE']
        
        location_data['DD3_NAME'][row1] = location_data_T['DD3_NAME'] 
        location_data['DD3_POSTAL'][row1] = location_data_T['DD3_POSTAL']
        location_data['DD3_STATE'][row1] = location_data_T['DD3_STATE']
        location_data['DD3_LAT'][row1] = location_data_T['DD3_LAT']
        location_data['DD3_LONG'][row1] = location_data_T['DD3_LONG']
        location_data['DD3_DISTANCE'][row1] = location_data_T['DD3_DISTANCE']
        
        location_data['DD2_NAME'][row1] = location_data_T['DD2_NAME'] 
        location_data['DD2_POSTAL'][row1] = location_data_T['DD2_POSTAL']
        location_data['DD2_STATE'][row1] = location_data_T['DD2_STATE']
        location_data['DD2_LAT'][row1] = location_data_T['DD2_LAT']
        location_data['DD2_LONG'][row1] = location_data_T['DD2_LONG']
        location_data['DD2_DISTANCE'][row1] = location_data_T['DD2_DISTANCE']
        
        location_data['DD1_NAME'][row1] = location_data_T['DD1_NAME'] 
        location_data['DD1_POSTAL'][row1] = location_data_T['DD1_POSTAL']
        location_data['DD1_STATE'][row1] = location_data_T['DD1_STATE']
        location_data['DD1_LAT'][row1] = location_data_T['DD1_LAT']
        location_data['DD1_LONG'][row1] = location_data_T['DD1_LONG']
        location_data['DD1_DISTANCE'][row1] = location_data_T['DD1_DISTANCE']
        
        location_data['AVG_DISTANCE_D'] = (location_data_T['DD5_DISTANCE']+location_data_T['DD4_DISTANCE']+location_data_T['DD3_DISTANCE']+location_data_T['DD2_DISTANCE']+location_data_T['DD1_DISTANCE'])/5
    ##################################################################################################
    ##################################################################################################
    
    #This Section adds the 5 closest competitors and their respective info
    
    ##################################################################################################
    ##################################################################################################

    competitors = pd.read_csv(PATH_COMPETITOR)
    print('\n######################### COMPETITOR DISTANCES #########################\n')
    
    for row1 in range(len(location_data)):
        if row1 % perc == 0:
            print(str(row1/perc)+'%')
        location_data_T['CD1_NAME'] = competitors['COMPETITOR'][0]
        location_data_T['CD1_POSTAL'] = competitors['POSTAL'][0]
        location_data_T['CD1_STATE'] = competitors['STATE'][0]
        location_data_T['CD1_LAT'] = competitors['LAT'][0]
        location_data_T['CD1_LONG'] = competitors['LONG'][0]
        location_data_T['CD1_DISTANCE'] = 100000
    
        location_data_T['CD2_NAME'] = competitors['COMPETITOR'][1]
        location_data_T['CD2_POSTAL'] = competitors['POSTAL'][1]
        location_data_T['CD2_STATE'] = competitors['STATE'][1]
        location_data_T['CD2_LAT'] = competitors['LAT'][1]
        location_data_T['CD2_LONG'] = competitors['LONG'][1]
        location_data_T['CD2_DISTANCE'] = 100000
        
        location_data_T['CD3_NAME'] = competitors['COMPETITOR'][2]
        location_data_T['CD3_POSTAL'] = competitors['POSTAL'][2]
        location_data_T['CD3_STATE'] = competitors['STATE'][2]
        location_data_T['CD3_LAT'] = competitors['LAT'][2]
        location_data_T['CD3_LONG'] = competitors['LONG'][2]
        location_data_T['CD3_DISTANCE'] = 100000
        
        location_data_T['CD4_NAME'] = competitors['COMPETITOR'][3]
        location_data_T['CD4_POSTAL'] = competitors['POSTAL'][3]
        location_data_T['CD4_STATE'] = competitors['STATE'][3]
        location_data_T['CD4_LAT'] = competitors['LAT'][3]
        location_data_T['CD4_LONG'] = competitors['LONG'][3]
        location_data_T['CD4_DISTANCE'] = 100000
        
        location_data_T['CD5_NAME'] = competitors['COMPETITOR'][4]
        location_data_T['CD5_POSTAL'] = competitors['POSTAL'][4]
        location_data_T['CD5_STATE'] = competitors['STATE'][4]
        location_data_T['CD5_LAT'] = competitors['LAT'][4]
        location_data_T['CD5_LONG'] = competitors['LONG'][4]
        location_data_T['CD5_DISTANCE'] = 100000
        
        for row2 in range(len(competitors)):
            lat_T = competitors['LAT'][row2]
            long_T = competitors['LONG'][row2]
            dist = distance([location_data['OWNER_LAT'][row1],location_data['OWNER_LONG'][row1]],[lat_T, long_T])
            if dist < location_data_T['CD1_DISTANCE']:
                
                location_data_T['CD5_NAME'] = location_data_T['CD4_NAME'] 
                location_data_T['CD5_POSTAL'] = location_data_T['CD4_POSTAL']
                location_data_T['CD5_STATE'] = location_data_T['CD4_STATE']
                location_data_T['CD5_LAT'] = location_data_T['CD4_LAT']
                location_data_T['CD5_LONG'] = location_data_T['CD4_LONG']
                location_data_T['CD5_DISTANCE'] = location_data_T['CD4_DISTANCE']
                
                location_data_T['CD4_NAME'] = location_data_T['CD3_NAME'] 
                location_data_T['CD4_POSTAL'] = location_data_T['CD3_POSTAL']
                location_data_T['CD4_STATE'] = location_data_T['CD3_STATE']
                location_data_T['CD4_LAT'] = location_data_T['CD3_LAT']
                location_data_T['CD4_LONG'] = location_data_T['CD3_LONG']
                location_data_T['CD4_DISTANCE'] = location_data_T['CD3_DISTANCE']
                
                location_data_T['CD3_NAME'] = location_data_T['CD2_NAME'] 
                location_data_T['CD3_POSTAL'] = location_data_T['CD2_POSTAL']
                location_data_T['CD3_STATE'] = location_data_T['CD2_STATE']
                location_data_T['CD3_LAT'] = location_data_T['CD2_LAT']
                location_data_T['CD3_LONG'] = location_data_T['CD2_LONG']
                location_data_T['CD3_DISTANCE'] = location_data_T['CD2_DISTANCE']
                
                location_data_T['CD2_NAME'] = location_data_T['CD1_NAME'] 
                location_data_T['CD2_POSTAL'] = location_data_T['CD1_POSTAL']
                location_data_T['CD2_STATE'] = location_data_T['CD1_STATE']
                location_data_T['CD2_LAT'] = location_data_T['CD1_LAT']
                location_data_T['CD2_LONG'] = location_data_T['CD1_LONG']
                location_data_T['CD2_DISTANCE'] = location_data_T['CD1_DISTANCE']
                
                location_data_T['CD1_NAME'] = competitors['COMPETITOR'][row2]
                location_data_T['CD1_POSTAL'] = competitors['POSTAL'][row2]
                location_data_T['CD1_STATE'] = competitors['STATE'][row2]
                location_data_T['CD1_LAT'] = competitors['LAT'][row2]
                location_data_T['CD1_LONG'] = competitors['LONG'][row2]
                location_data_T['CD1_DISTANCE'] = dist
                
            elif dist < location_data_T['CD2_DISTANCE']:
                
                location_data_T['CD5_NAME'] = location_data_T['CD4_NAME'] 
                location_data_T['CD5_POSTAL'] = location_data_T['CD4_POSTAL']
                location_data_T['CD5_STATE'] = location_data_T['CD4_STATE']
                location_data_T['CD5_LAT'] = location_data_T['CD4_LAT']
                location_data_T['CD5_LONG'] = location_data_T['CD4_LONG']
                location_data_T['CD5_DISTANCE'] = location_data_T['CD4_DISTANCE']
                
                location_data_T['CD4_NAME'] = location_data_T['CD3_NAME'] 
                location_data_T['CD4_POSTAL'] = location_data_T['CD3_POSTAL']
                location_data_T['CD4_STATE'] = location_data_T['CD3_STATE']
                location_data_T['CD4_LAT'] = location_data_T['CD3_LAT']
                location_data_T['CD4_LONG'] = location_data_T['CD3_LONG']
                location_data_T['CD4_DISTANCE'] = location_data_T['CD3_DISTANCE']
                
                location_data_T['CD3_NAME'] = location_data_T['CD2_NAME'] 
                location_data_T['CD3_POSTAL'] = location_data_T['CD2_POSTAL']
                location_data_T['CD3_STATE'] = location_data_T['CD2_STATE']
                location_data_T['CD3_LAT'] = location_data_T['CD2_LAT']
                location_data_T['CD3_LONG'] = location_data_T['CD2_LONG']
                location_data_T['CD3_DISTANCE'] = location_data_T['CD2_DISTANCE']
                
                location_data_T['CD2_NAME'] = competitors['COMPETITOR'][row2]
                location_data_T['CD2_POSTAL'] = competitors['POSTAL'][row2]
                location_data_T['CD2_STATE'] = competitors['STATE'][row2]
                location_data_T['CD2_LAT'] = competitors['LAT'][row2]
                location_data_T['CD2_LONG'] = competitors['LONG'][row2]
                location_data_T['CD2_DISTANCE'] = dist
                
            elif dist < location_data_T['CD3_DISTANCE']:
                
                location_data_T['CD5_NAME'] = location_data_T['CD4_NAME'] 
                location_data_T['CD5_POSTAL'] = location_data_T['CD4_POSTAL']
                location_data_T['CD5_STATE'] = location_data_T['CD4_STATE']
                location_data_T['CD5_LAT'] = location_data_T['CD4_LAT']
                location_data_T['CD5_LONG'] = location_data_T['CD4_LONG']
                location_data_T['CD5_DISTANCE'] = location_data_T['CD4_DISTANCE']
                
                location_data_T['CD4_NAME'] = location_data_T['CD3_NAME'] 
                location_data_T['CD4_POSTAL'] = location_data_T['CD3_POSTAL']
                location_data_T['CD4_STATE'] = location_data_T['CD3_STATE']
                location_data_T['CD4_LAT'] = location_data_T['CD3_LAT']
                location_data_T['CD4_LONG'] = location_data_T['CD3_LONG']
                location_data_T['CD4_DISTANCE'] = location_data_T['CD3_DISTANCE']
                
                location_data_T['CD3_NAME'] = competitors['COMPETITOR'][row2]
                location_data_T['CD3_POSTAL'] = competitors['POSTAL'][row2]
                location_data_T['CD3_STATE'] = competitors['STATE'][row2]
                location_data_T['CD3_LAT'] = competitors['LAT'][row2]
                location_data_T['CD3_LONG'] = competitors['LONG'][row2]
                location_data_T['CD3_DISTANCE'] = dist
                
            elif dist < location_data_T['CD4_DISTANCE']:
                
                location_data_T['CD5_NAME'] = location_data_T['CD4_NAME'] 
                location_data_T['CD5_POSTAL'] = location_data_T['CD4_POSTAL']
                location_data_T['CD5_STATE'] = location_data_T['CD4_STATE']
                location_data_T['CD5_LAT'] = location_data_T['CD4_LAT']
                location_data_T['CD5_LONG'] = location_data_T['CD4_LONG']
                location_data_T['CD5_DISTANCE'] = location_data_T['CD4_DISTANCE']
                
                location_data_T['CD4_NAME'] = competitors['COMPETITOR'][row2]
                location_data_T['CD4_POSTAL'] = competitors['POSTAL'][row2]
                location_data_T['CD4_STATE'] = competitors['STATE'][row2]
                location_data_T['CD4_LAT'] = competitors['LAT'][row2]
                location_data_T['CD4_LONG'] = competitors['LONG'][row2]
                location_data_T['CD4_DISTANCE'] = dist
                
            elif dist < location_data_T['CD4_DISTANCE']:
                
                location_data_T['CD5_NAME'] = competitors['COMPETITOR'][row2]
                location_data_T['CD5_POSTAL'] = competitors['POSTAL'][row2]
                location_data_T['CD5_STATE'] = competitors['STATE'][row2]
                location_data_T['CD5_LAT'] = competitors['LAT'][row2]
                location_data_T['CD5_LONG'] = competitors['LONG'][row2]
                location_data_T['CD5_DISTANCE'] = dist
        
        
        location_data['CD5_NAME'][row1] = location_data_T['CD5_NAME'] 
        location_data['CD5_POSTAL'][row1] = location_data_T['CD5_POSTAL']
        location_data['CD5_STATE'][row1] = location_data_T['CD5_STATE']
        location_data['CD5_LAT'][row1] = location_data_T['CD5_LAT']
        location_data['CD5_LONG'][row1] = location_data_T['CD5_LONG']
        location_data['CD5_DISTANCE'][row1] = location_data_T['CD5_DISTANCE']
        
        location_data['CD4_NAME'][row1] = location_data_T['CD4_NAME'] 
        location_data['CD4_POSTAL'][row1] = location_data_T['CD4_POSTAL']
        location_data['CD4_STATE'][row1] = location_data_T['CD4_STATE']
        location_data['CD4_LAT'][row1] = location_data_T['CD4_LAT']
        location_data['CD4_LONG'][row1] = location_data_T['CD4_LONG']
        location_data['CD4_DISTANCE'][row1] = location_data_T['CD4_DISTANCE']
        
        location_data['CD3_NAME'][row1] = location_data_T['CD3_NAME'] 
        location_data['CD3_POSTAL'][row1] = location_data_T['CD3_POSTAL']
        location_data['CD3_STATE'][row1] = location_data_T['CD3_STATE']
        location_data['CD3_LAT'][row1] = location_data_T['CD3_LAT']
        location_data['CD3_LONG'][row1] = location_data_T['CD3_LONG']
        location_data['CD3_DISTANCE'][row1] = location_data_T['CD3_DISTANCE']
        
        location_data['CD2_NAME'][row1] = location_data_T['CD2_NAME'] 
        location_data['CD2_POSTAL'][row1] = location_data_T['CD2_POSTAL']
        location_data['CD2_STATE'][row1] = location_data_T['CD2_STATE']
        location_data['CD2_LAT'][row1] = location_data_T['CD2_LAT']
        location_data['CD2_LONG'][row1] = location_data_T['CD2_LONG']
        location_data['CD2_DISTANCE'][row1] = location_data_T['CD2_DISTANCE']
        
        location_data['CD1_NAME'][row1] = location_data_T['CD1_NAME'] 
        location_data['CD1_POSTAL'][row1] = location_data_T['CD1_POSTAL']
        location_data['CD1_STATE'][row1] = location_data_T['CD1_STATE']
        location_data['CD1_LAT'][row1] = location_data_T['CD1_LAT']
        location_data['CD1_LONG'][row1] = location_data_T['CD1_LONG']
        location_data['CD1_DISTANCE'][row1] = location_data_T['CD1_DISTANCE']
        
        location_data['AVG_DISTANCE_C'] = (location_data_T['CD5_DISTANCE']+location_data_T['CD4_DISTANCE']+location_data_T['CD3_DISTANCE']+location_data_T['CD2_DISTANCE']+location_data_T['CD1_DISTANCE'])/5
        
    ################################################################################
    ################################################################################
    
    #This Section adds the 3 closest trails and their respective info
    
    ################################################################################
    ################################################################################
    
    trails = pd.read_csv(PATH_TRAIL)
    
    print('\n######################### TRAIL DISTANCES #########################\n')
    
    for row1 in range(len(location_data)):
        if row1 % perc == 0:
            print(str(row1/perc)+'%')
        location_data_T['TD1_NAME'] = trails['NAME'][0]
        location_data_T['TD1_POSTAL'] = trails['ZIPCODE'][0]
        location_data_T['TD1_STATE'] = trails['STATE'][0]
        location_data_T['TD1_LAT'] = trails['LAT'][0]
        location_data_T['TD1_LONG'] = trails['LONG'][0]
        location_data_T['TD1_DISTANCE'] = 100000
    
        location_data_T['TD2_NAME'] = trails['NAME'][1]
        location_data_T['TD2_POSTAL'] = trails['ZIPCODE'][1]
        location_data_T['TD2_STATE'] = trails['STATE'][1]
        location_data_T['TD2_LAT'] = trails['LAT'][1]
        location_data_T['TD2_LONG'] = trails['LONG'][1]
        location_data_T['TD2_DISTANCE'] = 100000
        
        location_data_T['TD3_NAME'] = trails['NAME'][2]
        location_data_T['TD3_POSTAL'] = trails['ZIPCODE'][2]
        location_data_T['TD3_STATE'] = trails['STATE'][2]
        location_data_T['TD3_LAT'] = trails['LAT'][2]
        location_data_T['TD3_LONG'] = trails['LONG'][2]
        location_data_T['TD3_DISTANCE'] = 100000
        
        for row2 in range(len(trails)):
            lat_T = trails['LAT'][row2]
            long_T = trails['LONG'][row2]
            dist = distance([location_data['OWNER_LAT'][row1],location_data['OWNER_LONG'][row1]],[lat_T, long_T])   
            if dist < location_data_T['TD1_DISTANCE']:
                
                location_data_T['TD3_NAME'] = location_data_T['TD2_NAME'] 
                location_data_T['TD3_POSTAL'] = location_data_T['TD2_POSTAL']
                location_data_T['TD3_STATE'] = location_data_T['TD2_STATE']
                location_data_T['TD3_LAT'] = location_data_T['TD2_LAT']
                location_data_T['TD3_LONG'] = location_data_T['TD2_LONG']
                location_data_T['TD3_DISTANCE'] = location_data_T['TD2_DISTANCE']
                
                location_data_T['TD2_NAME'] = location_data_T['TD1_NAME'] 
                location_data_T['TD2_POSTAL'] = location_data_T['TD1_POSTAL']
                location_data_T['TD2_STATE'] = location_data_T['TD1_STATE']
                location_data_T['TD2_LAT'] = location_data_T['TD1_LAT']
                location_data_T['TD2_LONG'] = location_data_T['TD1_LONG']
                location_data_T['TD2_DISTANCE'] = location_data_T['TD1_DISTANCE']
                
                location_data_T['TD1_NAME'] = trails['NAME'][row2]
                location_data_T['TD1_POSTAL'] = trails['ZIPCODE'][row2]
                location_data_T['TD1_STATE'] = trails['STATE'][row2]
                location_data_T['TD1_LAT'] = trails['LAT'][row2]
                location_data_T['TD1_LONG'] = trails['LONG'][row2]
                location_data_T['TD1_DISTANCE'] = dist
                
            elif dist < location_data_T['TD2_DISTANCE']:
                
                location_data_T['TD3_NAME'] = location_data_T['TD2_NAME'] 
                location_data_T['TD3_POSTAL'] = location_data_T['TD2_POSTAL']
                location_data_T['TD3_STATE'] = location_data_T['TD2_STATE']
                location_data_T['TD3_LAT'] = location_data_T['TD2_LAT']
                location_data_T['TD3_LONG'] = location_data_T['TD2_LONG']
                location_data_T['TD3_DISTANCE'] = location_data_T['TD2_DISTANCE']
                
                location_data_T['TD2_NAME'] = trails['NAME'][row2]
                location_data_T['TD2_POSTAL'] = trails['ZIPCODE'][row2]
                location_data_T['TD2_STATE'] = trails['STATE'][row2]
                location_data_T['TD2_LAT'] = trails['LAT'][row2]
                location_data_T['TD2_LONG'] = trails['LONG'][row2]
                location_data_T['TD2_DISTANCE'] = dist
                
            elif dist < location_data_T['TD3_DISTANCE']:
                
                location_data_T['TD3_NAME'] = trails['NAME'][row2]
                location_data_T['TD3_POSTAL'] = trails['ZIPCODE'][row2]
                location_data_T['TD3_STATE'] = trails['STATE'][row2]
                location_data_T['TD3_LAT'] = trails['LAT'][row2]
                location_data_T['TD3_LONG'] = trails['LONG'][row2]
                location_data_T['TD3_DISTANCE'] = dist
        
        
        location_data['TD3_NAME'][row1] = location_data_T['TD3_NAME'] 
        location_data['TD3_POSTAL'][row1] = location_data_T['TD3_POSTAL']
        location_data['TD3_STATE'][row1] = location_data_T['TD3_STATE']
        location_data['TD3_LAT'][row1] = location_data_T['TD3_LAT']
        location_data['TD3_LONG'][row1] = location_data_T['TD3_LONG']
        location_data['TD3_DISTANCE'][row1] = location_data_T['TD3_DISTANCE']
        
        location_data['TD2_NAME'][row1] = location_data_T['TD2_NAME'] 
        location_data['TD2_POSTAL'][row1] = location_data_T['TD2_POSTAL']
        location_data['TD2_STATE'][row1] = location_data_T['TD2_STATE']
        location_data['TD2_LAT'][row1] = location_data_T['TD2_LAT']
        location_data['TD2_LONG'][row1] = location_data_T['TD2_LONG']
        location_data['TD2_DISTANCE'][row1] = location_data_T['TD2_DISTANCE']
        
        location_data['TD1_NAME'][row1] = location_data_T['TD1_NAME'] 
        location_data['TD1_POSTAL'][row1] = location_data_T['TD1_POSTAL']
        location_data['TD1_STATE'][row1] = location_data_T['TD1_STATE']
        location_data['TD1_LAT'][row1] = location_data_T['TD1_LAT']
        location_data['TD1_LONG'][row1] = location_data_T['TD1_LONG']
        location_data['TD1_DISTANCE'][row1] = location_data_T['TD1_DISTANCE']
    
        location_data['AVG_DISTANCE_T'] = (location_data_T['TD3_DISTANCE']+location_data_T['TD2_DISTANCE']+location_data_T['TD1_DISTANCE'])/3
        
    ################################################################################
    ################################################################################
    
    #This Section adds the closest city and its respective info
    
    ################################################################################
    ################################################################################
        
    city = pd.read_csv(PATH_CITY)
    
    print('\n######################### CITY DISTANCES #########################\n')

    for row1 in range(len(location_data)):
        if row1 % perc == 0:
            print(str(row1/perc)+'%')
        location_data_T['CLOSEST_URBAN_NAME'] = city['CITY_NAME'][0]
        location_data_T['CLOSEST_URBAN_STATE'] = city['STATE'][0]
        location_data_T['CLOSEST_URBAN_DISTANCE'] = 100000
        
        for row2 in range(len(city)):
            lat_T = city['LAT'][row2]
            long_T = city['LON'][row2]
            dist = distance([location_data['OWNER_LAT'][row1],location_data['OWNER_LONG'][row1]],[lat_T, float(long_T)]) 
            
            if dist < location_data_T['CLOSEST_URBAN_DISTANCE']:
                 location_data_T['CLOSEST_URBAN_NAME'] = city['CITY_NAME'][row2]
                 location_data_T['CLOSEST_URBAN_STATE'] = city['STATE'][row2]
                 location_data_T['CLOSEST_URBAN_DISTANCE'] = dist
        
        location_data['CLOSEST_URBAN_NAME'][row1] = location_data_T['CLOSEST_URBAN_NAME']
        location_data['CLOSEST_URBAN_STATE'][row1] = location_data_T['CLOSEST_URBAN_STATE']
        location_data['CLOSEST_URBAN_DISTANCE'][row1] = location_data_T['CLOSEST_URBAN_DISTANCE']
    
    location_data.to_csv('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/GEOLOCAL_DATA.csv', index = False)
    
    return location_data

def sql_dump(PATH, sql_table):
    data = pd.read_csv(PATH)
    return
        
    

#city = get_biggest_cities_csv('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/Biggest cities and states.csv', 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/Biggest_cities_and_states.csv')
#city = get_biggest_cities_state('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/biggest city by state.csv', 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/biggest_city_by_state.csv')
#city = city_filter('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/Biggest_cities_and_states.csv', 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/biggest_city_by_state.csv', 'C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/biggest_city.csv')
#owners = pd.read_csv('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/Trails.csv', sep = ';')
#print(owners['Reg_Date_YYYYMMDD'][3])
#print(str(owners['Reg_Date_YYYYMMDD'][3])[:4]+'-'+str(owners['Reg_Date_YYYYMMDD'][3])[4:6]+'-'+str(owners['Reg_Date_YYYYMMDD'][3])[6:])
loc = geolocal_script('C:/Users/masn2601/Documents/BRP/brp_forecasrting_internship/SSV_OWNER.csv', DEALER_PATH, COMPETITOR_PATH, TRAIL_PATH, CITY_PATH)
#dealer_info , competitor_info= dealer_filter(DEALER_PATH_SOURCE, DEALER_PATH, COMPETITOR_PATH, True)
#trail = trail_filter(TRAIL_PATH_SOURCE, TRAIL_PATH, True)
# pos1 = [30.0664, -93.2001]
# pos2 = [44.7612, -69.3165]
# for i in range(100000):
#     distance(pos1, pos2)
#     #dist_between_two_lat_lon(pos1, pos2)
#city = pd.read_csv(CITY_PATH)
print("--- %s seconds ---" % (time.time() - start_time))

