# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 13:05:25 2021

@author: David Bernier
"""


"""
This script gathers all the data in a specified time frame from NOAA FTP server and insert it into
a SQL table named dbo.MKTPL_Z_METEOROLOGICAL. You can also specify the Databse where you want to send it.

Returned data:
    COUNTY_ID : county id
    W_DATE : date of the measure
    COUNTY_NAME : county name
    STATE_ID : state id
    ZONE_TYPE : 'cty' = county
    PRCP : Precipitations in mm
    TAVG : Average temperature in Celsius
    TMAX : Maximum temperature in Celsius
    TMIN : Minimum temperature in Celsius

NOTE : Be careful of the timeframe used, the data from NOAA are not always refreshed to today's date

"""


# Connect to the SQL server
import pyodbc as pc

# Connect to ftp



import ftplib
from io import BytesIO

# Dataframe manipulations
import pandas as pd
from datetime import datetime, timedelta


# This function allows to select unique values in a list
def get_unique_elements(list_elem):

    list_of_unique_elements = []

    unique_elements = set(list_elem)

    for elem in unique_elements:
        list_of_unique_elements.append(elem)

    return list_of_unique_elements

# This function gathers the data from the ftp directory. 
def get_data_ftp(ftp_server, ftp_directory, zone = "cty"):
    
    # connect to the server
    ftp = ftplib.FTP(ftp_server) #pass the url without protocol
    ftp.login() #pass credentials if anonymous access is not allowed
    
    # Go to the directory containing the data
    ftp.cwd(ftp_directory)
    
    
    # List for column names
    days = range(1,32)
    list_column = ["zone", "zone_id", "zone_name", "year", "month", "measure"] 
    for day in days:
        list_column.append(str(day).zfill(2)) 
    
    
    # get list of all files in this ftp directory
    all_files = ftp.nlst()
    
    # select the good files that you need
    good_files = [file for file in all_files if zone in file]
    

    # Save evert file with the right zone in your DataPath
    for f in range(len(good_files)):
        filename = good_files[f]
        r = BytesIO()
        ftp.retrbinary("RETR " + filename, r.write)
        # Transform the data into a dataframe
        df = pd.read_csv(BytesIO(r.getvalue()), header = None)
        df = pd.DataFrame(df)
        df.columns = list_column
        
        # Drop columns with impossible date (ex : february 30th doesnt make sens)
        df = df.replace(-999.99, pd.NA) 
        na_count = df.isna().sum()
        for col in df.columns:
            if na_count[col] > 200:
                df = df.drop(col, axis = 1)

        if df["measure"][1] == "PRCP":
            PRCP_array = []
            PRCP_array = df.iloc[:,6:37].stack()
            PRCP_array = PRCP_array.values
            
        if df["measure"][1] == "TAVG":
            TAVG_array = []
            TAVG_array = df.iloc[:,6:37].stack()
            TAVG_array = TAVG_array.values
            
        if df["measure"][1] == "TMAX":
            TMAX_array = []
            TMAX_array = df.iloc[:,6:37].stack()
            TMAX_array = TMAX_array.values
            
        if df["measure"][1] == "TMIN":
            TMIN_array = []
            TMIN_array = df.iloc[:,6:37].stack()
            TMIN_array = TMIN_array.values

    return df, PRCP_array, TAVG_array, TMAX_array, TMIN_array

# This function transforms the gathered data into a dataframe with to right format for SQL export
def transform_df(ftp_server, ftp_directory, zone = "cty"):
    
    results = get_data_ftp(ftp_server,ftp_directory)
    df = results[0]
    PRCP_array = results[1]
    TAVG_array = results[2]
    TMAX_array = results[3]
    TMIN_array = results[4]
    

    # List for for index date
    days = range(1,len(df.columns[6:])+1)
    list_date = []
    for day in days:
        list_date.append(str(df["year"][0]) +"-"+ str(df["month"][0]).zfill(2) +"-"+ str(day).zfill(2))
     

    # Create a new data frame 
    df_temp = pd.DataFrame(data = 0, index=df["zone_id"], columns = list_date).stack().to_frame().drop([0], axis = 1)
    df_temp = df_temp.rename_axis(index = ["zone_id", "DATE"])
    df_temp["z_id"] = df_temp.index.get_level_values("zone_id")
    df_temp["date"] = df_temp.index.get_level_values("DATE")
    df_temp = df_temp.merge(df.set_index("zone_id")["zone_name"].str.split(":").str[1], left_on = ["zone_id"], right_index = True)
    df_temp = df_temp.merge(df.set_index("zone_id")["zone_name"].str.split(":").str[0], left_on = ["zone_id"], right_index = True)
    df_temp = df_temp.merge(df.set_index("zone_id")["zone"], left_on = ["zone_id"], right_index = True)
    df_temp["PRCP"] = PRCP_array
    df_temp["TAVG"] = TAVG_array
    df_temp["TMAX"] = TMAX_array
    df_temp["TMIN"] = TMIN_array
    
    
    # Create the final dataframe
    df_fin = df_temp
    
    # Rename index and columns
    index_names = ["Z_ID", "date"]
    column_names = ["ZONE_ID", "DATE","ZONE_NAME", "STATE_ID", "ZONE_TYPE", "PRCP", "TAVG", "TMAX", "TMIN"]
    df_fin = df_fin.rename_axis(index = index_names)
    df_fin.columns = column_names    

        
    return df_fin

# This fonction compile all the data for the choosen timeframe
def compile_df(ftp_server, ftp_directory, start, end, zone = "cty"):
    
     # Get the year and month that will complete the ftp_directory
    start_date = datetime.strptime(start,"%Y-%m-%d")
    end_date = datetime.strptime(end,"%Y-%m-%d")
    
    # Get dates between those two dates
    delta = end_date - start_date
    
    list_dates = []
    for i in range(delta.days + 1):
        list_dates.append(start_date + timedelta(days=i))
        
    # Transform into string with year/month
    list_dates1 = [d.strftime("%Y/%m") 
                    for d in list_dates]
    
    # Get unique year and month (e.g. "2020/09")
    list_dates1 = get_unique_elements(list_dates1)
    list_dates1.sort()
    
    df_final = pd.DataFrame()
    # Get the csv files for the selected dates
    for n, y_m in enumerate(list_dates1):
        
        df_final = df_final.append(transform_df(ftp_server, ftp_directory+y_m))
    
    return df_final

# This function push the data to SQL Server
def push_to_staging(sql_server, sql_database,  staging_table, target_table, ftp_server, ftp_directory, start = None, end = None, zone = "cty", drop_create = True):

   
    print("Connecting to server: "+sql_server)
    print("Connecting to database: " +sql_database)    
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+sql_server+';DATABASE='+sql_database+';Trusted_Connection=yes;')
    cursor = cnxn.cursor()
    
    if drop_create == True:
        print("Dropping table: "+staging_table)
        cursor.execute("DROP TABLE IF EXISTS "+staging_table)
        cursor.commit()
        print("Creating table: "+staging_table)
        cursor.execute(f"""
                       CREATE TABLE 
                           {staging_table} (
                               [COUNTY_ID]      int             NOT NULL,
                               [DATE]           datetime        NOT NULL,
                               [COUNTY_NAME]    nvarchar(50)    NOT NULL,
                               [STATE_ID]       nvarchar(10)    NOT NULL,
                               [ZONE_TYPE]      nvarchar(10)    NOT NULL,
                               [PRCP]           float           NOT NULL,
                               [TAVG]           float           NOT NULL,
                               [TMAX]           float           NOT NULL,
                               [TMIN]           float           NOT NULL
                               );
                         """
                         )           
        cursor.commit()
    
    if start == None:
        start = cursor.execute(f"""
                               SELECT 
                                   MAX([DATE]) as 'Max_Date' 
                               FROM  
                                   [Market_Planning].{target_table}
                               """
                               ).fetchone()
        start = start.Max_Date 
        start += timedelta(1)
        start = datetime.strftime(start,"%Y-%m-%d")

    if end == None:
        try:
            end = datetime.date(datetime.today())
            end = datetime.strftime(end,"%Y-%m-%d")
            
            # Get data from NOAA
            print("Gathering data with NOAA ftp from "+start+ " to " + end)
            
            # Start task timer
            startTime = datetime.now()
        
            data = compile_df(ftp_server, ftp_directory, start, end, zone = "cty")
         
            #Print task completion time: 
            print("Gathering data with NOAA ftp completion time: " + str(datetime.now() - startTime))
            
        except:
            
            end = datetime.date(datetime.today())
            print(str(end.year)+"/"+str(end.month)+" is not available yet")
            end = end.replace(day=1)
            end -= timedelta(1)
            print("End date is set to: "+str(end.year)+"-"+str(end.month)+"-"+str(end.day))
            end = datetime.strftime(end, "%Y-%m-%d")
            # Get data from NOAA
            print("Gathering data with NOAA ftp from "+start+ " to " + end)
            
            # Start task timer
            startTime = datetime.now()
        
            data = compile_df(ftp_server, ftp_directory, start, end, zone = "cty")
         
            #Print task completion time: 
            print("Gathering data with NOAA ftp completion time: " + str(datetime.now() - startTime))
    else:

        # Get data from NOAA
        print("Gathering data with NOAA ftp from "+start+ " to " + end)
        
        # Start task timer
        startTime = datetime.now()
    
        data = compile_df(ftp_server, ftp_directory, start, end, zone = "cty")
     
        #Print task completion time: 
        print("Gathering data with NOAA ftp completion time: " + str(datetime.now() - startTime))
        
    # Get a list of unique years
    years = (pd.to_datetime(data.index.get_level_values("date"))).year.unique()
    
    # Create batch insertion in SQL per year    
    for y in years:
        
        # Start task timer
        startTime = datetime.now()
        
        print("Transforming dataframe into list for: "+str(y))
        data1 = data[pd.to_datetime(data.index.get_level_values("date")).year == y]
        data_list = data1.values.tolist()
    
        print("Inserting data into SQL for: "+str(y))
        cursor.fast_executemany = True
        cursor.executemany("INSERT INTO " + staging_table + " ([COUNTY_ID], [DATE], [COUNTY_NAME], [STATE_ID], [ZONE_TYPE], [PRCP], [TAVG], [TMAX], [TMIN]) VALUES (?,?,?,?,?,?,?,?,?)", data_list)
        print(f'{len(data_list)} rows inserted to the {staging_table} table')
        cursor.commit()
                
        #Print task completion time: 
        print("Inserting data into SQL for " +str(y)+" completion time: " + str(datetime.now() - startTime))
        
    cursor.close()
    return


# This function updates the target table used by the team
def update_table(sql_server, sql_database,  staging_table, target_table):
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+sql_server+';DATABASE='+sql_database+';Trusted_Connection=yes;')
    cursor = cnxn.cursor()
    try:
        print("Updating: " + target_table)
        merge = f"""
                MERGE Market_Planning.{target_table} as [Target]
             	USING 
            		Market_Planning.{staging_table} as [Source]
             	ON 
            		[Source].[COUNTY_ID] = [Target].[COUNTY_ID]
            		AND [Source].[DATE] = [Target].[DATE]
             			
             	WHEN NOT MATCHED BY TARGET THEN
            		INSERT (
             			[COUNTY_ID],
             			[DATE],
             			[COUNTY_NAME],
             			[STATE_ID],
             			[ZONE_TYPE],
             			[PRCP],
             			[TAVG],
             			[TMAX],
             			[TMIN])
            		VALUES(
             			[SOURCE].[COUNTY_ID],
             			[SOURCE].[DATE],
             			[SOURCE].[COUNTY_NAME],
             			[SOURCE].[STATE_ID],
             			[SOURCE].[ZONE_TYPE],
             			[SOURCE].[PRCP],
             			[SOURCE].[TAVG],
             			[SOURCE].[TMAX],
             			[SOURCE].[TMIN]
            		);"""
        cursor.execute(merge)
        cursor.commit()
    except:
        print('Could not update '+target_table)
    finally:
        cursor.close()
    return



# Define the SQL server
SQL_server = "cavlsqlpd2\pbi2"
# Database
Database = "Market_Planning"

Staging = "dbo.MKTPL_Z_US_METEO"

Target = "dbo.MKTPL_US_METEO"

# FTP server : NOAA
ftpServer = "ftp.ncdc.noaa.gov"
# FTP directory to gather data
ftpDirectory = '/pub/data/daily-grids/beta/by-month/'

# Call function to push data from NOAA ftp to SQL database
push_to_staging(sql_server = SQL_server, sql_database = Database, staging_table = Staging, target_table = Target, ftp_server = ftpServer, ftp_directory = ftpDirectory)


# Uncomment this line only if you need to gather meteo between specific dates
# push_to_staging(sql_server = SQL_server, sql_database = Database, staging_table = Staging, target_table = Target, ftp_server = ftpServer, ftp_directory = ftpDirectory, start = "2012-01-01", end = "2012-01-31")


update_table(sql_server = SQL_server, sql_database = Database, staging_table = Staging, target_table = Target)

