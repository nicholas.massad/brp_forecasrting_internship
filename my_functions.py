# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 09:43:29 2021

@author: bernida2
"""

# Import libraries
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import scipy.stats as stats
import numpy as np

class registrations():
    
    
    def __init__(self):
        return
    
    def registrations_count(self, data, freq = "W", column = None, rows = None, start = None, end = None):
        """
    
        In order to run this function, you need to have pandas library installed
    
        Parameters
        ----------
        data : Series
            Serie having dates as index
            
        freq : string, optional
            Frequency you want the column to be grouped by, if no frequency is specified, it is weekly by default
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        column : string, optional
            Name of the column you want to group by (ex: Postal Code / State / Etc.)
            The default is None
            
        row : string, optional
            Sepecify a zone (Precise zone to look at (ex : row = "Alabama" or row = "99654"))
            The default is None
            
        start : datetime, optional
            Start of the window (included)
            The default is None
        
        end : datetime, optional
            End of the window (included)
            The default is None
            
        detail : boolean, optional
            Add indexes for the desired frequency (freq)
            The default is None
        
        Returns
        -------
        z : Series
            Return a serie with 2 or more index. The first index is the date, then the frequency and the last is the zone. 
            It returns the number of registration
    
        """
        self.data = data
        self.freq = freq
        self.column = column
        self.rows = rows
        self.start = start
        self.end = end
        
        if start != None:
            data = data[data.index >= start]
        if end != None:
            data = data[data.index <= end]
        
        # Details about frequency in the index

        if column == None: 
            z = data.resample(freq).size()
            # Rename index columns
            z.index.names = ["Date"]
        
        else :
            if freq != "Y":
                z = data.groupby(column).resample(freq).size()
                # Rename index columns
                z.index.names = [column,"Date"] 
            else:
                z = data.groupby(column).resample(freq).size()
                z = data.groupby(column)[data.columns[0]].resample(freq).size()
                # Rename index columns
                z.index.names = [column,"DATE"] 
        
        # Get a specific zone in the specified column  
        if rows != None:
            z = z.loc[(z.index.get_level_values(column) == rows)]     
    
        return z
    
    
    
    
    def ts_plot(self, data, list_freq = "W", column = None, list_rows = None, start = None, end = None, style = "line", theme = "darkgrid", palette = "crest", ci = False):
        
        """
    
        Parameters
        ----------
        data : Series
            Series having dates as index
            
        list_freq : string, optional
            Define the frequency of the time series. It can be a list or a scalar
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        list_column : string, optional
            Define the columns to plot, it can be a list or a single string
            The default is None
        
        list_rows : string, optional
            Sepecify a zone (Precise zone to look at (ex : "Alabama", "99654", "ALL"))
            It can be a single zone or multiple zones
            The default is None
        
        start : datetime, optional
            Start of the window (included)
            The default is None
        
        end : datetime, optional
            End of the window (included)
            The default is None
        
        style : string, optional
            Defines the plot style, it can be "line" or "scatter"
            The default is "line"
            
        theme : string, optional
            Defines the plot theme, it can be "white", "dark", "whitegrid" or "darkgrid"
            The default is "darkgrid"
            
        palette : string, optional
            Defines the line colors of your plot
            The default is "crest".
        
        Returns
        -------
        Plot
    
        """ 
        
        self.data = data
        self.list_freq = list_freq
        self.column = column
        self.list_rows = list_rows
        self.start = start
        self.end = end
        self.style = style
        self.theme = theme
        self.palette = palette
        
        
        if style == "line":
            style = sns.lineplot
        if style == "scatter":
            style = sns.scatterplot
        
        # set theme
        sns.set_theme(style = theme)
        
        # Set color palette
        sns.set_palette(sns.color_palette(palette))
        
        
        if column == None: 
            for i in list_freq:
                ts = self.registrations_count(data, freq = i, start = start, end = end)
                style(data = ts, ci = ci)
        # Plot format
            # Title
                if i == "D":
                    t = "Daily SSV sales"
                if i == "W":
                    t = "Weekly SSV sales"
                if i == "M":
                    t = "Monthly SSV sales"
                if i == "Y":
                    t = "Yearly SSV sales"
                plt.title(t)
            # Axis
                plt.ylabel("# of units")
            # Show plot
                plt.show()
                
        if column != None and list_rows == "ALL":
            list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
            for j in list_rows:
                for i in list_freq:
                    ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                    ts = ts.droplevel(column)
                    style(data = ts, ci = ci)
            # Plot format
                # Title
                    if i == "D":
                        t = "Daily SSV sales in " + str(column) + " : " + str(j)
                    if i == "W":
                        t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                    if i == "M":
                        t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                    if i == "Y":
                        t = "Yearly SSV sales in " + str(column) + " : " + str(j)
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Show plot
                    plt.show()
        
        if column != None and type(list_rows) != list and list_rows != "ALL":
            for i in list_freq:
                ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                ts = ts.droplevel(column)
                style(data = ts, ci = ci)
        # Plot format
            # Title
                if i == "D":
                    t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "W":
                    t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "M":
                    t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "Y":
                    t = "Yearly SSV sales in " + str(column) + " : " + str(list_rows)
                plt.title(t)
            # Axis
                plt.ylabel("# of units")
            # Show plot
                plt.show()
    
        if column != None and type(list_rows) == list:
            for j in list_rows:
                for i in list_freq:
                    ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                    ts = ts.droplevel(column)
                    style(data = ts, ci = ci)
            # Plot format
                # Title
                    if i == "D":
                        t = "Daily SSV sales in " + str(column) + " : " + str(j)
                    if i == "W":
                        t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                    if i == "M":
                        t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                    if i == "Y":
                        t = "Yearly SSV sales in " + str(column) + " : " + str(j)
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Show plot
                    plt.show()
                    
        return
    
    
    
    def seasonal_plot(self, data, list_freq = "W", column = None, list_rows = None, start = None, end = None, style = "line", theme = "darkgrid", palette = "crest", sequence = False, ci = None):
       
        """
        
        Parameters
        ----------
        data : Series
            Series having dates as index
            
        list_freq : string or list, optional
            Define the frequency of the time series. It can be a list or a scalar
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        list_column : string, optional
            Define the column to plot
            The default is None.
        
        list_rows : string or list, optional
            Sepecify a zone (Precise zone to look at (ex : "Alabama", "99654", "ALL"))
            It can be a single zone or multiple zones
            The default is None.
        
        start : datetime, optional
            Start of the window (date included)
            The default is None.
        
        end : datetime, optional
            End of the window (date included)
            The default is None.
        
        style : string, optional
            Defines the plot style, it can be "line" or "scatter"
            The default is "line".
            
        theme : string, optional
            Defines the plot theme, it can be "white", "dark", "whitegrid" or "darkgrid"
            The default is "darkgrid".
        
        palette : string, optional
            Defines the line colors of your plot
            The default is "crest".
        
        sequence : boolean, optional
            Gives different layout to vizualise the graph
            The default is False.
    
        Returns
        -------
        Plot
    
        """ 
        self.data = data
        self.list_freq = list_freq
        self.column = column
        self.list_rows = list_rows
        self.start = start
        self.end = end
        self.style = style
        self.theme = theme
        self.palette = palette
        self.sequence = sequence
        self.ci = ci
        
        # Set graph theme
        sns.set_theme(style = theme)
    
        # Set color palette
        sns.set_palette(sns.color_palette(palette))
    
        # Take out "Y" from list_freq and create a temporary list
        list_freq_temp = []
        list_freq_temp = list_freq[:]    
        if "Y" in list_freq_temp:
            list_freq_temp.remove("Y")
        
        if sequence == False :
            # Set graphic style
            if style == "line":
                style = sns.lineplot
            if style == "scatter":
                style = sns.scatterplot
        
            if column == None: 
                for i in list_freq_temp:
                    if i == "D":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                        # Plot title
                        t = "Daily SSV sales"
                        # Plot xlabel
                        plt.xlabel("Day")
                    if i == "W":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Weekly SSV sales"
                        # Plot xlabel
                        plt.xlabel("Week")
                    if i == "M":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Monthly SSV sales"
                        # Plot xlabel
                        plt.xlabel("Month")
            # Plot format
                # Title
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Legend
                    plt.legend(bbox_to_anchor = (1.05,1))
                # Show plot
                    plt.show()
        
            if column != None and list_rows == "ALL":
                list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
                for j in list_rows:
                    for i in list_freq_temp:
        
                        if i == "D" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Day")
                        if i == "W" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Week")
                        if i == "M" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Month")
                            
                # Plot format
                    # Title
                        plt.title(t)
                    # Axis
                        plt.ylabel("# of units")
                    # Legend
                        plt.legend(bbox_to_anchor = (1.05,1))
                    # Show plot
                        plt.show()
        
            if column != None and type(list_rows) != list and list_rows != "ALL":
                for i in list_freq_temp:
                    if i == "D":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                        # Plot title
                        t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Day")
                    if i == "W":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Week")
                    if i == "M":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Month")
            # Plot format
                # Title
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Legend
                    plt.legend(bbox_to_anchor = (1.05,1))
                # Show plot
                    plt.show()
        
            if column != None and type(list_rows) == list:
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Day")
                        if i == "W":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Week")
                        if i == "M":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Month")
                # Plot format
                    # Title
                        plt.title(t)
                    # Axis
                        plt.ylabel("# of units")
                    # Legend
                        plt.legend(bbox_to_anchor = (1.05,1))
                    # Show plot
                        plt.show()
    
    
    
    #############################################################################################################################
    #                                        Seasonal graph with sequence
    #############################################################################################################################
    
        if sequence == True :
            if column == None: 
                     for i in list_freq_temp:
                            if i == "D":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
            
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
            
                                # Plot title
                                t = "Daily SSV sales"
                                # Axis labels
                                g.set_axis_labels("Day", "# of units")
                                
                            if i == "W":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
                                # Plot title
                                t = "Weekly SSV sales"                    
                                # Axis labels
                                g.set_axis_labels("Week", "# of units")
            
            
                            if i == "M":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
                                
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
                                # Plot title
                                t = "Monthly SSV sales"
                                # Axis labels
                                g.set_axis_labels("Month", "# of units")  
                                
                                
                            # Title
                            g.set_titles(t)
                            # Layout
                            g.tight_layout()
    
            
            if column != None and list_rows == "ALL":
                list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Day", "# of units")
    
    
                        if i == "W" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Week", "# of units")
                            
                            
                        if i == "M" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Month", "# of units")
                            
                        # Title
                        g.set_titles(t)
                        # Layout
                        g.tight_layout()
    
            if column != None and type(list_rows) != list and list_rows != "ALL":
                for i in list_freq_temp:
                    
                    if i == "D":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Day", "# of units")                   
    
                    if i == "W":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Week", "# of units")                   
                                            
                    if i == "M":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Month", "# of units")    
                        
         
                    
                    
            if column != None and type(list_rows) == list:
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Day", "# of units")   
                            
                        if i == "W":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Week", "# of units")           
                            
                        if i == "M":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Month", "# of units")  
                            
                        # Title
                        g.set_titles(t)
                        # Layout
                        g.tight_layout()             
        
        return


class dfmanipulations:
    
    
    def __init__(self):
        return
    
    def lag_columns(self,data, level, lag, first_col = None, last_col = None, drop = True):
        """
        This function aims to apply a lag on multiple column on a MultiIndex Dataframe (pandas) by creating new columns to the dataframe

        Parameters
        ----------
        data : pandas DataFrame
            data takes a multiindex dataframe as input.
            
        level : string
            Level on which to apply the lag on
        
        lag : int
            Number of lag, it can be a positive or a negative value.
        
        first_col : integer, optional
            This is the first column to apply the lag on. 
            The default is None.
        
        last_col : integer, optional
            This is the last column to apply the lag on. 
            The default is None.
        
        drop : boolean, optional
            True if we want to drop rows with NA values (created by the lag), False will keep those rows.
            The default is True.

        Returns
        -------
        data : TYPE
            DESCRIPTION.

        """
        # Declare variables
        self.data = data
        self.level = level
        self.lag = lag
        self.first_col = first_col
        self.last_col = last_col
        self.drop = drop
        
        data1 = data.copy()  
            
        # Identify the columns to be lagged
        if first_col == None:
            first_col = 1
        if last_col == None:
            last_col = len(data1.columns)
        
        # Get all zones
        level_list = data1.index.get_level_values(level).unique()
        
        # For all the column to be lagged create a new lagged column
        for c in data1.columns[first_col:last_col]:
            column_name = ''
            column_name = str("LAG_"+str(lag)+"_"+c)
    
            list_lagged = []
            for z in level_list:
                list_lagged.append(data1[c][data1.index.get_level_values(level) == z].shift(lag))
            
            list_final = []
            for x in list_lagged:
                list_final.extend(x)
            data1[column_name] = list_final
            
        if drop == True:
            data1 = data1.dropna(how = "any", axis = 0)
            
        return data1



    def corr_results(self, data, level, plot_col = None, graphs = True):
        """
        This function aims to return the correlation and p-value in a MultiIndex dataframe

        Parameters
        ----------
        data : dataframe (pandas)
            dataframe on wich you want to calculate the correlations, 
            the first column should ALWAYS be your reference value which you will use to compare correlations with other columns
            
        level : string
            Index level on which you want to test the correlation.
            
        plot_col : list, string, optional
            Specific column that you want to be plotted
            
        graphs : bool, optional
            Returns the correlation graphs for all the levels. The default is True.

        Returns
        -------
        corr_results : TYPE
            DESCRIPTION.

        """
        self.data = data
        self.level = level
        self.plot_col = plot_col
        self.graphs = graphs
        
        
        # Set graph theme
        sns.set_theme(style = "darkgrid")
        
        # Level treatment (has to be par of a multiindex dataframe)
        level_list = data.index.get_level_values(level).unique()
        
        # Create a column with the index as a new column
        column_names = [level]
        
        # Create columns containing correlation and p-values
        for c in data.columns[1:]:
            column_names += ["CORR_"+c, "P_"+c]
        
        # Create the DataFrame that will contain correlation results
        corr_results = pd.DataFrame(columns = column_names)
        corr_results[level] = level_list
        
        # For each element of the level (for instance the STATE_ID)
        for z in level_list:
            # Get the column to correlate the first column with (data.columns[0])
            for measure in data.columns[1:]:
                # Values to test the correlation
                m = data.columns[0]
                
                # Temporary dataframe
                data_temp = data.loc[(data.index.get_level_values(level) == z),:]
            
                x = data_temp[measure][data_temp[m] != '']
                y = data_temp[m][data_temp[m] != '']
                
                # Calculate the correlation
                corr = stats.pearsonr(x,y)
                
                # Graph creation
                if graphs == True:
                    if plot_col != None:
                        for col in plot_col:
                            if col == measure:
                                
                                x_plot = data_temp[col][data_temp[m] != '']
                                y_plot = data_temp[m][data_temp[m] != '']
                                
                                sns.scatterplot(x = x_plot, y = y_plot, palette = "crest", color = "black")
                                m, b = np.polyfit(x_plot, y_plot, 1)
            
                                plt.plot(x_plot, m*x_plot + b, color = "darkgoldenrod")
                                
                                plt.title("Monthly correlation between "+measure+ " and "+str(data.columns[0])+" for: "+z + "\n Correlation :" + str(corr))
                                
                                plt.show()
                    else:
                        sns.scatterplot(x = x, y = y, palette = "crest", color = "black")
                        m, b = np.polyfit(x, y, 1)
    
                        plt.plot(x, m*x + b, color = "darkgoldenrod")
                        
                        plt.title("Monthly correlation between "+measure+ " and "+str(data.columns[0])+" for: "+z + "\n Correlation :" + str(corr))
                        
                        plt.show()
                
                # For every column of the initial data frame, insert a CORR and a P-VALLUE column
                corr_results["CORR_"+measure][corr_results[level]==z] =  corr[0]
                corr_results["P_"+measure][corr_results[level]==z] =  corr[1]
                    
                
        return corr_results