# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd

import pyodbc as pc



# Show all columns
pd.set_option('display.max_columns', None)


def get_census_table():
    
    
    # Define the SQL server
    SQL_server = "cavlsqlpd2\pbi2"
    
    print("Connecting to server: "+SQL_server)
            
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';Trusted_Connection=yes;')
        
    # Start task timer
    startTime = pd.Timestamp.now()
    

    print("Gathering table names")
    df = pd.read_sql("""
                        SELECT
                        	CONCAT('[Market_Planning].',schema_name(t.schema_id),'.', t.name) AS 'TARGET',
                            t.name
                        FROM 
                        	Market_Planning.sys.tables t
                        WHERE
                        	t.name like 'MKTPL_CENSUS%'
                        """
                      ,cnxn
                    
                     )
    
    #Print task completion time: 
    print("Gathering table names | completion time: " + str(pd.Timestamp.now() - startTime))
    
                  

    return df

def alter_census_table():  
    
    census_tables = get_census_table() 
        
    
    # Define the SQL server
    SQL_server = "cavlsqlpd2\pbi2"
    
    print("Connecting to server: "+SQL_server)
            
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';Trusted_Connection=yes;')
    cursor = cnxn.cursor()
    
    for i in range(len(census_tables)):
        print("Changing ZIPCODE datatype for table : " + census_tables.iloc[i][1])
        cursor.execute(f"""
                        ALTER TABLE 
                        	{census_tables.iloc[i][0]}
                        ALTER COLUMN ZIPCODE varchar(5)
                        ;
                        """
                        )
        cursor.commit()
    return

def update_census_table():
    census_tables = get_census_table() 
    
    # Define the SQL server
    SQL_server = "cavlsqlpd2\pbi2"
    
    print("Connecting to server: "+SQL_server)
            
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';Trusted_Connection=yes;')
    cursor = cnxn.cursor()
    
    for i in range(len(census_tables)):
        print("Padding ZIPCODE with zeros for table : " + census_tables.iloc[i][1])
        cursor.execute(f"""
                        UPDATE
                        	{census_tables.iloc[i][0]}
                        SET 
                            ZIPCODE = RIGHT('00000' + ZIPCODE, 5)
                        WHERE
                            LEN(ZIPCODE) < 5
                        ;
                        """
                        )
        cursor.commit()
    return

alter_census_table()
update_census_table()
