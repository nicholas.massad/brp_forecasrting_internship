# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 16:12:22 2021

@author: bernida2
"""


# Import libraries
import pandas as pd
from zipcode_association import zip_dictionnary
from our_functions import registrations, dfmanipulations, ImpExp


import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import scipy.stats as stats
# from statsmodels.graphics.factorplots import interaction_plot
# import statsmodels.api as sm
# import statsmodels.formula.api as smf
# Connect to the SQL server
# import pyodbc as pc



# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

# =====================================================
# ----- Data Importation
# =====================================================


# Put data from SQL into dataframes

# Get all the industry data
df = imp.get_data("sql", "dbo.MKTPL_CENSUS_PERCENTAGE_HOUSEHOLD_CHARACTERISTICS", path = None, Server = 'cavlsqlpd2\pbi2', Database = "Market_Planning", columns = None)


for col in df.columns:
    if col not in ("STATE", "YEAR", "ZIPCODE"):
        df = df[df[col]!= '']
        df[col] = df[col].astype(float)

for col in df.columns:
    if col not in ("STATE", "YEAR", "ZIPCODE"):
        #mask = (df[col] > 0) & (df[col] <= 100)
        mask = (df[col] > 0)
        df[col][mask].apply(np.log)
        df[col][mask].plot.density()
        plt.title(str(col)+"\n mean : "+str(df[col][mask].mean()))
        if max(df[col]) <= 100:
            plt.xlim(0,100)
        else:
            plt.xlim(0,max(df[col]))
        plt.show()
        print(str(col) +"\n" + str(df[col][mask].describe()))
        
        
        