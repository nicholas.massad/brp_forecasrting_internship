# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# import seaborn as sns

import scipy.stats as stats
from statsmodels.graphics.factorplots import interaction_plot
# import statsmodels.api as sm
# import statsmodels.formula.api as smf

from our_functions import registrations, dfmanipulations, ImpExp



# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

# =====================================================
# ----- Data Importation
# =====================================================

# Dataset in SQL
US_METEO = "meteo"
VEHICLE_REGISTRATION = "VR"
INDUSTRY = "ind"



# Put data from SQL into dataframes

# Get all the vehicle registrations
VehicleRegistration = imp.import_predefined_from_sql(VEHICLE_REGISTRATION, min_date = "2012-01-01")

# Get all the meteorological data
USMeteo = imp.import_predefined_from_sql(US_METEO)

IndustryRetail = imp.import_predefined_from_sql(INDUSTRY)


# =====================================================
# ----- Correlation analysis on customer state
# =====================================================

# Get meteo data by date and state
USMeteo_Serie = USMeteo.groupby("STATE_ID").resample("M").mean()


# Get registrations by date and state
VehicleRegistration_Serie = reg.registrations_count(VehicleRegistration, freq = "M", column = "STATE_ID")
VehicleRegistration_Serie.index.names = ["STATE_ID", "DATE"]
VehicleRegistration_Serie = pd.DataFrame(VehicleRegistration_Serie, columns=["COUNT"])

# Create a main dataframe 
df = VehicleRegistration_Serie.merge(USMeteo_Serie, how = "left", on = ["STATE_ID", "DATE"])

# test = stats.pearsonr(df["COUNT"],df["TAVG"])



df_lag_12 = mani.lag_columns(df,"STATE_ID", 12)

# Test with 2020 and above included
summer_1 = df[(df.index.get_level_values("DATE").month >= 5) & (df.index.get_level_values("DATE").month < 10)]

winter_1 = df[(df.index.get_level_values("DATE").month < 5) | (df.index.get_level_values("DATE").month >= 10)]

results = mani.corr_results(df, "STATE_ID", graphs=False)

results_summer_1 = mani.corr_results(summer_1, "STATE_ID", graphs=False)

results_winter_1 = mani.corr_results(winter_1, "STATE_ID", graphs=False)


# fig, ax = plt.subplots(figsize=(6, 6))
# fig = interaction_plot(x=winter_1["TAVG"], trace=winter_1.index.get_level_values("DATE").month, response=winter_1["COUNT"],
#                        ax=ax, plottype="scatter")

result_12_months = mani.corr_results(mani.lag_columns(df, "STATE_ID", 12),"STATE_ID", graphs=False)
result_12_months["P_LAG_12_TAVG"][result_12_months["P_LAG_12_TAVG"]< 0.05].count()

result_12_months["STATE_ID"][result_12_months["P_LAG_12_TAVG"] > 0.05]


# Test with 2019 and above excluded

df2 = df[df.index.get_level_values("DATE")<"2019-01-01"]

df2_lag_12 = mani.lag_columns(df2,"STATE_ID", 12)


result_1_month_df2 = mani.corr_results(mani.lag_columns(df2, "STATE_ID", 1),"STATE_ID", graphs=False)
result_1_month_df2["P_LAG_1_TAVG"][result_1_month_df2 ["P_LAG_1_TAVG"]< 0.05].count()


result_12_month_df2 = mani.corr_results(df2_lag_12,"STATE_ID", graphs=False)
result_12_month_df2 ["P_LAG_12_TAVG"][result_12_month_df2 ["P_LAG_12_TAVG"]< 0.05].count()


summer_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month >= 5) & (df2_lag_12.index.get_level_values("DATE").month < 10)]

winter_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month < 5) | (df2_lag_12.index.get_level_values("DATE").month >= 10)]


# fig, ax = plt.subplots(figsize=(6, 6))
# fig = interaction_plot(x=winter_2_lag_12["TAVG"], trace=winter_2_lag_12.index.get_level_values("DATE").month, response=winter_2_lag_12["COUNT"],
#                         ax=ax, plottype="scatter")


result_12_months_summer_2 = mani.corr_results(summer_2_lag_12,"STATE_ID", graphs=False)
result_12_months_summer_2["P_LAG_12_TAVG"][result_12_months_summer_2["P_LAG_12_TAVG"]< 0.05].count()

# applying a log on the count : Doesn't change much the correlation
# winter_2_lag_12["COUNT"] = winter_2_lag_12["COUNT"].apply(np.log)
# winter_2_lag_12 = winter_2_lag_12[winter_2_lag_12["COUNT"] > 0]

result_12_months_winter_2 = mani.corr_results(winter_2_lag_12,"STATE_ID", plot_col = ["TAVG"],graphs=True)
result_12_months_winter_2["P_LAG_12_TAVG"][(result_12_months_winter_2["P_LAG_12_TAVG"]< 0.05) & (result_12_months_winter_2["CORR_LAG_12_TAVG"] > 0.5)].count()

result_12_months_winter_2["STATE_ID"][result_12_months_winter_2["P_LAG_12_TAVG"] > 0.025]


# Conclusion
# There is correlation between certain states and the temperature of the last 12 months especially during winter
# It happens also for states like LA and Colorado during summertime





# =====================================================
# ----- Correlation analysis on dealer state
# =====================================================

# # Get meteo data by date and state
# USMeteo_Serie = USMeteo.groupby("STATE_ID").resample("M").mean()

# # Get registrations by date and state
# VehicleRegistration_Serie = reg.registrations_count(VehicleRegistration, freq = "M", column = "Dealer_Region")
# VehicleRegistration_Serie.index.names = ["STATE_ID", "DATE"]
# VehicleRegistration_Serie = pd.DataFrame(VehicleRegistration_Serie, columns=["COUNT"])

# # Create a main dataframe 
# df = VehicleRegistration_Serie.merge(USMeteo_Serie, how = "left", on = ["STATE_ID", "DATE"])



# df_lag_12 = mani.lag_columns(df,"STATE_ID", 12)

# # Test with 2020 and above included
# summer_1 = df[(df.index.get_level_values("DATE").month >= 5) & (df.index.get_level_values("DATE").month < 10)]

# winter_1 = df[(df.index.get_level_values("DATE").month < 5) | (df.index.get_level_values("DATE").month >= 10)]

# results = mani.corr_results(df, "STATE_ID", graphs=False)

# results_summer_1 = mani.corr_results(summer_1, "STATE_ID", graphs=False)

# results_winter_1 = mani.corr_results(winter_1, "STATE_ID", graphs=False)


# # fig, ax = plt.subplots(figsize=(6, 6))
# # fig = interaction_plot(x=winter_1["TAVG"], trace=winter_1.index.get_level_values("DATE").month, response=winter_1["COUNT"],
# #                        ax=ax, plottype="scatter")

# result_12_months = mani.corr_results(mani.lag_columns(df, "STATE_ID", 12),"STATE_ID", graphs=False)
# result_12_months["P_LAG_12_TAVG"][result_12_months["P_LAG_12_TAVG"]< 0.05].count()

# result_12_months["STATE_ID"][result_12_months["P_LAG_12_TAVG"] > 0.05]



# # Test with 2020 and above excluded

# df2 = df[df.index.get_level_values("DATE")<"2020-01-01"]

# df2_lag_12 = mani.lag_columns(df2,"STATE_ID", 12)


# result_1_month_df2 = mani.corr_results(mani.lag_columns(df2, "STATE_ID", 1),"STATE_ID", graphs=False)
# result_1_month_df2["P_LAG_1_TAVG"][result_1_month_df2 ["P_LAG_1_TAVG"]< 0.05].count()


# result_12_month_df2 = mani.corr_results(df2_lag_12,"STATE_ID", graphs=False)
# result_12_month_df2 ["P_LAG_12_TAVG"][result_12_month_df2 ["P_LAG_12_TAVG"]< 0.05].count()


# summer_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month >= 5) & (df2_lag_12.index.get_level_values("DATE").month < 10)]

# winter_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month < 5) | (df2_lag_12.index.get_level_values("DATE").month >= 10)]


# fig, ax = plt.subplots(figsize=(6, 6))
# fig = interaction_plot(x=winter_2_lag_12["TAVG"], trace=winter_2_lag_12.index.get_level_values("DATE").month, response=winter_2_lag_12["COUNT"],
#                        ax=ax, plottype="scatter")

# result_12_months_summer_2 = mani.corr_results(summer_2_lag_12,"STATE_ID", graphs=False)
# result_12_months_summer_2["P_LAG_12_TAVG"][result_12_months_summer_2["P_LAG_12_TAVG"]< 0.05].count()


# result_12_months_winter_2 = mani.corr_results(winter_2_lag_12,"STATE_ID", plot_col = ["TAVG"],graphs=True)
# result_12_months_winter_2["P_LAG_12_TAVG"][result_12_months_winter_2["P_LAG_12_TAVG"]< 0.05].count()

# result_12_months_winter_2["STATE_ID"][result_12_months_winter_2["P_LAG_12_TAVG"] > 0.025]

# # Conclusion
# # Same results when we use CUSTOMER location




# =====================================================
# ----- Correlation analysis on industry
# =====================================================

# # Get meteo data by date and state
# USMeteo_Serie = USMeteo.groupby("STATE_ID").resample("M").mean()

# # Get registrations by date and state
# IndustryRetail_Serie = reg.registrations_count(IndustryRetail, freq = "M", column = "STATE_ID")
# IndustryRetail_Serie.index.names = ["STATE_ID", "DATE"]
# IndustryRetail_Serie= pd.DataFrame(IndustryRetail_Serie, columns=["COUNT"])

# # Create a main dataframe 
# df = IndustryRetail_Serie.merge(USMeteo_Serie, how = "left", on = ["STATE_ID", "DATE"])




# df_lag_12 = mani.lag_columns(df,"STATE_ID", 12)

# # Test with 2020 and above included
# summer_1 = df[(df.index.get_level_values("DATE").month >= 5) & (df.index.get_level_values("DATE").month < 10)]

# winter_1 = df[(df.index.get_level_values("DATE").month < 5) | (df.index.get_level_values("DATE").month >= 10)]

# results = mani.corr_results(df, "STATE_ID", graphs=False)
# results["P_TAVG"][results["P_TAVG"]< 0.05].count()

# results_summer_1 = mani.corr_results(summer_1, "STATE_ID", graphs=False)
# results_summer_1["P_TAVG"][results_summer_1["P_TAVG"]< 0.05].count()

# results_summer_1["STATE_ID"][results_summer_1["P_TAVG"] < 0.05]


# results_winter_1 = mani.corr_results(winter_1, "STATE_ID", graphs=False)
# results_winter_1["P_TAVG"][results_winter_1["P_TAVG"]< 0.05].count()

# results_winter_1["STATE_ID"][results_winter_1["P_TAVG"] > 0.05]

# # fig, ax = plt.subplots(figsize=(6, 6))
# # fig = interaction_plot(x=winter_1["TAVG"], trace=winter_1.index.get_level_values("DATE").month, response=winter_1["COUNT"],
# #                        ax=ax, plottype="scatter")

# result_1_months = mani.corr_results(mani.lag_columns(df, "STATE_ID", 1),"STATE_ID", graphs=False)
# result_1_months["P_LAG_1_TAVG"][result_1_months["P_LAG_1_TAVG"]< 0.05].count()

# result_1_months["STATE_ID"][result_1_months["P_LAG_1_TAVG"] > 0.05]


# result_12_months = mani.corr_results(mani.lag_columns(df, "STATE_ID", 12),"STATE_ID", graphs=False)
# result_12_months["P_LAG_12_TAVG"][result_12_months["P_LAG_12_TAVG"]< 0.05].count()

# result_12_months["STATE_ID"][result_12_months["P_LAG_12_TAVG"] > 0.05]


# # Test with 2020 and above excluded

# df2 = df[df.index.get_level_values("DATE")<"2020-01-01"]

# df2_lag_12 = mani.lag_columns(df2,"STATE_ID", 12)


# result_1_month_df2 = mani.corr_results(mani.lag_columns(df2, "STATE_ID", 1),"STATE_ID", graphs=False)
# result_1_month_df2["P_LAG_1_TAVG"][result_1_month_df2 ["P_LAG_1_TAVG"]< 0.05].count()


# result_12_month_df2 = mani.corr_results(df2_lag_12,"STATE_ID", graphs=False)
# result_12_month_df2 ["P_LAG_12_TAVG"][result_12_month_df2 ["P_LAG_12_TAVG"]< 0.05].count()


# summer_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month >= 5) & (df2_lag_12.index.get_level_values("DATE").month < 10)]

# winter_2_lag_12 = df2_lag_12[(df2_lag_12.index.get_level_values("DATE").month < 5) | (df2_lag_12.index.get_level_values("DATE").month >= 10)]


# fig, ax = plt.subplots(figsize=(6, 6))
# fig = interaction_plot(x=winter_2_lag_12["TAVG"], trace=winter_2_lag_12.index.get_level_values("DATE").month, response=winter_2_lag_12["COUNT"],
#                         ax=ax, plottype="scatter")

# result_12_months_summer_2 = mani.corr_results(summer_2_lag_12,"STATE_ID", graphs=False)
# result_12_months_summer_2["P_LAG_12_TAVG"][result_12_months_summer_2["P_LAG_12_TAVG"]< 0.05].count()

# result_12_months_summer_2["STATE_ID"][result_12_months_summer_2["P_LAG_12_TAVG"] < 0.05]



# result_12_months_winter_2 = mani.corr_results(winter_2_lag_12,"STATE_ID", plot_col = ["TAVG"],graphs=True)
# result_12_months_winter_2["P_LAG_12_TAVG"][result_12_months_winter_2["P_LAG_12_TAVG"]< 0.05].count()

# #result_12_months_winter_2["STATE_ID"][result_12_months_winter_2["P_LAG_12_TAVG"] > 0.025]


# # Conclusion
# # Industry sales are even more correlated with temperature than BRP sales.

