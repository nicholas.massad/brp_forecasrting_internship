# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
from our_functions import registrations, dfmanipulations, ImpExp
import os
from zipcode_association import zip_dictionnary
import math

# Connect to the SQL server
import pyodbc as pc


# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

#PATH = os.getcwd()
PATH = 'C:/Users/masn2601/Documents'
PATH_PERCENT = PATH+'/Data/CENSUS_DATA_PERCENTAGE'
PATH = PATH+'/Data/CENSUS_DATA'
Database = 'Market_Planning'

EVIL_STATES = ['AK', 'HI', 'GU', 'VI', 'PR', 'AS', 'DC']
# =====================================================
# ----- Data Importation
# =====================================================

#Datasets in CSV
csv_dataset = (
    "Class_of_workers.csv",
    "Education_attainement.csv",
    "Employement_status.csv",
    "Households_characteristics.csv",
    "Housing_units.csv",
    "Housing_value.csv",
    "Income_and_benefits.csv",
    "Industry.csv",
    "Male_population_by_age.csv",
    "Maried_couple.csv",
    "Non_family_households.csv",
    "Occupation.csv",
    "Population_by_age.csv",
    "Rooms.csv",
    "School_enrollement.csv",
    "Single_dad.csv",
    "Single_mom.csv",
    "Vehicules_available.csv",
    "Workers_16over_vehicule_availability.csv")


# Datasets in SQL
sql_dataset = (
    "dbo.MKTPL_CENSUS_CLASS_OF_WORKERS",
    "dbo.MKTPL_CENSUS_EDUCATION_ATTAINEMENT",
    "dbo.MKTPL_CENSUS_EMPLOYEMENT_STATUS",
    "dbo.MKTPL_CENSUS_HOUSEHOLDS_CHARACTERISTICS",
    "dbo.MKTPL_CENSUS_HOUSING_UNITS",
    "dbo.MKTPL_CENSUS_HOUSING_VALUE",
    "dbo.MKTPL_CENSUS_INCOME_AND_BENEFITS",
    "dbo.MKTPL_CENSUS_INDUSTRY",
    "dbo.MKTPL_CENSUS_MALE_POPULATION_BY_AGE",
    "dbo.MKTPL_CENSUS_MARIED_COUPLE",
    "dbo.MKTPL_CENSUS_NON_FAMILY_HOUSEHOLDS",
    "dbo.MKTPL_CENSUS_OCCUPATION",
    "dbo.MKTPL_CENSUS_POPULATION_BY_AGE",
    "dbo.MKTPL_CENSUS_ROOMS",
    "dbo.MKTPL_CENSUS_SCHOOL_ENROLLEMENT",
    "dbo.MKTPL_CENSUS_SINGLE_DAD",
    "dbo.MKTPL_CENSUS_SINGLE_MOM",
    "dbo.MKTPL_CENSUS_VEHICULES_AVAILABLE",
    "dbo.MKTPL_CENSUS_WORKERS_16OVER_VEHICULE_AVAILABILITY",
    "dbo.MKTPL_CENSUS_PERCENTAGE_CLASS_OF_WORKERS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_EDUCATION_ATTAINEMENT",
    "dbo.MKTPL_CENSUS_PERCENTAGE_EMPLOYEMENT_STATUS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_HOUSEHOLDS_CHARACTERISTICS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_HOUSING_UNITS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_HOUSING_VALUE",
    "dbo.MKTPL_CENSUS_PERCENTAGE_INCOME_AND_BENEFITS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_INDUSTRY",
    "dbo.MKTPL_CENSUS_PERCENTAGE_MALE_POPULATION_BY_AGE",
    "dbo.MKTPL_CENSUS_PERCENTAGE_MARIED_COUPLE",
    "dbo.MKTPL_CENSUS_PERCENTAGE_NON_FAMILY_HOUSEHOLDS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_OCCUPATION",
    "dbo.MKTPL_CENSUS_PERCENTAGE_POPULATION_BY_AGE",
    "dbo.MKTPL_CENSUS_PERCENTAGE_ROOMS",
    "dbo.MKTPL_CENSUS_PERCENTAGE_SCHOOL_ENROLLEMENT",
    "dbo.MKTPL_CENSUS_PERCENTAGE_SINGLE_DAD",
    "dbo.MKTPL_CENSUS_PERCENTAGE_SINGLE_MOM",
    "dbo.MKTPL_CENSUS_PERCENTAGE_VEHICULES_AVAILABLE",
    "dbo.MKTPL_CENSUS_PERCENTAGE_WORKERS_16OVER_VEHICULE_AVAILABILITY")


#initialises the menu to sepecify wich operation to do
def menu():
    #Initialisation
    print('###################Initialising#####################\n')
    
    #gets the source of the data for processing
    data_source = ''
    while data_source.upper() != 'CSV' and data_source.upper() != 'SQL':    
        data_source = input('will you be getting data from csv or sql? : ')
        if data_source.upper() != 'CSV' and data_source.upper() != 'SQL':
            print('\nInvalid data Source\n')
            
    
    yn =''
    while yn.upper() != 'Y' and yn.upper() != 'N':
        yn = input('do you wish to have your data in percentage (y/n)')
        
    
    #Select the type of analysis to be performed
    #normal = 0
    #log = 1
    #Growth = 2
    anal = -1
    while anal != 1 and anal != 2:
        anal = int(input('\nPlease enter the type of correlation analysis\n1 - Normal\n2 - Log\n: '))
        if anal != 1 and anal != 2:
            print('invalid analysis type')
    
    #Select the dataset to analyse
    data_type = -1
    while (data_type != 0 and data_type != 1 and data_type != 2 and data_type != 3 and data_type != 4 and 
            data_type != 5 and data_type != 6 and data_type != 7 and data_type != 8 and data_type != 9 and
            data_type != 10 and data_type != 11 and data_type != 12 and data_type != 13 and data_type != 14 and
            data_type != 15 and data_type != 16 and data_type != 17 and data_type != 18 and data_type != 19 and data_type != 20):
        
        data_type = int(input('''\nPlease input the subject for the correlation
        0 - Class of worker
        1 - Education attainement
        2 - Employement status
        3 - Households characteristics
        4 - Housing units
        5 - Housing value
        6 - Income and benefits
        7 - Industry
        8 - Male population by age
        9 - Maried couple
        10 - Non family households
        11 - Occupation
        12 - Population by age
        13 - Rooms
        14 - School enrollement
        15 - Single dad
        16 - Single mom
        17 - Vehicules available
        18 - Workers 16over vehicule availability
        
        : 
        '''))
        
        if (data_type != 0 and data_type != 1 and data_type != 2 and data_type != 3 and data_type != 4 and 
            data_type != 5 and data_type != 6 and data_type != 7 and data_type != 8 and data_type != 9 and
            data_type != 10 and data_type != 11 and data_type != 12 and data_type != 13 and data_type != 14 and
            data_type != 15 and data_type != 16 and data_type != 17 and data_type != 18 and data_type != 19 and data_type != 20):
            print('Invalid dataset')
    
    percentage = False
    if yn.upper() == 'Y':
        percentage = True   
        
    if data_source.upper() == 'CSV':
        data_type = csv_dataset[data_type]
    elif data_source.upper() == 'SQL':
        if percentage == True:    
            data_type = sql_dataset[data_type+19]
        else:
            data_type = sql_dataset[data_type]
        
    yearly = ''
    while yearly != 'a' and yearly != 'y':
        yearly = input('Would you like to have to data per year or the average\ny for yearly\na for average\n: ').lower()
    if yearly == 'y':
        split_year = True
    else:
        split_year = False
        
    
        
    return data_source, anal, data_type, split_year, percentage


#=============================================================================
#=============================================================================
#=================CENSUS DATA CORRELATION ANALYSIS============================
#=============================================================================

#Retransforms percentage values in various tables to the original value
#If percentage is True : retransforms values into percentages based on the total population in the state
#modifies data to have a ratio in zipcodes to improve correlation
# def perc_arrange(df, dataframe, percentage = False):
#     if (dataframe == csv_dataset[3] or dataframe == csv_dataset[8] or dataframe == csv_dataset[9] or:
#         dataframe == csv_dataset[11] or dataframe == csv_dataset[13] or dataframe == csv_dataset[16] or
#         dataframe == csv_dataset[17] or dataframe == csv_dataset[19]):
        
#     return
    

#This functioon will remove any column with the state or zipcode that is null
#It will also properly reindex the values depending on the desired output correlation
#It will add the sales per zipcode as well for the total years
#This function can return a dataset of the average of the all the years or all the years seperatly

#Do not use the drop_year for the year 2019 if you are using table 13 (Population by age)
def arrange_census_data(dataframe, data_type, percentage, drop_year = None, year_split = False):
    '''
    Parameters
    ----------
    dataframe : STR
        Location either of the SQL table or the CSV filename
    data_type : STR
        'csv' if using csv
        'sql' if using sql
    drop_year : Array INT
        Array of the years not to use 
        if left blank keeps all the years
    year_split : Bool
        True : splits the points into years
        False : averages the points over all the years
    ratio : Bool
        True : returns the dataset without the Associated states and sales to be further processed
        False : returns normal dataset
        
    Return
    ------
    Returns the modified dataframe as a pandas dataframme

    '''
    path = ''
    if percentage == True:
        path = PATH_PERCENT
    else:
        path = PATH
    if data_type == 'csv':
        if year_split == False:
            #################################################
            print('importing from csv\n')
            df = pd.read_csv(path+'/'+dataframe)            # Imports the data from CSV
            df = df.drop('STATE', axis = 1)                 # Modifies the data into indexes and removes states (str) for processing
            df = df.set_index(['YEAR', 'ZIPCODE'])
            #################################################
            #################################################
            if drop_year != None:                           # Drops the selected years from the database
                df = df.drop(index = drop_year, level = 0)
            #################################################    
            df = df.dropna()                                #Removes lack of datapoints and converts to float for further processing
            df = df.astype('float')
            #################################################
            for row in df:                                  #Changes negative values to nan instead
                new_row = []
                for i in df[row]:
                    if i < 0:
                        new_row.append(None)
                    else:
                        new_row.append(i)
                df[row] = new_row
            #################################################    
            df2 = df.groupby('ZIPCODE').mean()              #Calculates the average over the given time period
            #################################################
            state = []
            for i in df2.index:                             #Reads the states
                try:
                    state.append(zip_dictionnary[str(i).zfill(5)])
                except:
                    state.append(None)
            df2['STATE'] = state
            #################################################
            sales = pd.read_csv(PATH+'/Sales_data.csv')     #Gets sales data and formats it
            if drop_year != None:
                sales = sales.set_index(['YEAR'])
                sales = sales.drop(index = str(drop_year), level = 0)
                sales = sales.reset_index()    
            sales = sales.set_index(['ZIPCODE'])
            sales = sales.groupby('ZIPCODE').size()
            sales = sales.rename('UNITS_SOLD')
            #########################################################
            df3 = df2.merge(sales, how = 'outer', on = 'ZIPCODE')   #Merges the 2 dataframes
            df3['UNITS_SOLD'] = df3['UNITS_SOLD'].fillna(0)
        #############################################################
        #############################################################
        elif year_split == True:
            print('importing from csv\n')                   #exactly the same code for the next 3 sections but for year split instead of average and sql instead of csv
            df = pd.read_csv(path+'/'+dataframe)
            df = df.drop('STATE', axis = 1)
            df = df.set_index(['YEAR', 'ZIPCODE'])
            
            if drop_year != None:              
                df = df.drop(index = drop_year, level = 0)
                
            df = df.dropna()
            df = df.astype('float')
            
            for row in df:
                new_row = []
                for i in df[row]:
                    if i < 0:
                        new_row.append(None)
                    else:
                        new_row.append(i)
                df[row] = new_row
                
            df = df.reset_index()
            df2_2 = df
            df['YEAR'] = pd.to_datetime(df['YEAR'], format = '%Y')
            df = df.set_index('YEAR')
            df = df.groupby('ZIPCODE').resample('Y').mean()
            df = df.drop('ZIPCODE', axis = 1)
            df = df.reset_index(level = 1)
            df2 = df
            
            state = []
            for i in df2.index:
                try:
                    state.append(zip_dictionnary[str(i).zfill(5)])
                except:
                    state.append(None)
            df2['STATE'] = state
            df2 = df2.set_index('YEAR', append = True)
            sales = pd.read_csv(PATH+'/Sales_data.csv')
            if drop_year != None:
                sales = sales.set_index(['YEAR'])
                sales = sales.drop(index = str(drop_year), level = 0)
                sales = sales.reset_index()
            sales2 = sales
            sales['YEAR'] = pd.to_datetime(sales['YEAR'], format = '%Y')
            
            if percentage == True:
                sales2 = sales2.set_index(['YEAR', 'ZIPCODE'])
                sales2 = sales2.drop('STATE', axis = 1)
                df3 = sales2.merge(df2_2, how = 'left', on = ['ZIPCODE', 'YEAR'])
            else:
                sales = sales.set_index('YEAR')
                sales = sales.groupby('ZIPCODE').resample('Y').size()
                sales = sales.rename('UNITS_SOLD')
                df3 = df2.merge(sales, how = 'outer', on = ['ZIPCODE', 'YEAR'])
                df3['UNITS_SOLD'] = df3['UNITS_SOLD'].fillna(0)
            
        #############################################################
        #############################################################            
    elif data_type == 'sql':
        if year_split == False:
            
            print('importing from sql\n')
            df = imp.get_data(source = 'sql', table_name = dataframe, Database = Database)
            df = df.drop('STATE', axis = 1)
            df = df.set_index(['YEAR', 'ZIPCODE'])
            
            if drop_year != None:
                df = df.drop(index = drop_year, level = 0)
                
            df = df.dropna()
            df = df.astype('float')
            
            for row in df:
                new_row = []
                for i in df[row]:
                    if i < 0:
                        new_row.append(None)
                    else:
                        new_row.append(i)
                df[row] = new_row
                
            df2 = df.groupby('ZIPCODE').mean()
            
            state = []
            for i in df2.index:
                try:
                    state.append(zip_dictionnary[str(i).zfill(5)])
                except:
                    state.append(None)
                    
            df2['STATE'] = state
            
            sales = imp.import_predefined_from_sql(target = 'VR', min_date = '2011-01-01')
            sales = sales.reset_index()
            sales = sales[['Postal_Code', 'STATE_ID', 'Registration_Date']]
            sales.columns = ['ZIPCODE', 'STATE', 'YEAR']
            
            
            new_year = []
            for year in sales['YEAR']:
                new_year.append(year.year)
            sales['YEAR'] = new_year
            if drop_year != None:
                sales = sales.set_index(['YEAR'])
                sales = sales.drop(index = drop_year)
                sales = sales.reset_index()
                
            sales = sales.set_index(['ZIPCODE'])
            sales = sales.groupby('ZIPCODE').size()
            sales = sales.rename('UNITS_SOLD')
            
            df3 = df2.merge(sales, how = 'outer', on = 'ZIPCODE')
            df3['UNITS_SOLD'] = df3['UNITS_SOLD'].fillna(0)
        #############################################################
        #############################################################        
        elif year_split == True:
            print('importing from sql\n')
            df = imp.get_data(source = 'sql', table_name = dataframe, Database = Database)
            df = df.drop('STATE', axis = 1)
            df = df.set_index(['YEAR', 'ZIPCODE'])
            
            if drop_year != None:              
                df = df.drop(index = drop_year, level = 0)
                
            df = df.dropna()
            df = df.astype('float')
            
            for row in df:
                new_row = []
                for i in df[row]:
                    if i < 0:
                        new_row.append(None)
                    else:
                        new_row.append(i)
                df[row] = new_row
                
            df = df.reset_index()
            df2_2 = df
            df['YEAR'] = pd.to_datetime(df['YEAR'], format = '%Y')
            df = df.set_index('YEAR')
            df = df.groupby('ZIPCODE').resample('Y').mean()
            df = df.drop('ZIPCODE', axis = 1)
            df = df.reset_index(level = 1)
            df2 = df
            
            state = []
            for i in df2.index:
                try:
                    state.append(zip_dictionnary[str(i).zfill(5)])
                except:
                    state.append(None)
            df2['STATE'] = state
            df2 = df2.set_index('YEAR', append = True)
            
            sales = imp.import_predefined_from_sql(target = 'VR', min_date = '2011-01-01')
            sales = sales.reset_index()
            sales = sales[['Postal_Code', 'STATE_ID', 'Registration_Date']]
            sales.columns = ['ZIPCODE', 'STATE', 'YEAR']
            sales2 = sales
            
            if drop_year != None:
                sales = sales.set_index(['YEAR'])
                sales = sales.drop(index = str(drop_year), level = 0)
                sales = sales.reset_index()
            if percentage == True:
                sales2 = sales2.set_index(['YEAR', 'ZIPCODE'])
                sales2 = sales2.drop('STATE', axis = 1)
                df3 = sales2.merge(df2_2, how = 'left', on = ['ZIPCODE', 'YEAR'])
            else:
                sales['YEAR'] = pd.to_datetime(sales['YEAR'], format = '%Y')
                sales = sales.set_index('YEAR')
                sales = sales.groupby('ZIPCODE').resample('Y').size()
                sales = sales.rename('UNITS_SOLD')
                
                df3 = df2.merge(sales, how = 'outer', on = ['ZIPCODE', 'YEAR'])
                df3['UNITS_SOLD'] = df3['UNITS_SOLD'].fillna(0)
    print('===========Successfully Manipulated The data==============')
        
    return df3

    

#This Function chooses the desired tables in the dataframe to use in correlation and produces correlatoin graphs
def corr_graph(data, anal, ptype, allstate = False):
    if anal == 2:
        log = True
    else:
        log = False
    series_name = ''
    #Selects the desired column to use
    print('please choose the data you wish to correlate with the sales in this Census\n')
    if ptype != 'p':
        i = 0
        data1 = data.drop('UNITS_SOLD', axis = 1)
        data1 = data1.drop('STATE', axis = 1)
        df_corr = pd.DataFrame(columns = ['r', 'p', 'state'])
        corr = {'r' : None, 'p' : None, 'state' : None}
        for column in data1:
            print(str(i)+' - '+column+'\n')
            i += 1
        select = int(input(': '))
        df = data[['UNITS_SOLD', data.columns[select], 'STATE']]
        df.sort_values(by = df.columns[2], axis = 0, inplace  = True)
    else:
        i = 0
        data1 = data.drop('ZIPCODE', axis = 1)
        data1 = data1.drop('YEAR', axis = 1)
        df_corr = pd.DataFrame(columns = ['r', 'p', 'state'])
        corr = {'r' : None, 'p' : None, 'state' : None}
        for column in data1:
            print(str(i)+' - '+column+'\n')
            i += 1
        select = int(input(': '))
        print(data1.columns[select])
        df = data1[data1.columns[select]]
        series_name = data1.columns[select]
    df = df.dropna()
    
    #indicates wich form to output data in
    plot_type = int(input('What form do you wish to have it in\n1 - Graph\n2 - Heatmap\n: '))
    
    
    if plot_type == 1 and allstate == False:
        #Cycles through states and plots one state at a time
        df_t = pd.DataFrame(columns = [])
        init_state = df.iloc[0]['STATE']
        x = []
        y = []
        for index in range(len(df)):
            if df.iloc[index]['STATE'] == init_state:
                if df.iloc[index][1] <= 100 and ptype == 'p':
                    if df.iloc[index][1] != 0.0:
                        if log == True:
                            if df.iloc[index][0] != None:
                                x.append(df.iloc[index][1])
                                y.append(df.iloc[index][0])
                        else:
                            x.append(df.iloc[index][1])
                            y.append(df.iloc[index][0])
                elif ptype == 'n':
                    if df.iloc[index][1] != 0.0:
                        if log == True:
                            if df.iloc[index][0] != None:
                                x.append(df.iloc[index][1])
                                y.append(df.iloc[index][0])
                        else:
                            x.append(df.iloc[index][1])
                            y.append(df.iloc[index][0])
            else:
                #Add exception not to graph
                if init_state not in EVIL_STATES:
                    if len(x) > 1:
                        if log == True:
                            for i in range(len(y)):
                                if y[i] != 0:
                                    y[i] = math.log(y[i])
                        df_t[df.columns[1]] = x
                        df_t[df.columns[0]] = y
                        #df_t = df_t[df_t[df_t.columns[0]] != 0]
                        x = []
                        y = []
                        # for i in range(len(df_t)):
                        #     if df_t.iloc[i][0] != 0 and df_t.iloc[i][1] != 0:
                        #         x.append(df_t.iloc[i][1])
                        #         y.append(df_t.iloc[i][0])
                        # df_t = pd.DataFrame(columns = [])
                        # df_t[df.columns[1]] = y
                        # df_t[df.columns[0]] = x
                        corr['r'], corr['p'] = mani.corr_results_census(data = df_t, state = init_state, ptype = ptype)
                        corr['state'] = init_state
                        df_corr = df_corr.append(corr, ignore_index = True)
                init_state = df.iloc[index]['STATE']
                x = []
                y = []
                df_t = pd.DataFrame(columns = [])
                x.append(df.iloc[index][1])
                y.append(df.iloc[index][0])
                
    elif plot_type == 1 and allstate == True:
        #Cycles through states and plots one state at a time
        if ptype == 'p':
            df.plot.density()
            plt.xlim([0, 100])
            plt.title(series_name)
            plt.xlabel('percentage')
            plt.show()
            print('**'+series_name+'**\n======================')
            print('**Distribution analysis**\n')
            print(df.describe())
            
            return df, df
        else:    
            df_t = pd.DataFrame(columns = [])
            x = []
            y = []
            for index in range(len(df)):
                if df.iloc[index][1] <= 100 and ptype == 'p':
                    if df.iloc[index][1] != 0.0:
                        if log == True:
                            if df.iloc[index][0] != None:
                                x.append(df.iloc[index][1])
                                y.append(df.iloc[index][0])
                        else:
                            x.append(df.iloc[index][1])
                            y.append(df.iloc[index][0])
                elif ptype == 'n':
                    if df.iloc[index][1] != 0.0:
                        if log == True:
                            if df.iloc[index][0] != None:
                                x.append(df.iloc[index][1])
                                y.append(df.iloc[index][0])
                        else:
                            x.append(df.iloc[index][1])
                            y.append(df.iloc[index][0])
            if len(x) > 1:
                if log == True:
                    for i in range(len(y)):
                        if y[i] != 0:
                            y[i] = math.log(y[i])
                df_t[df.columns[1]] = x
                df_t[df.columns[0]] = y
                #df_t = df_t[df_t[df_t.columns[0]] != 0]
                corr['r'], corr['r'] = mani.corr_results_census(data = df_t, state = 'US', ptype = ptype)
                corr['state'] = 'us'
                df_corr = df_corr.append(corr, ignore_index = True)
                
    return df, df_corr
    


def main(allstate = False):
    data_source, anal, data_type, yearly, percentage = menu()
    ptype = ''
    if percentage == True:
        ptype = 'p'
    else:
        ptype = 'n'
    df, df_corr = corr_graph(arrange_census_data(data_type, data_source,percentage, year_split = yearly), anal, ptype, allstate = allstate)
    if ptype != 'p': 
        if anal == 2:
            print('**Y = LOG(UNITS)===========================**')
        elif anal == 1:
            print('**Y = UNITS==============================**')
        if allstate == False:
            result = df_corr['r'][(df_corr['p']< 0.05) &(abs(df_corr['r']) > 0.5)].describe()
            result_all = df_corr['r'].describe()
            print(result_all)
            print('\nMASK (Corr >= 0.5 and p-value < 0.05 )\n')
            print(result)
        else:
            print('Bell curve analysis for the US')
            print(df[df.columns[1]].describe())
    return df, df_corr

#=============================================================================
#=========================Execution===========================================
#=============================================================================




#Formats the columns of CSV files properly for SQL
def sql_format():
    for path in csv_dataset:
        df = pd.read_csv(PATH+'/'+path)
        new_c = []
        for i in range(len(df.columns)):
            new = df.columns[i].upper()
            new = new.replace(' ', '_')
            new = new.replace('(', '')
            new = new.replace(')', '')
            new = new.replace('-', '_')
            new = new.replace(',', '_')
            new = new.replace('"', '')
            new = new.replace("'", "")
            new_c.append(new)
        df.columns = new_c
        
        df.to_csv(PATH+'/'+path, index = False)
        






df, df_corr = main(allstate = True)
#df_corr = corr_graph(arrange_census_data(csv_dataset[0], 'csv', percentage = True, year_split = True), anal = 0, ptype = 'p', allstate = True)
#df = arrange_census_data(csv_dataset[0], 'csv', percentage = True, drop_year = None, year_split = True)
# mask = (df["YEAR"] < 2019)
# df[df.columns[2]][mask].isna().sum() / len(df[df.columns[2]][mask])




#Male population by age
# df1 = pd.read_csv(PATH+'/'+csv_dataset[8])
# dfcol = df1.columns
# df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[20], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(18):
#     c = i+4
#     df3[df3.columns[c]] = df3[df3.columns[1]]*df3[df3.columns[c]]/100
# df3 = df3[dfcol]
# df3.to_csv(PATH+'/'+csv_dataset[8])

# #Class of worker
# df1 = pd.read_csv(PATH+'/'+csv_dataset[0])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(5):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# df3.to_csv(PATH+'/'+csv_dataset[0], index = False)

# # df = pd.read_csv(PATH+'/'+csv_dataset[8])
# # df = df.drop(df.columns[0], axis = 1)
# # df.to_csv(PATH+'/'+csv_dataset[8], index = False)

# #Education attainement
# df1 = pd.read_csv(PATH+'/'+csv_dataset[1])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(10):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[1], index = False)

# #Employement status
# df1 = pd.read_csv(PATH+'/'+csv_dataset[2])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(9):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[2], index = False)

# #Housing units
# df1 = pd.read_csv(PATH+'/'+csv_dataset[4])
# #dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# #df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# #df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# #df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# df3 = df1
# for i in range(2):
#     c = i+1
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# for i in range(9):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# #df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[4], index = False)

# #Housing Value
# df1 = pd.read_csv(PATH+'/'+csv_dataset[5])
# #dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# #df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# #df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# #df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# df3 = df1
# for i in range(9):
#     c = i+1
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# for i in range(2):
#     c = i+11
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# new_c = []
# for i in range(len(df3.columns)):
#     new = df3.columns[i].upper()
#     new = new.replace(' ', '_')
#     new = new.replace('(', '')
#     new = new.replace(')', '')
#     new = new.replace('-', '_')
#     new = new.replace(',', '_')
#     new = new.replace('"', '')
#     new = new.replace("'", "")
#     new = new.replace('$', '')
#     new_c.append(new)
# df3.columns = new_c
# #df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[5], index = False)

# #Income and Benefits
# df1 = pd.read_csv(PATH+'/'+csv_dataset[6])
# #dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# #df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# #df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# #df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# df3 = df1
# for i in range(15):
#     c = i+1
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# # for i in range(9):
# #     c = i+3
# #     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# new_c = []
# for i in range(len(df3.columns)):
#     new = df3.columns[i].upper()
#     new = new.replace(' ', '_')
#     new = new.replace('(', '')
#     new = new.replace(')', '')
#     new = new.replace('-', '_')
#     new = new.replace(',', '_')
#     new = new.replace('"', '')
#     new = new.replace("'", "")
#     new = new.replace('$', '')
#     new_c.append(new)
# df3.columns = new_c
# #df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[6], index = False)

# #Industry
# df1 = pd.read_csv(PATH+'/'+csv_dataset[7])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(14):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# # #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[7], index = False)

# #fix monthly morgage
# df1 = pd.read_csv(PATH+'/'+csv_dataset[10])
# df1[df1.columns[0]] = df1[df1.columns[1]]+df1[df1.columns[2]]+df1[df1.columns[3]]+df1[df1.columns[4]]+df1[df1.columns[5]]+df1[df1.columns[6]]+df1[df1.columns[7]]
# df1[df1.columns[9]] = df1[df1.columns[10]]+df1[df1.columns[11]]+df1[df1.columns[12]]+df1[df1.columns[13]]+df1[df1.columns[14]]+df1[df1.columns[15]]
# df1.to_csv(PATH+'/'+csv_dataset[10])

# #Monthly owner cost
# df1 = pd.read_csv(PATH+'/'+csv_dataset[10])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(14):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# for i in range(14):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# new_c = []
# for i in range(len(df3.columns)):
#     new = df3.columns[i].upper()
#     new = new.replace(' ', '_')
#     new = new.replace('(', '')
#     new = new.replace(')', '')
#     new = new.replace('-', '_')
#     new = new.replace(',', '_')
#     new = new.replace('"', '')
#     new = new.replace("'", "")
#     new = new.replace('$', '')
#     new_c.append(new)
# df3.columns = new_c
# #df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[10], index = False)

# #Occupation
# df1 = pd.read_csv(PATH+'/'+csv_dataset[12])
# dfcol = df1.columns
# #df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(6):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[12], index = False)

#Rooms
# df1 = pd.read_csv(PATH+'/'+csv_dataset[14])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# # df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# # df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# # df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(9):
#     c = i+1
#     df1[df1.columns[c]] = df1[df1.columns[c]]/df1[df1.columns[0]]*100
# for i in range(2):
#     c = i+11
#     df1[df1.columns[c]] = df1[df1.columns[c]]/df1[df1.columns[0]]*100
# df1 = df1[dfcol]
# # df3 = df3.drop(df3.columns[9], axis = 1)
# df1.to_csv(PATH+'/'+csv_dataset[14], index = False)

# #School enrollement
# df1 = pd.read_csv(PATH+'/'+csv_dataset[15])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(4):
#     c = i+3
#     df3[df3.columns[c]] = df3[df3.columns[c]]/df3[df3.columns[0]]*100
# df3 = df3[dfcol]
# # #df3 = df3.drop(df3.columns[9], axis = 1)
# df3.to_csv(PATH+'/'+csv_dataset[15], index = False)

#Vehicules available
# df1 = pd.read_csv(PATH+'/'+csv_dataset[19])
# dfcol = df1.columns
# # df1 = df1.drop(dfcol[19], axis = 1)
# # df2 = pd.read_csv(PATH+'/'+csv_dataset[13])
# # df2 = df2[[df2.columns[0], df2.columns[24], df2.columns[25]]]
# # df3 = df2.merge(df1, how = 'right', on = ['ZIPCODE', 'YEAR'])
# for i in range(4):
#     c = i+1
#     df1[df1.columns[c]] = df1[df1.columns[c]]/df1[df1.columns[0]]*100
# df1 = df1[dfcol]
# # #df3 = df3.drop(df3.columns[9], axis = 1)
# df1.to_csv(PATH+'/'+csv_dataset[19], index = False)

#######################################################
######################################################
#Fix the backwards shit going on with the files

#Education attainement
# df = pd.read_csv(PATH+'/'+csv_dataset[1])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2018]
# df2 = df[df.index.get_level_values('YEAR') > 2018]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 9
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3.to_csv(PATH+'/'+csv_dataset[1], index = False)

# #Housing Value
# df = pd.read_csv(PATH+'/'+csv_dataset[5])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2014]
# df2 = df[df.index.get_level_values('YEAR') > 2014]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 9
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3[df3.columns[10]] = df3[df3.columns[0]]
# df3.to_csv(PATH+'/'+csv_dataset[5], index = False)

# #Monthly owner cost Unable to reach conclusion based on the table, will have to look online
# df = pd.read_csv(PATH+'/'+csv_dataset[10])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2014]
# df2 = df[df.index.get_level_values('YEAR') > 2014]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 8
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3.to_csv(PATH+'/'+csv_dataset[10], index = False)

# #Rooms
# df = pd.read_csv(PATH+'/'+csv_dataset[14])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2014]
# df2 = df[df.index.get_level_values('YEAR') > 2014]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 10
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3.to_csv(PATH+'/'+csv_dataset[14], index = False)

# #School enrollement
# df = pd.read_csv(PATH+'/'+csv_dataset[15])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2018]
# df2 = df[df.index.get_level_values('YEAR') > 2018]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 3
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3.to_csv(PATH+'/'+csv_dataset[15], index = False)

# #Vehicules available
# df = pd.read_csv(PATH+'/'+csv_dataset[18])
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df[df.index.get_level_values('YEAR') <= 2014]
# df2 = df[df.index.get_level_values('YEAR') > 2014]
# df2 = df2.reset_index()
# df1 = df1.reset_index()
# df2 = df2[dfcol]
# df1 = df1[dfcol]
# maxi = 4
# x = df1[df1.columns[maxi]].copy()
# for i in range(maxi):
#     df1[df1.columns[maxi-i]] = df1[df1.columns[maxi-1-i]]
# df1[df1.columns[0]] = x
# df3 = df1.append(df2)
# df3.to_csv(PATH+'/'+csv_dataset[18], index = False)

#=========================================================
#remove percentages
#=========================================================
#Household characteristics Watch out must rechange the columns 8 to 16 if you are gonna reuse at some point
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Households_characteristics.csv')
# df[df.columns[5]] = df[df.columns[5]]*df[df.columns[4]]/100
# df[df.columns[6]] = df[df.columns[6]]*df[df.columns[4]]/100
# df[df.columns[7]] = df[df.columns[7]]*df[df.columns[4]]/100
# df[df.columns[8]] = df[df.columns[8]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[9]] = df[df.columns[9]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[10]] = df[df.columns[10]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[11]] = df[df.columns[11]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[12]] = df[df.columns[12]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[13]] = df[df.columns[13]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[14]] = df[df.columns[14]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[15]] = df[df.columns[15]]/df[df.columns[4]]*df[df.columns[0]]
# df[df.columns[16]] = df[df.columns[16]]/df[df.columns[4]]*df[df.columns[0]]
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Households_characteristics.csv', index = False)

#Male Population by age
# df1 = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Male_population_by_age.csv')
# df2 = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Population_by_age.csv')
# df1col = df1.columns
# df2col = df2.columns
# df1.sort_values(['ZIPCODE', 'YEAR'], axis = 0, inplace = True, ignore_index = True)
# df2.sort_values(['ZIPCODE', 'YEAR'], axis = 0, inplace = True, ignore_index = True)
# df3 = df1.set_index('YEAR')
# df3 = df3.drop([2011, 2012, 2013, 2014, 2015, 2016], axis = 0)
# df3 = df3.reset_index()
# df3 = df3[df1col]
# df1 = df1.set_index('YEAR')
# df1 = df1.drop([2017, 2018, 2019], axis = 0)
# df1 = df1.reset_index()
# df2 = df2.set_index('YEAR')
# df2 = df2.drop(2017, axis = 0)
# df2 = df2.reset_index()
# df1 = df1[df1col]
# df2 = df2[df2col]
# for i in range(18):
#     df1[df1.columns[i]] = df1[df1.columns[i]]*(df2[df2.columns[0]]*df2[df2.columns[20]]/100)/100
# df1 = df1.append(df3)
# df1.sort_values(['ZIPCODE', 'YEAR'], axis = 0, inplace = True, ignore_index = True)
# df1.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Male_population_by_age.csv', index = False)

#Maried_couple
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Maried_couple.csv')
# df[df.columns[5]] = df[df.columns[5]]*df[df.columns[4]]/100
# df[df.columns[6]] = df[df.columns[6]]*df[df.columns[4]]/100
# df[df.columns[7]] = df[df.columns[7]]*df[df.columns[4]]/100
# df[df.columns[8]] = df[df.columns[8]]*df[df.columns[0]]/100
# df[df.columns[9]] = df[df.columns[9]]*df[df.columns[0]]/100
# df[df.columns[10]] = df[df.columns[10]]*df[df.columns[0]]/100
# df[df.columns[11]] = df[df.columns[11]]*df[df.columns[0]]/100
# df[df.columns[12]] = df[df.columns[12]]*df[df.columns[0]]/100
# df[df.columns[13]] = df[df.columns[13]]*df[df.columns[0]]/100
# df[df.columns[14]] = df[df.columns[14]]*df[df.columns[0]]/100
# df[df.columns[15]] = df[df.columns[15]]*df[df.columns[0]]/100
# df[df.columns[16]] = df[df.columns[16]]*df[df.columns[0]]/100
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Maried_couple.csv', index = False)

#Non_family_households
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Non_family_households.csv')
# df[df.columns[5]] = df[df.columns[5]]*df[df.columns[4]]/100
# df[df.columns[6]] = df[df.columns[6]]*df[df.columns[4]]/100
# df[df.columns[7]] = df[df.columns[7]]*df[df.columns[4]]/100
# df[df.columns[8]] = df[df.columns[8]]*df[df.columns[0]]/100
# df[df.columns[9]] = df[df.columns[9]]*df[df.columns[0]]/100
# df[df.columns[10]] = df[df.columns[10]]*df[df.columns[0]]/100
# df[df.columns[11]] = df[df.columns[11]]*df[df.columns[0]]/100
# df[df.columns[12]] = df[df.columns[12]]*df[df.columns[0]]/100
# df[df.columns[13]] = df[df.columns[13]]*df[df.columns[0]]/100
# df[df.columns[14]] = df[df.columns[14]]*df[df.columns[0]]/100
# df[df.columns[15]] = df[df.columns[15]]*df[df.columns[0]]/100
# df[df.columns[16]] = df[df.columns[16]]*df[df.columns[0]]/100
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Non_family_households.csv', index = False)

#Population_by_age
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Population_by_age.csv')
# dfcol = df.columns
# df = df.set_index('YEAR')
# df1 = df.drop([2017, 2018], axis = 0)
# df1 = df1.reset_index()
# df1 = df1[dfcol]
# df2 = df.drop([2011, 2012, 2013, 2014, 2015, 2016], axis = 0)
# df2 = df2.reset_index()
# df2 = df2[dfcol]
# for i in range(18):
#     c = i+1
#     df1[df1.columns[c]] = df1[df1.columns[c]]*df1[df1.columns[0]]/100
# df = df1.append(df2)
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Population_by_age.csv', index = False)

# #Single dad
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Single_dad.csv')
# df[df.columns[5]] = df[df.columns[5]]*df[df.columns[4]]/100
# df[df.columns[6]] = df[df.columns[6]]*df[df.columns[4]]/100
# df[df.columns[7]] = df[df.columns[7]]*df[df.columns[4]]/100
# df[df.columns[8]] = df[df.columns[8]]*df[df.columns[0]]/100
# df[df.columns[9]] = df[df.columns[9]]*df[df.columns[0]]/100
# df[df.columns[10]] = df[df.columns[10]]*df[df.columns[0]]/100
# df[df.columns[11]] = df[df.columns[11]]*df[df.columns[0]]/100
# df[df.columns[12]] = df[df.columns[12]]*df[df.columns[0]]/100
# df[df.columns[13]] = df[df.columns[13]]*df[df.columns[0]]/100
# df[df.columns[14]] = df[df.columns[14]]*df[df.columns[0]]/100
# df[df.columns[15]] = df[df.columns[15]]*df[df.columns[0]]/100
# df[df.columns[16]] = df[df.columns[16]]*df[df.columns[0]]/100
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Single_dad.csv', index = False)


#Single mom
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Single_mom.csv')
# df[df.columns[5]] = df[df.columns[5]]*df[df.columns[4]]/100
# df[df.columns[6]] = df[df.columns[6]]*df[df.columns[4]]/100
# df[df.columns[7]] = df[df.columns[7]]*df[df.columns[4]]/100
# df[df.columns[8]] = df[df.columns[8]]*df[df.columns[0]]/100
# df[df.columns[9]] = df[df.columns[9]]*df[df.columns[0]]/100
# df[df.columns[10]] = df[df.columns[10]]*df[df.columns[0]]/100
# df[df.columns[11]] = df[df.columns[11]]*df[df.columns[0]]/100
# df[df.columns[12]] = df[df.columns[12]]*df[df.columns[0]]/100
# df[df.columns[13]] = df[df.columns[13]]*df[df.columns[0]]/100
# df[df.columns[14]] = df[df.columns[14]]*df[df.columns[0]]/100
# df[df.columns[15]] = df[df.columns[15]]*df[df.columns[0]]/100
# df[df.columns[16]] = df[df.columns[16]]*df[df.columns[0]]/100
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Single_mom.csv', index = False)

#Single mom
# df = pd.read_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Workers_16over_vehicule_availability.csv')
# df[df.columns[1]] = df[df.columns[1]]*df[df.columns[0]]/100
# df[df.columns[2]] = df[df.columns[2]]*df[df.columns[0]]/100
# df[df.columns[3]] = df[df.columns[3]]*df[df.columns[0]]/100
# df[df.columns[4]] = df[df.columns[4]]*df[df.columns[0]]/100
# df.to_csv('C:/Users/masn2601/Documents/Data/CENSUS_DATA/Workers_16over_vehicule_availability.csv', index = False)

