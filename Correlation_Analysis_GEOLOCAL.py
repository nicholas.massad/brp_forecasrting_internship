# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 11:16:13 2021

@author: bernida2
"""

# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
from zipcode_association import zip_dictionnary
from our_functions import registrations, dfmanipulations, ImpExp


import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import scipy.stats as stats
# from statsmodels.graphics.factorplots import interaction_plot
# import statsmodels.api as sm
# import statsmodels.formula.api as smf
# Connect to the SQL server
# import pyodbc as pc


# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

# =====================================================
# ----- Data Importation
# =====================================================

# Dataset in SQL
GEO = "geolocal"
VEHICLE_REGISTRATION = "VR"

# Put data from SQL into dataframes

# Get all the industry data
Geolocal = imp.import_predefined_from_sql(GEO)

VehicleRegistration = imp.import_predefined_from_sql(VEHICLE_REGISTRATION, min_date = "2010-01-01")


columns = ["OWNER_POSTAL", "CUST_DISTANCE_OWN", "AVG_DISTANCE_C" , "AVG_DISTANCE_D", "AVG_DISTANCE_T"]
levels = ["STATE_ID", "OWNER_POSTAL", "DATE"]
data_1 = Geolocal
data_2 = VehicleRegistration
set_data_2_count_column_name = "UNITS_SOLD"
data_2_count_on = "Postal_Code"
freq = "Y"

def units_dist(data_1, data_2, set_data_2_count_column_name, data_2_count_on, levels, columns, by_year = False, freq = "Y"):
    
  
    level_1 = levels[0]
    level_2 = levels[1]
    level_3 = levels[2]
    
    df = data_1[columns]
    
    if by_year == True:
        freq = "Y"
        
        
        print("Counting units sold per postal code")
        units_count = reg.count_to_df(data_2, freq = freq, on_col = data_2_count_on, index = [level_2, level_3], col = [set_data_2_count_column_name] )
  
        
        print("Getting average distance per owner")
        df = df.groupby(level_2).resample(freq).mean()
    
        df1 = df[:]
        print("Merging dataframes")
        df1 = df1.merge(units_count, how = "left", on = [level_2, level_3])
        
        
        print("Creating state indexes")
        zipcodes = pd.DataFrame.from_dict(zip_dictionnary, orient = "index")
        zipcodes.index.names = [level_2]
        zipcodes.columns = [level_1]
        
        print("Joining zipcode to the dataframe")
        df1 = df1.join(zipcodes, how = "left", on = [level_2])
        
        
        print("Reorganizing indexes")
        df1.set_index(level_1, append = True, inplace = True)
        df1 = df1.reorder_levels([level_1, level_2, level_3])
        
        print("Reorganizinf columns")
        cols = df1.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        
        print("Sorting by :" + level_1)
        df2 = df1[cols]
        df2 = df2.sort_index(level = level_1)
        df2 = df2.drop(index = ['HI', 'AK'])

    if by_year == False:
        df = data_1[columns]

        print("Counting units sold per postal code")
        units_count = reg.count_to_df(data_2, freq = freq, on_col = data_2_count_on, index = [level_2, level_3], col = [set_data_2_count_column_name])
        test = units_count[:]
        test = test.groupby(level_2).sum()
        
        
        print("Getting average distance per owner")
        df = df.groupby(level_2).mean()
        
        
        df1 = df[:]
        print("Merging dataframes")
        df1 = df1.merge(test, how = "left", on = [level_2])
        
        
        print("Creating state indexes")
        zipcodes = pd.DataFrame.from_dict(zip_dictionnary, orient = "index")
        zipcodes.index.names = [level_2]
        zipcodes.columns = [level_1]
        
        print("Joining zipcode to the dataframe")
        df1 = df1.merge(zipcodes, how = "left", on = [level_2])
        
        
        print("Reorganizing indexes")
        df1.set_index(level_1, append = True, inplace = True)
        df1 = df1.reorder_levels([level_1, level_2])
        
        print("Reorganizinf columns")
        cols = df1.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        
        print("Sorting by :" + level_1)
        df2 = df1[cols]
        df2 = df2.sort_index(level = level_1)
        df2 = df2.drop(index = ['HI', 'AK'])  
    
    return df2


df = units_dist(data_1, data_2, set_data_2_count_column_name, data_2_count_on, levels, columns)

df_final = df.copy()

test = df_final.columns[1]

df_final = df_final[df_final[test] > 5]


df_test = df_final[:]
df_test["UNITS_SOLD"] = df_final["UNITS_SOLD"].apply(np.log)
df_test = df_test[df_test["UNITS_SOLD"]!=0]

# Get the log distance
# df_test[test] = df[test].apply(np.log)
# df_test = df_test[df_test[test] > 0]


results = mani.corr_results(df_final, "STATE_ID", plot_col = [test], graphs = True)
results = mani.corr_results(df_test, "STATE_ID", plot_col = [test], graphs = True)


results[(results["P_" + test]< 0.05) &(abs(results["CORR_" + test]) > 0.5)].count()


results = mani.corr_results(df, "STATE_ID", plot_col = None, graphs = True)






# Get density plots of units sold

for state in df_test.index.get_level_values("STATE_ID").unique():
    dense = df_test["UNITS_SOLD"][df_test.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.show()
    
# for state in df_test.index.get_level_values("STATE_ID").unique():
#     dense = df_test["UNITS_SOLD"][df_test.index.get_level_values("STATE_ID") == state].apply(np.log10)
#     dense.plot.density()
#     plt.show()

for state in df_test.index.get_level_values("STATE_ID").unique():
    dense = df_test["CUST_DISTANCE_OWN"][df_test.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.xlim(0,100)
    plt.show()