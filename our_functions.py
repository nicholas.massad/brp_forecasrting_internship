# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 09:43:29 2021

@author: bernida2
"""

# Import libraries
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import scipy.stats as stats
import numpy as np
import os
import pyodbc as pc

class registrations():
    
    
    def __init__(self):
        return
    
    def registrations_count(self, data, freq = "W", column = None, rows = None, start = None, end = None):
        """
    
        In order to run this function, you need to have pandas library installed
    
        Parameters
        ----------
        data : Series
            Serie having dates as index
            
        freq : string, optional
            Frequency you want the column to be grouped by, if no frequency is specified, it is weekly by default
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        column : string, optional
            Name of the column you want to group by (ex: Postal Code / State / Etc.)
            The default is None
            
        row : string, optional
            Sepecify a zone (Precise zone to look at (ex : row = "Alabama" or row = "99654"))
            The default is None
            
        start : datetime, optional
            Start of the window (included)
            The default is None
        
        end : datetime, optional
            End of the window (included)
            The default is None
            
        detail : boolean, optional
            Add indexes for the desired frequency (freq)
            The default is None
        
        Returns
        -------
        z : Series
            Return a serie with 2 or more index. The first index is the date, then the frequency and the last is the zone. 
            It returns the number of registration
    
        """
        self.data = data
        self.freq = freq
        self.column = column
        self.rows = rows
        self.start = start
        self.end = end
        
        if start != None:
            data = data[data.index >= start]
        if end != None:
            data = data[data.index <= end]
        
        # Details about frequency in the index

        if column == None: 
            z = data.resample(freq).size()
            # Rename index columns
            z.index.names = ["Date"]
        
        else :
            try:
                if freq != "Y":
                    z = data.groupby(column).resample(freq).size()
                    # Rename index columns
                    z.index.names = [column,"Date"] 
                else:
                    z = data.groupby(column).resample(freq).size()
                    z = data.groupby(column)[data.columns[0]].resample(freq).size()
                    # Rename index columns
                    z.index.names = [column,"DATE"] 
            except:
                z = data.groupby(column).resample(freq).size()
                z = z.stack()
                # Rename index columns
                z.index.names = [column,"Date"]
        # Get a specific zone in the specified column  
        if rows != None:
            z = z.loc[(z.index.get_level_values(column) == rows)]     
    
        return z
    
    
    
    def count_to_df(self, data, freq, on_col, index, col):
        """
        
    
        Parameters
        ----------
        data : dataframe
            source dataframe
        freq : string
            frequency to agregate the data ("D", "W", "M", "Y")
        on_col : string
            Column name that need to be agregated
        index : list, string
            List of string containing the indexes name you want to have (multiIndex)
        col : string, list
            Name of the counted column in a list : ex : ["COUNT_YOURCOUNT"]
    
        Returns
        -------
        df : TYPE
            DESCRIPTION.
    
        """
        
        df = self.registrations_count(data, freq = freq, column = on_col)
        df.index.names = index
        df = pd.DataFrame(df)
        df.columns = col
        return df
    

    def merge_categories(self, target_df, target_index, src_df, src_column, freq = "M", rename_src_col = None):
        """
        This function count each categorie of a column from a source dataframe and merge it to your target dataframe based on an index
        
        Parameters
        ----------
        target_df : dataframe
            This is the dataframe that you want the data to be added to.
        target_index : string
            This is the index that you want the count to be calculated on (ex : STATE_ID).
        src_df : dataframe
            This is the source dataframe that you will count on
        src_column : string
            This is the column with different categorie in youre source dataframe.
        freq : TYPE, optional
            This is the frequency of the count, it can be on "D", "W", "M" or "Y". The default is "M".
        rename_src_col : TYPE, optional
            This is the name of the new columns that you will add to youre target dataframe. 
            ex : 
                rename_src_col = "Total_count"
                src_column = "potatoes" (lets say there is 2 categories : White, Red)
                First new column in target_df = Total_count_White
                Second new column in target_df = Total_count_Red
            The default is None.

        Returns
        -------
        target_df : dataframe
            Returns a dataframe with new columns.

        """
        
        if rename_src_col != None:
            rename_src_col = rename_src_col
            
        if rename_src_col == None:
            rename_src_col = src_column
            
        for cat in src_df[src_column].unique():
            new_col = (rename_src_col + "_" + cat).upper()
            indexes = target_df.index.names
            col = self.count_to_df(src_df[src_df[src_column] == cat], freq = "M", on_col = target_index, index = indexes, col = [new_col])
            target_df = target_df.merge(col, how = "left" , on = indexes)
        return target_df  
    
    
    def ts_plot(self, data, list_freq = "W", column = None, list_rows = None, start = None, end = None, style = "line", theme = "darkgrid", palette = "crest", ci = False):
        
        """
    
        Parameters
        ----------
        data : Series
            Series having dates as index
            
        list_freq : string, optional
            Define the frequency of the time series. It can be a list or a scalar
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        list_column : string, optional
            Define the columns to plot, it can be a list or a single string
            The default is None
        
        list_rows : string, optional
            Sepecify a zone (Precise zone to look at (ex : "Alabama", "99654", "ALL"))
            It can be a single zone or multiple zones
            The default is None
        
        start : datetime, optional
            Start of the window (included)
            The default is None
        
        end : datetime, optional
            End of the window (included)
            The default is None
        
        style : string, optional
            Defines the plot style, it can be "line" or "scatter"
            The default is "line"
            
        theme : string, optional
            Defines the plot theme, it can be "white", "dark", "whitegrid" or "darkgrid"
            The default is "darkgrid"
            
        palette : string, optional
            Defines the line colors of your plot
            The default is "crest".
        
        Returns
        -------
        Plot
    
        """ 
        
        self.data = data
        self.list_freq = list_freq
        self.column = column
        self.list_rows = list_rows
        self.start = start
        self.end = end
        self.style = style
        self.theme = theme
        self.palette = palette
        
        
        if style == "line":
            style = sns.lineplot
        if style == "scatter":
            style = sns.scatterplot
        
        # set theme
        sns.set_theme(style = theme)
        
        # Set color palette
        sns.set_palette(sns.color_palette(palette))
        
        
        if column == None: 
            for i in list_freq:
                ts = self.registrations_count(data, freq = i, start = start, end = end)
                style(data = ts, ci = ci)
        # Plot format
            # Title
                if i == "D":
                    t = "Daily SSV sales"
                if i == "W":
                    t = "Weekly SSV sales"
                if i == "M":
                    t = "Monthly SSV sales"
                if i == "Y":
                    t = "Yearly SSV sales"
                plt.title(t)
            # Axis
                plt.ylabel("# of units")
            # Show plot
                plt.show()
                
        if column != None and list_rows == "ALL":
            list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
            for j in list_rows:
                for i in list_freq:
                    ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                    ts = ts.droplevel(column)
                    style(data = ts, ci = ci)
            # Plot format
                # Title
                    if i == "D":
                        t = "Daily SSV sales in " + str(column) + " : " + str(j)
                    if i == "W":
                        t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                    if i == "M":
                        t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                    if i == "Y":
                        t = "Yearly SSV sales in " + str(column) + " : " + str(j)
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Show plot
                    plt.show()
        
        if column != None and type(list_rows) != list and list_rows != "ALL":
            for i in list_freq:
                ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                ts = ts.droplevel(column)
                style(data = ts, ci = ci)
        # Plot format
            # Title
                if i == "D":
                    t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "W":
                    t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "M":
                    t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                if i == "Y":
                    t = "Yearly SSV sales in " + str(column) + " : " + str(list_rows)
                plt.title(t)
            # Axis
                plt.ylabel("# of units")
            # Show plot
                plt.show()
    
        if column != None and type(list_rows) == list:
            for j in list_rows:
                for i in list_freq:
                    ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                    ts = ts.droplevel(column)
                    style(data = ts, ci = ci)
            # Plot format
                # Title
                    if i == "D":
                        t = "Daily SSV sales in " + str(column) + " : " + str(j)
                    if i == "W":
                        t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                    if i == "M":
                        t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                    if i == "Y":
                        t = "Yearly SSV sales in " + str(column) + " : " + str(j)
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Show plot
                    plt.show()
                    
        return
    
    
    
    def seasonal_plot(self, data, list_freq = "W", column = None, list_rows = None, start = None, end = None, style = "line", theme = "darkgrid", palette = "crest", sequence = False, ci = None):
       
        """
        
        Parameters
        ----------
        data : Series
            Series having dates as index
            
        list_freq : string or list, optional
            Define the frequency of the time series. It can be a list or a scalar
            Possible freq : "Y" = year, "M" = month, "W" = week, "D" = day
            The default is "W"
            
        list_column : string, optional
            Define the column to plot
            The default is None.
        
        list_rows : string or list, optional
            Sepecify a zone (Precise zone to look at (ex : "Alabama", "99654", "ALL"))
            It can be a single zone or multiple zones
            The default is None.
        
        start : datetime, optional
            Start of the window (date included)
            The default is None.
        
        end : datetime, optional
            End of the window (date included)
            The default is None.
        
        style : string, optional
            Defines the plot style, it can be "line" or "scatter"
            The default is "line".
            
        theme : string, optional
            Defines the plot theme, it can be "white", "dark", "whitegrid" or "darkgrid"
            The default is "darkgrid".
        
        palette : string, optional
            Defines the line colors of your plot
            The default is "crest".
        
        sequence : boolean, optional
            Gives different layout to vizualise the graph
            The default is False.
    
        Returns
        -------
        Plot
    
        """ 
        self.data = data
        self.list_freq = list_freq
        self.column = column
        self.list_rows = list_rows
        self.start = start
        self.end = end
        self.style = style
        self.theme = theme
        self.palette = palette
        self.sequence = sequence
        self.ci = ci
        
        # Set graph theme
        sns.set_theme(style = theme)
    
        # Set color palette
        sns.set_palette(sns.color_palette(palette))
    
        # Take out "Y" from list_freq and create a temporary list
        list_freq_temp = []
        list_freq_temp = list_freq[:]    
        if "Y" in list_freq_temp:
            list_freq_temp.remove("Y")
        
        if sequence == False :
            # Set graphic style
            if style == "line":
                style = sns.lineplot
            if style == "scatter":
                style = sns.scatterplot
        
            if column == None: 
                for i in list_freq_temp:
                    if i == "D":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                        # Plot title
                        t = "Daily SSV sales"
                        # Plot xlabel
                        plt.xlabel("Day")
                    if i == "W":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Weekly SSV sales"
                        # Plot xlabel
                        plt.xlabel("Week")
                    if i == "M":   
                        ts = self.registrations_count(data, freq = i, start = start, end = end)
                        style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Monthly SSV sales"
                        # Plot xlabel
                        plt.xlabel("Month")
            # Plot format
                # Title
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Legend
                    plt.legend(bbox_to_anchor = (1.05,1))
                # Show plot
                    plt.show()
        
            if column != None and list_rows == "ALL":
                list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
                for j in list_rows:
                    for i in list_freq_temp:
        
                        if i == "D" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Day")
                        if i == "W" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Week")
                        if i == "M" :
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Month")
                            
                # Plot format
                    # Title
                        plt.title(t)
                    # Axis
                        plt.ylabel("# of units")
                    # Legend
                        plt.legend(bbox_to_anchor = (1.05,1))
                    # Show plot
                        plt.show()
        
            if column != None and type(list_rows) != list and list_rows != "ALL":
                for i in list_freq_temp:
                    if i == "D":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                        # Plot title
                        t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Day")
                    if i == "W":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Week")
                    if i == "M":
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
                        style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                        # Plot title
                        t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Plot xlabel
                        plt.xlabel("Month")
            # Plot format
                # Title
                    plt.title(t)
                # Axis
                    plt.ylabel("# of units")
                # Legend
                    plt.legend(bbox_to_anchor = (1.05,1))
                # Show plot
                    plt.show()
        
            if column != None and type(list_rows) == list:
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.day, y  = ts.values, hue = ts.index.year, palette = palette)
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Day")
                        if i == "W":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.week, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Week")
                        if i == "M":
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            style(data = ts, x = ts.index.month, y  = ts.values, hue = ts.index.year, palette = palette, ci = ci)
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Plot xlabel
                            plt.xlabel("Month")
                # Plot format
                    # Title
                        plt.title(t)
                    # Axis
                        plt.ylabel("# of units")
                    # Legend
                        plt.legend(bbox_to_anchor = (1.05,1))
                    # Show plot
                        plt.show()
    
    
    
    #############################################################################################################################
    #                                        Seasonal graph with sequence
    #############################################################################################################################
    
        if sequence == True :
            if column == None: 
                     for i in list_freq_temp:
                            if i == "D":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
            
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
            
                                # Plot title
                                t = "Daily SSV sales"
                                # Axis labels
                                g.set_axis_labels("Day", "# of units")
                                
                            if i == "W":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
                                # Plot title
                                t = "Weekly SSV sales"                    
                                # Axis labels
                                g.set_axis_labels("Week", "# of units")
            
            
                            if i == "M":  
                                # Set time series data
                                ts = self.registrations_count(data, freq = i, start = start, end = end)
                                
                                g = sns.relplot(
                                    data=ts,
                                    x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                    kind=style, palette=palette, linewidth=4, zorder=5,
                                    col_wrap=1, height=4, aspect=4, legend=False,
                                )
                                
                                
                                # Iterate over each subplot to customize further
                                for year, ax in g.axes_dict.items():
            
                                    # Add the title as an annotation within the plot
                                    ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                                
                                    # Plot every year's time series in the background
                                    sns.lineplot(
                                        data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                        estimator=None, color=".5", linewidth=1, ax=ax,
                                    )
                                # Plot title
                                t = "Monthly SSV sales"
                                # Axis labels
                                g.set_axis_labels("Month", "# of units")  
                                
                                
                            # Title
                            g.set_titles(t)
                            # Layout
                            g.tight_layout()
    
            
            if column != None and list_rows == "ALL":
                list_rows = self.registrations_count(data, column = column, start = start, end = end).index.get_level_values(column).unique()
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Day", "# of units")
    
    
                        if i == "W" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Week", "# of units")
                            
                            
                        if i == "M" :
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)
                            # Axis labels
                            g.set_axis_labels("Month", "# of units")
                            
                        # Title
                        g.set_titles(t)
                        # Layout
                        g.tight_layout()
    
            if column != None and type(list_rows) != list and list_rows != "ALL":
                for i in list_freq_temp:
                    
                    if i == "D":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Daily SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Day", "# of units")                   
    
                    if i == "W":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Weekly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Week", "# of units")                   
                                            
                    if i == "M":
                        # Get time series data
                        ts = self.registrations_count(data, freq = i, column = column, rows = list_rows, start = start, end = end)
                        ts = ts.droplevel(column)
          
                        # Create graph
                        g = sns.relplot(
                            data=ts,
                            x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                            kind=style, palette=palette, linewidth=4, zorder=5,
                            col_wrap=1, height=4, aspect=4, legend=False,
                        )
                        
                        
                        # Iterate over each subplot to customize further
                        for year, ax in g.axes_dict.items():
        
                            # Add the title as an annotation within the plot
                            ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                        
                            # Plot every year's time series in the background
                            sns.lineplot(
                                data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                estimator=None, color=".5", linewidth=1, ax=ax,
                            )
        
                        # Plot title
                        t = "Monthly SSV sales in " + str(column) + " : " + str(list_rows)
                        # Axis labels
                        g.set_axis_labels("Month", "# of units")    
                        
         
                    
                    
            if column != None and type(list_rows) == list:
                for j in list_rows:
                    for i in list_freq_temp:
                        if i == "D":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.day, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.day, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Daily SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Day", "# of units")   
                            
                        if i == "W":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.week, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.week, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Weekly SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Week", "# of units")           
                            
                        if i == "M":
                            # Get time series data
                            ts = self.registrations_count(data, freq = i, column = column, rows = j, start = start, end = end)
                            ts = ts.droplevel(column)
    
                            # Create graph
                            g = sns.relplot(
                                data=ts,
                                x=ts.index.month, y=ts.values, col = ts.index.year, hue=ts.index.year,
                                kind=style, palette=palette, linewidth=4, zorder=5,
                                col_wrap=1, height=4, aspect=4, legend=False,
                            )
                            
                            
                            # Iterate over each subplot to customize further
                            for year, ax in g.axes_dict.items():
            
                                # Add the title as an annotation within the plot
                                ax.text(.9, .85, year, transform=ax.transAxes, fontweight="bold")
                            
                                # Plot every year's time series in the background
                                sns.lineplot(
                                    data=ts, x=ts.index.month, y=ts.values, units=ts.index.year,
                                    estimator=None, color=".5", linewidth=1, ax=ax,
                                )
            
                            # Plot title
                            t = "Monthly SSV sales in " + str(column) + " : " + str(j)   
                            # Axis labels
                            g.set_axis_labels("Month", "# of units")  
                            
                        # Title
                        g.set_titles(t)
                        # Layout
                        g.tight_layout()             
        
        return


class dfmanipulations:
    
    
    def __init__(self):
        return
    
    def lag_columns(self,data, level, lag, first_col = None, last_col = None, drop = False):
        """
        This function aims to apply a lag on multiple column on a MultiIndex Dataframe (pandas) by creating new columns to the dataframe

        Parameters
        ----------
        data : pandas DataFrame
            data takes a multiindex dataframe as input.
            
        level : string
            Level on which to apply the lag on
        
        lag : int
            Number of lag, it can be a positive or a negative value.
        
        first_col : integer, optional
            This is the first column to apply the lag on. 
            The default is None.
        
        last_col : integer, optional
            This is the last column to apply the lag on. 
            The default is None.
        
        drop : boolean, optional
            True if we want to drop rows with NA values (created by the lag), False will keep those rows.
            The default is True.

        Returns
        -------
        data : TYPE
            DESCRIPTION.

        """
        # Declare variables
        self.data = data
        self.level = level
        self.lag = lag
        self.first_col = first_col
        self.last_col = last_col
        self.drop = drop
        
        data1 = data.copy()  
            
        # Identify the columns to be lagged
        if first_col == None:
            first_col = 1
        if last_col == None:
            last_col = len(data1.columns)
        
        # Get all zones
        level_list = data1.index.get_level_values(level).unique()
        
        # For all the column to be lagged create a new lagged column
        for c in data1.columns[first_col:last_col]:
            column_name = ''
            column_name = str("LAG_"+str(lag)+"_"+c)
    
            list_lagged = []
            for z in level_list:
                list_lagged.append(data1[c][data1.index.get_level_values(level) == z].shift(lag))
            
            list_final = []
            for x in list_lagged:
                list_final.extend(x)
            data1[column_name] = list_final
            
        if drop == True:
            data1 = data1.dropna(how = "any", axis = 0)
            
        return data1



    def corr_results(self, data, level, plot_col = None, graphs = True):
        """
        This function aims to return the correlation and p-value in a MultiIndex dataframe

        Parameters
        ----------
        data : dataframe (pandas)
            dataframe on wich you want to calculate the correlations, 
            the first column should ALWAYS be your reference value which you will use to compare correlations with other columns
            
        level : string
            Index level on which you want to test the correlation.
            
        plot_col : list, string, optional
            Specific column that you want to be plotted
            
        graphs : bool, optional
            Returns the correlation graphs for all the levels. The default is True.

        Returns
        -------
        corr_results : TYPE
            DESCRIPTION.

        """
        self.data = data
        self.level = level
        self.plot_col = plot_col
        self.graphs = graphs
        
        
        # Set graph theme
        sns.set_theme(style = "darkgrid")
       
        # Level treatment (has to be par of a multiindex dataframe)
        level_list = data.index.get_level_values(level).unique()
        
        # Create a column with the index as a new column
        column_names = [level]
        
        # Create columns containing correlation and p-values
        for c in data.columns[1:]:
            column_names += ["CORR_"+c, "P_"+c]
        
        # Create the DataFrame that will contain correlation results
        corr_results = pd.DataFrame(columns = column_names)
        corr_results[level] = level_list
        
        # For each element of the level (for instance the STATE_ID)
        for z in level_list:
            # Get the column to correlate the first column with (data.columns[0])
            for measure in data.columns[1:]:
                # Values to test the correlation
                m = data.columns[0]
                
                # Temporary dataframe
                data_temp = data.loc[(data.index.get_level_values(level) == z),:]
            
                x_temp = data_temp[measure][data_temp[m] != '']
                y_temp = data_temp[m][data_temp[m] != '']
                
                # Taking care of NaNs values
                temp = pd.DataFrame()
                temp[m], temp[measure] = y_temp, x_temp
                temp = temp.dropna()
                
                # Defining our x and y
                x = temp[measure]
                y = temp[m]
                 
                # Calculate the correlation
                corr = stats.pearsonr(x,y)
                
                # Graph creation
                if graphs == True:
                    if plot_col != None:
                        for col in plot_col:
                            if col == measure:
                                
                                x_plot_temp = data_temp[col][data_temp[m] != '']
                                y_plot_temp = data_temp[m][data_temp[m] != '']
                                
                                # Taking care of NaNs values
                                temp = pd.DataFrame()
                                temp[m], temp[col] = y_plot_temp, x_plot_temp
                                temp = temp.dropna()
                                
                                # Defining our x and y
                                x_plot = temp[col]
                                y_plot = temp[m]
                                
                                sns.scatterplot(x = x_plot, y = y_plot, palette = "crest", color = "black")
                                m, b = np.polyfit(x_plot, y_plot, 1)
            
                                plt.plot(x_plot, m*x_plot + b, color = "darkgoldenrod")
                                
                                plt.title("Monthly correlation between "+measure+ " and "+str(data.columns[0])+" for: "+z + "\n Correlation :" + str(corr))
                                
                                plt.show()
                    else:
                        sns.scatterplot(x = x, y = y, palette = "crest", color = "black")
                        m, b = np.polyfit(x, y, 1)
    
                        plt.plot(x, m*x + b, color = "darkgoldenrod")
                        
                        plt.title("Yearly correlation between "+measure+ " and "+str(data.columns[0])+" for: "+z + "\n Correlation :" + str(corr))
                        
                        plt.show()
                
                # For every column of the initial data frame, insert a CORR and a P-VALLUE column
                corr_results["CORR_"+measure][corr_results[level]==z] =  corr[0]
                corr_results["P_"+measure][corr_results[level]==z] =  corr[1]
                    
                
        return corr_results

    def corr_results_census(self, data, state, graphs = True, ptype = 'n'):
        """
        This function aims to return the correlation and p-value in a MultiIndex dataframe
         
        Parameters
        ----------
        data : dataframe (pandas)
            dataframe on wich you want to calculate the correlations, 
            the first column should ALWAYS be your reference value which you will use to compare correlations with other columns
            
        level : string
            Index level on which you want to test the correlation.
            
        plot_col : list, string, optional
            Specific column that you want to be plotted
            
        graphs : bool, optional
            Returns the correlation graphs for all the levels. The default is True.
            
        ptype: str
            Sets x axis from 0 to 100 if equal to p. works for percentage values
         
        Returns
        -------
        corr_results : TYPE
            DESCRIPTION.
         
        """
        self.data = data
        self.state = state
        self.graphs = graphs
        
        
        # Set graph theme
        sns.set_theme(style = "darkgrid")
        
        
        #column_names = ["CORR_"+data.columns[0], "P_"+data.columns[0]]
        
        # Create the DataFrame that will contain correlation results
        #corr_results = pd.DataFrame(columns = column_names)

        x = data[data.columns[0]]
        y = data[data.columns[1]]
        # x = np.log(x)
        # for value in range(len(x)):
        #     if x[value] < 0 or x[value] == None:
        #         x[value] = 0
        # Calculate the correlation
        corr = stats.pearsonr(x,y)
        # Graph creation
        if graphs == True:
            sns.scatterplot(x = x, y = y, palette = "crest", color = "black")
            m, b = np.polyfit(x, y, 1)
    
            plt.plot(x, m*x + b, color = "darkgoldenrod")
            
            plt.title("correlation between "+data.columns[1]+ " and "+str(data.columns[0])+" for: "+ state + "\n Correlation :" + str(corr))
            
            if ptype == 'p':
                plt.xlim([0, 100])
            plt.show()
         
        # For every column of the initial data frame, insert a CORR and a P-VALLUE column
        #corr_results["CORR_"+data.columns[0]].append(corr[0])
        #corr_results["P_"+data.columns[0]].append(corr[1])
        #print('\n'+str(state)+' Correlation : '+str(corr[0])+' P_Value : '+str(corr[1]))
        #print(corr_results)
              
        return corr[0], corr[1]   
    
class ImpExp:

    def __init__(self):
        return

    #Path for folder containing all the Census CSV files
    CENSUS_CSV_PATH = "C:/Users/masn2601/Downloads/CENSUS_DATA"
    
    
    
    #This function modifies CENSUS csv files to put them in proper format
    def census_format(self, dir_path):
        '''
        Parameters
        ----------
        dir_path : STR
            the path for the directory containing the csv files
        files : os list
            file name
    
        '''
        #gets all the names of the CSV files
        files = os.listdir(self.CENSUS_CSV_PATH)
    
        #loops through all the files and modifies them appropriately and overwrites them    
        for f in files:
            df = pd.read_csv(dir_path+'/'+f)
            
            #renames columns by removing () and replaceing ' ' and '-' by '_'
            new_columns = []
            for name in df.columns:
                rename = name
                rename = rename.replace('(', '')
                rename = rename.replace(')', '')
                rename = rename.replace('-', '_')
                new_columns.append(rename.upper().replace(' ', '_'))
            df.columns = new_columns
            
            #Overwrites files
            df.to_csv(dir_path+'/'+f, index = False)
        
        return
        
    
    #imports datasets for useage in time series from either Industry, Vehicule_registration or dealers
    #Target = 'ind' for Industry
    #Target = 'VR' for the Vehicule_Registration
    #Target = 'deal' for the Dealers
    #Target = 'meteo'
    #Tarfet = "leads" for leads
    def import_predefined_from_sql(self, target, min_date = None):
        target = target
        min_date = min_date
        
        # Define the SQL server
        SQL_server = "cavlsqlpd2\pbi2"
        
        print("Connecting to server: "+SQL_server)
                
        # Connection data to SQL server
        cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';Trusted_Connection=yes;')
        
        if target == 'VR':
            
            if min_date == None:
                print("Please insert a min_date")
            
            # Start task timer
            startTime = pd.Timestamp.now()
            
    
            print("Gathering from: Vehicle Registrations")
            df = pd.read_sql(f"""
                                SELECT
                                	Division,
                                	Model_number,
                                	Usage_code,
                                	Owner_No AS OWNER_NUMBER,
                                	LEFT(Postal_Code,5) as Postal_Code,
                                	City,
                                	Region AS 'STATE_ID',
                                	Registration_Date,
                                	CUSTOMER_NUMBER,
                                	REG_DEALER_NUMBER
                                FROM 
                                    Market_Planning.dbo.MKTPL_ORIGINAL_OWNER
                                WHERE 
                                    	Country = 'US' 
                                    AND Division = '91' 
                                    AND Registration_Date >= '{min_date}'
                                    AND Region <> '' 
                                    AND Region NOT IN ('AK', 'HI', 'GU', 'VI', 'PR', 'AS', 'DC')
                                ORDER BY 
                                	Registration_Date
                                """
                              ,cnxn ,parse_dates=["Registration_Date"], index_col = "Registration_Date"
                             )
            
            
            # Set columns datatype 
            df["Division"] = df["Division"].astype(int)
            df["Model_number"] = df["Model_number"].astype(str)
            df["Usage_code"] = df["Usage_code"].astype(str)
            df["OWNER_NUMBER"] = df["OWNER_NUMBER"].astype(str)
            df["Postal_Code"] = df["Postal_Code"].astype(str)
            df["City"] = df["City"].astype(str)
            df["STATE_ID"] = df["STATE_ID"].astype(str).str.strip()
            df["CUSTOMER_NUMBER"] = df["CUSTOMER_NUMBER"].astype(str)
            df["REG_DEALER_NUMBER"] = df["REG_DEALER_NUMBER"].astype(str)
         
            
            #Print task completion time: 
            print("Gathering from: dbo.MKTPL_ORIGINAL_OWNER | completion time: " + str(pd.Timestamp.now() - startTime))
        
        if target == 'deal':
            
            # Start task timer
            startTime = pd.Timestamp.now()
    
            print("Gathering data from: Dealers")
            
            df = pd.read_sql("""
                                SELECT 
                                	DEAL.DEALER_NUMBER,
                                	DEAL.CITY,
                                	DEAL.[STATE],
                                	DEAL.COUNTRY,
                                	ACTIV.DEALER_INCEPTION_DATE,
                                	ACTIV.DEALER_STATUS,
                                	IIF(DEAL.SKIDOO_FLAG='X',1,0) AS SKIDOO_FLAG,
                                	IIF(DEAL.SEADOO_FLAG='X',1,0) AS SEADOO_FLAG,
                                	IIF(DEAL.SPORTBOAT_FLAG='X',1,0) AS SPORTBOAT_FLAG,
                                	IIF(DEAL.ATV_FLAG='X',1,0) AS ATV_FLAG,
                                	IIF(DEAL.NEV_FLAG='X',1,0) AS NEV_FLAG,
                                	IIF(DEAL.ROADSTER_FLAG='X',1,0) AS ROADSTER_FLAG,
                                	IIF(DEAL.SSV_FLAG='X',1,0) AS SSV_FLAG
                                FROM 
                                	MasterData.dbo.MD_DEALER_INFO DEAL
                                LEFT JOIN
                                	NetDev.dbo.ACTIVE_DEALER_LIST ACTIV
                                ON	ACTIV.DEALER_NUMBER = DEAL.DEALER_NUMBER
                                WHERE 
                                	DEAL.SSV_FLAG = 'X'
                                AND 
                                	DEAL.COUNTRY = 'US'
                                AND 
                                    ACTIV.DEALER_STATUS = 'ACTIVE RETAIL DEALER'
                                AND 
                                    DEAL.[STATE] NOT IN ('AK', 'AP', 'HI', 'GU', 'VI', 'PR', 'AS', 'DC')
    
                                """
                              ,cnxn, parse_dates=["DEALER_INCEPTION_DATE"], index_col = "DEALER_INCEPTION_DATE"
                             )
            
            
            # Set columns datatype 
            df["DEALER_NUMBER"] = df["DEALER_NUMBER"].astype(str)
            df["CITY"] = df["CITY"].astype(str)
            df["STATE"] = df["STATE"].astype(str).str.strip()
            df["COUNTRY"] = df["COUNTRY"].astype(str)
            df["DEALER_STATUS"] = df["DEALER_STATUS"].astype(str)
            df["SKIDOO_FLAG"] = df["SKIDOO_FLAG"].astype(int)
            df["SEADOO_FLAG"] = df["SEADOO_FLAG"].astype(int)
            df["SPORTBOAT_FLAG"] = df["SPORTBOAT_FLAG"].astype(int)
            df["ATV_FLAG"] = df["ATV_FLAG"].astype(int)
            df["NEV_FLAG"] = df["NEV_FLAG"].astype(int)
            df["ROADSTER_FLAG"] = df["ROADSTER_FLAG"].astype(int)
            df["SSV_FLAG"] = df["SSV_FLAG"].astype(int)
            
       
            #Print task completion time: 
            print("Gathering data from: Dealers | completion time: " + str(pd.Timestamp.now() - startTime))
            
        if target == 'meteo':
        
            # Start task timer
            startTime = pd.Timestamp.now()
            
    
            print("Gathering from: dbo.MKTPL_US_METEO")
            df = pd.read_sql("""
                                SELECT 
                                	STATE_ID,
                                	YEAR([DATE]) AS REF_YEAR,
                                	MONTH([DATE]) AS REF_MONTH,
                                	AVG(PRCP) AS AVG_PRCP,
                                    SUM(PRCP) AS TOT_PRCP,
                                	AVG(TAVG) AS TAVG,
                                	MAX(TMAX) AS TMAX,
                                    AVG(TMAX) AS AVG_TMAX,
                                	MIN(TMIN) AS TMIN,
                                    AVG(TMIN) AS AVG_TMIN
                                FROM 
                                	Market_Planning.dbo.MKTPL_US_METEO
                                GROUP BY
                                	STATE_ID,
                                	YEAR([DATE]),
                                	MONTH([DATE])
                                ORDER BY
                                	REF_YEAR,
                                	REF_MONTH,
                                    STATE_ID
    
                                """
                              ,cnxn
                             )
            
            # Create a date column that will be used as an index
            df["DATE"] = pd.to_datetime(df["REF_YEAR"].astype(str)+"-"+df["REF_MONTH"].astype(str))
            # Set the index as DATE
            df = df.set_index("DATE")
            
            
            df["STATE_ID"] = df["STATE_ID"].astype(str)
            df["REF_YEAR"] = df["REF_YEAR"].astype(str)
            df["REF_MONTH"] = df["REF_MONTH"].astype(str)
            
            
            #Print task completion time: 
            print("Gathering from: dbo.MKTPL_US_METEO completion time: " + str(pd.Timestamp.now() - startTime))
            
        if target == 'ind':
            
            # Start task timer
            startTime = pd.Timestamp.now()
    
            print("Gathering data from: Industry")
            
            df = pd.read_sql("""
                                SELECT	
                                	DISTINCT(IND.Transaction_code) as TRANSACTION_CODE,
                                	IND.Division AS DIVISION,
                                    CAST(EOMONTH(CAST(IND.Year_Code AS varchar) + '-' + CAST(IND.Month_Code AS varchar) + '-' + CAST(1 AS varchar)) as datetime) as RETAIL_DATE,
                                	TRIM(IND.Manufacturer_Code) AS MANUFACTURER_CODE,
                                	UPPER(CODE.MANUFACTURER_NAME) as MANUFACTURER_NAME,
                                	IND.Model_Code AS MODEL_CODE,
                                	IND.Country_Code AS COUNTRY_CODE,
                                	TRIM(IND.State_Code) AS STATE_ID,
                                	IND.Postal_Code AS POSTAL_CODE,
                                	IND.Retailed_Units AS RETAILED_UNITS,
                                	IND.Model_Year_Motorcycle AS MODEL_YEAR
                                FROM 
                                	Sales_Library.dbo.SL_MARKET_SALES_IND_UPLOAD IND
                                LEFT JOIN 
                                    NetDev.dbo.GLOBAL_MANUFACTURER_CODE CODE
                                    ON CODE.MANUFACTURER_CODE_2 = IND.Manufacturer_Code
                                WHERE 
                                	IND.Division = 91
                                AND 
                                	IND.Country_Code = 'US'
                                AND 
                                    IND.State_Code NOT IN ('AK', 'AP', 'HI', 'GU', 'VI', 'PR', 'AS', 'DC')
                                ORDER BY Retail_Date ASC
    
                                """
                              ,cnxn ,parse_dates=["RETAIL_DATE"], index_col = "RETAIL_DATE"
                             )
            
            
            # Set columns datatype 
            df["TRANSACTION_CODE"] = df["TRANSACTION_CODE"].astype(str)
            df["DIVISION"] = df["DIVISION"].astype(str)
            df["MANUFACTURER_CODE"] = df["MANUFACTURER_CODE"].astype(str)
            df["MANUFACTURER_NAME"] = df["MANUFACTURER_NAME"].astype(str)
            df["MODEL_CODE"] = df["MODEL_CODE"].astype(str)
            df["COUNTRY_CODE"] = df["COUNTRY_CODE"].astype(str)
            df["STATE_ID"] = df["STATE_ID"].astype(str).str.strip()
            df["POSTAL_CODE"] = df["POSTAL_CODE"].astype(str)
            df["RETAILED_UNITS"] = df["RETAILED_UNITS"].astype(str)
            df["MODEL_YEAR"] = df["MODEL_YEAR"].astype(str)
            
       
            #Print task completion time: 
            print("Gathering data from: Industry | completion time: " + str(pd.Timestamp.now() - startTime))
            
        if target == "leads":
            
            # Start task timer
            startTime = pd.Timestamp.now()
    
            print("Gathering data from: Leads")
            
            df = pd.read_sql("""
                                SELECT *
                                FROM Market_Planning.dbo.MKTPL_LEADS_2010_to_2020_CLEANED
                                WHERE STATE_ID NOT IN ('DC')
    
                                """
                              ,cnxn, parse_dates=["LEAD_DATE"], index_col = "LEAD_DATE"
                             )
            
            
            # Set columns datatype 
            df["LEAD_ID"] = df["LEAD_ID"].astype(str)
            df["BRAND"] = df["BRAND"].astype(str)
            df["LEAD_CATEGORY"] = df["LEAD_CATEGORY"].astype(str)
            df["ZIP_CD"] = df["ZIP_CD"].astype(str).str.strip()
            df["STATE_ID"] = df["STATE_ID"].astype(str).str.strip()
            df["COUNTRY"] = df["COUNTRY"].astype(str)
            df["BUSINESS_NAME"] = df["BUSINESS_NAME"].astype(str)
            df["CONVERSION_INDICATOR"] = df["CONVERSION_INDICATOR"].astype(int)
           
       
            #Print task completion time: 
            print("Gathering data from: Leads | completion time: " + str(pd.Timestamp.now() - startTime)) 

        if target == "geolocal":
            
            # Start task timer
            startTime = pd.Timestamp.now()
    
            print("Gathering data from: GEOLOCAL")
            
            df = pd.read_sql("""
                                SELECT
                                     [CUST_DISTANCE_OWN]
                                      ,[DATE]
                                      ,[OWNER_NUMBER]
                                      ,[OWNER_POSTAL]
                                      ,[OWNER_STATE]
                                      ,[AVG_DISTANCE_C]
                                      ,[AVG_DISTANCE_D]
                                      ,[AVG_DISTANCE_T]
                                      ,[CLOSEST_URBAN_DISTANCE]
                                      ,[CLOSEST_URBAN_STATE]
                                      ,[CD1_DISTANCE]
                                      ,[CD1_POSTAL]
                                      ,[CD1_STATE]
                                      ,[CD2_DISTANCE]
                                      ,[CD2_POSTAL]
                                      ,[CD2_STATE]
                                      ,[CD3_DISTANCE]
                                      ,[CD3_POSTAL]
                                      ,[CD3_STATE]
                                      ,[CD4_DISTANCE]
                                      ,[CD4_POSTAL]
                                      ,[CD4_STATE]
                                      ,[CD5_DISTANCE]
                                      ,[CD5_POSTAL]
                                      ,[CD5_STATE]
                                      ,[DD1_DISTANCE]
                                      ,[DD1_POSTAL]
                                      ,[DD1_STATE]
                                      ,[DD2_DISTANCE]
                                      ,[DD2_POSTAL]
                                      ,[DD2_STATE]
                                      ,[DD3_DISTANCE]
                                      ,[DD3_POSTAL]
                                      ,[DD3_STATE]
                                      ,[DD4_DISTANCE]
                                      ,[DD4_POSTAL]
                                      ,[DD4_STATE]
                                      ,[DD5_DISTANCE]
                                      ,[DD5_POSTAL]
                                      ,[DD5_STATE]
                                      ,[TD1_DISTANCE]
                                      ,[TD1_POSTAL]
                                      ,[TD1_STATE]
                                      ,[TD2_DISTANCE]
                                      ,[TD2_POSTAL]
                                      ,[TD2_STATE]
                                      ,[TD3_DISTANCE]
                                      ,[TD3_POSTAL]
                                      ,[TD3_STATE]
                                FROM Market_Planning.dbo.MKTPL_GEOLOCAL_DATA
                                WHERE OWNER_STATE NOT IN ('AK', 'HI', 'GU', 'VI', 'PR', 'AS', 'DC')
                                ORDER BY 'DATE'
    
                                """
                              ,cnxn, parse_dates=["DATE"], index_col = "DATE"
                             )
            df["OWNER_STATE"] = df["OWNER_STATE"].astype(str).str.strip()
            #Print task completion time: 
            print("Gathering data from: GEOLOCAL | completion time: " + str(pd.Timestamp.now() - startTime))
            
    
        return df
    
    
    #imports data from csv file and returns it in dataframe format
    #columns must be a list of column names in string format
    def import_csv(self, path, columns = None):
        #import the data
        df = pd.read_csv(path)
    
        #returns only specified columns
        if columns != None:
            df = df[columns]
        return df
    
    
        
    #this function returns raw data from either sql or CSV
    def get_data(self, source, table_name, path = None, Server = 'cavlsqlpd2\pbi2', Database = None, columns = None):
        '''
        Parameters
        ----------
        source : STR
            csv if you are getting it from CSV
            sql if you are getting the data from sql
        table_name : STR
            Table name if in sql or file name if in csv. Remember to add .csv at the end of the csv file
        path : STR
            path of the csv file, defaults to none if using sql
        Server : STR
            SQL server name, defaults to none if using csv
        Database : STR
            Databse name, defaults to none if using csv
        columns : LIST <STR>
            list of the desired columns to keep, if left blank retrieves all columns
                
        Return
        ---------
        df : Pandas Dataframe
            Returns pandas dataframe containing the desired parameters
        '''
            
        #error management
        if type(source) != type('str'):
            print('Please enter a valid source type')
            return
        if type(table_name) != type('str'):
            print('Please enter a valid table name format')
            return
        if type(path) != type('str') and path != None:
            print('Please enter a valid path format')
            return
        if type(Server) != type('str') and Server != None:
            print('Please enter a valid Server Format')
            return
        if type(Database) != type('str') and Database != None:
            print('Please enter a valid database format')
            return
        if type(columns) != type([]) and columns != None:
            print('Please enter a valid column format, hint its an array of strings')
            return
        
            
        #Gets CSV data and returns it into pandas dataframe
        if source.upper() == 'CSV':
            df = self.import_csv(path+'/'+table_name, columns)
            return df
            
        #Gets SQL data and returns it into pandas dataframe
        elif source.upper() == 'SQL':
            #Connects to SQL Server
            cnxn = pc.connect('DRIVER={SQL Server};SERVER='+Server+';DATABASE='+Database+';Trusted_Connection=yes;')
            
            #If columns arent specified returns all the columns
            if columns == None:
                df = pd.read_sql(f'''
                                 SELECT *
                                 FROM {table_name}
                                 '''
                                 ,cnxn)
            #Returns only specified columns
            else:
                select_names = ''
                for name in columns:
                    select_names = select_names +'['+name+'], '
                select_names = select_names[:-2]
                df = pd.read_sql(f'''
                                 SELECT {select_names}
                                 FROM {table_name}
                                 '''
                                 ,cnxn)
            return df
        #Error management
        else:
            print('Please enter a valid source type')
            return
            

