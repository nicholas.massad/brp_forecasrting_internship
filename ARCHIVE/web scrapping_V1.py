# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 08:59:02 2021

@author: bernida2
"""

import requests

from bs4 import BeautifulSoup

import pandas as pd

import uszipcode as uzp

# Connect to the SQL server
import pyodbc as pc


# List of all the state names
states_name = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii"
               , "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine","Maryland", "Massachusetts", "Michigan"
               , "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New_Hampshire", "New_Jersey", "New_Mexico", "New_York"
               , "North_Carolina", "North_Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode_Island", "South_Carolina", "South_Dakota"
               , "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West_Virginia", "Wisconsin", "Wyoming"]

# Transform in lower case
states_name = [state.lower() for state in states_name]



# List of all the state id (2 letters)
states_id = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV"
             ,"NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]

# Transform states_id in lower case
states_id = [state.lower() for state in states_id]

# Transform states_name and states_id into a dictionnary
states_dict = {states_name[i] : states_id[i] for i in range(len(states_name))}

def get_park_url():
    
    # Add to the dictionnary the number of pages 
    # For each state, there are different number of pages with details on tracks
    print("Getting the number of pages for each states")
    for i in states_dict:
        # Get the url of the choosen state
        URL = "https://www.riderplanet-usa.com/atv/trails/"+i+"_list.htm"
        
        page = requests.get(URL)
        
        soup = BeautifulSoup(page.content,"html.parser")
        
        # Get the number of pages
        results_headers = soup.find(id = "idstateheader_"+states_dict.get(i))
    
        state_nbr_page = results_headers.find("span")
        last_page = state_nbr_page.contents[0][-2:][0]
        
        # Store the info into the dictionnary states_dict
        states_dict[i] = [states_dict[i],last_page]
    
    print("Getting URL for each parks")
    for state in states_dict:
        page = states_dict[state][1]
        for p in range(1,int(page)+1): 
            # Get the park URL on the first page
            if p == 1 :
                URL = "https://www.riderplanet-usa.com/atv/trails/"+state+"_list.htm"
                page = requests.get(URL)
                soup = BeautifulSoup(page.content,"html.parser")
                results_parks = soup.find(id = "idstatelist_"+states_dict[state][0])
                parks_url_page = results_parks.find_all("a", class_ = "clsrastitle")
                
                parks_url_list = []            
                for i in range(len(parks_url_page)):
                    # href gives the park URL
                    parks_url_list.append(parks_url_page[i]["href"])

            # Get the park URL for subsequent pages (over 1)
            else:
                URL = "https://www.riderplanet-usa.com/atv/trails/"+state+"_list_p"+str(p)+".htm"
                page = requests.get(URL)
                soup = BeautifulSoup(page.content,"html.parser")
                results_parks = soup.find(id = "idstatelist_"+states_dict[state][0])
                parks_url_page = results_parks.find_all("a", class_ = "clsrastitle")
                        
                for i in range(len(parks_url_page)):
                    # href gives the park URL
                    parks_url_list.append(parks_url_page[i]["href"])
              
        states_dict[state].append(parks_url_list)
        
        print(str(len(states_dict[state][2]))+" parks listed for "+state)
        
    return 

get_park_url()





def gather_data():

    # Create column names
    column_names = ["NAME", "OTHER_NAMES", "STATE", "ZIP_CODE", "NEAREST_CITY", "ADDRESS","LAST_KNOW_STATUS"
                    , "PERMIT_REQUIRED", "MAX_V_WIDTH", "MOTORCYCLES_&_DIRT_BIKES", "ATV", "UTV_&_SXS"
                    , "SUV_&_JEEPS", "DUNE_BUGGY_&_SAND_RAILS", "OWNERS", "FEES", "APPROX_SIZE", "TERRAIN_TYPE", "ELEVATION","URL"]
        
    # Create a dataframe to store the info
    parks_df = pd.DataFrame(columns = column_names)    
    
    search = uzp.SearchEngine()
    
    for state in states_dict:
        print("Working on: "+state)
        for park_url in states_dict[state][2]:
            print(park_url)
            # Get the right URL
            URL = "https://www.riderplanet-usa.com/atv/trails/"+park_url
            
            page = requests.get(URL)
            
            soup = BeautifulSoup(page.content,"html.parser")

            # Get park name
            name = soup.find("h1", class_ = "clsblockheader clsmainheader clspagetitle").text
            
            # Get park other names
            other_names = soup.find("p", class_ = "clsaltnamestext").text
            other_names = other_names[:100]
            
            status = soup.find(text = "Last Known Status").findNext("td").text
            
            # Get permit requirement
            permit = soup.find(text = "Permit Required").findNext("td").text
            
            # Get Maximum vehicle width
            max_width = soup.find(text = "Maximum Vehicle Width").findNext("td").text
            
            # Get Motorcycle & Dirt Bikes permission
            m_and_db = soup.find("img", alt = "Allow Dirt Bikes, Trail Bikes and Motorcycles").findNext("td").text
            
            # Get ATV permission
            atv = soup.find("img", alt = "Allow All Terrain Vehicles / Quads / Four Wheelers").findNext("td").text
            
            # Get UTV and SSV permission
            ssv = soup.find("img", alt = "Allow Utility Terrain Vehicles, Side by Sides (SXS), RZR").findNext("td").text
            
            # Get UTV & SXS permission
            suv_jeeps = soup.find("img", alt = "Allow 4x4 Jeeps, SUVs and 4 x 4 Trucks with License Plates").findNext("td").text
            
            try:
                # Get Dune Buggies permission
                db_and_sr = soup.find(text = "Dune Buggy & Sand Rail").findNext("td").text
            except:
                db_and_sr = soup.find(text = "Dune Buggy & Swamp Buggy").findNext("td").text
                
            # Get land owners
            owners = soup.find(text = "Land").findNext("td").text
            
            try:
                # Get entry fees
                fees = soup.find(text = "Entry").findNext("td").text
            except:
                fees = soup.find(text = "Riding").findNext("td").text
            
            try :
                # Get land size
                approx_size = soup.find(text = "Approx Size").findNext("td").text
            except:
                approx_size = None
            
            try:
                # Get terrain type
                terrain_type = soup.find(text = "Climate Type").findNext("td").text
            except: 
                terrain_type = None
            
            try:
                # Get terrain elevation
                elevation = soup.find(text = "Elevation").findNext("td").text
            except:
                elevation = None
            
            try:
                # Get nearest city
                city_state = soup.find(text = "Nearest City").findNext("td").text.split(",")
                
                near_city = city_state[0]
            except:
                near_city = None
            
            try: 
                # Get adress
                full_address = soup.find(text = "Address").findNext("td").text.split(",")
                address = full_address[0]
            except :
                address = None
            
            near_state = states_dict[state][0].upper()
            
            try :
                if pd.isna(address):
                    zipcode = search.by_city_and_state(near_city, near_city)
                    zip_code = zipcode[0].zipcode
                else:
                    # Get Postal code
                    zip_code = full_address[-1][-5:]
            except:
                zip_code = None
            
            # Store all infos in a list
            info_list = [name, other_names, near_state, zip_code, near_city, address, status, permit, max_width, m_and_db, atv, ssv, suv_jeeps, db_and_sr, owners, fees, approx_size, terrain_type, elevation, URL]
            
            # Create a serie to store info into dataframe
            info_list_serie = pd.Series(info_list, index = column_names)
            
            # Append infos to the dataframe
            parks_df = parks_df.append(info_list_serie, ignore_index = True)
    
    return parks_df

df = gather_data()


# Define the SQL server
SQL_server = "cavlsqlpd2\pbi2"
# Database
Database = "Market_Planning"

# Staging = "dbo.MKTPL_Z_US_METEO"

Target = "dbo.MKTPL_US_TRAILS"

print("Connecting to server: "+SQL_server)
print("Connecting to database: " +Database)    
# Connection data to SQL server
cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';DATABASE='+Database+';Trusted_Connection=yes;')
cursor = cnxn.cursor()


print("Dropping table: "+Target)
cursor.execute("DROP TABLE IF EXISTS "+Target)
cursor.commit()
print("Creating table: "+Target)
cursor.execute(f"""
               CREATE TABLE 
                   {Target} (
                       [NAME]                           nvarchar(100)    NOT NULL, 
                       [OTHER_NAMES]                    nvarchar(100), 
                       [STATE]                          nvarchar(2)      NOT NULL, 
                       [ZIP_CODE]                       int, 
                       [NEAREST_CITY]                   nvarchar(100), 
                       [ADRESS]                         nvarchar(100), 
                       [LAST_KNOW_STATUS]               nvarchar(150), 
                       [PERMIT_REQUIRED]                nvarchar(8), 
                       [MAX_V_WIDTH]                    nvarchar(15), 
                       [MOTORCYCLES_AND_DIRT_BIKES]     nvarchar(15), 
                       [ATV]                            nvarchar(15), 
                       [SSV_AND_USV]                    nvarchar(15), 
                       [SUV_AND_JEEPS]                  nvarchar(15), 
                       [DUNE_BUGGY_AND_SAND_RAILS]      nvarchar(15), 
                       [OWNERS]                         nvarchar(100), 
                       [FEES]                           nvarchar(300), 
                       [APPROX_SIZE]                    nvarchar(100),
                       [TERRAIN_TYPE]                   nvarchar(100), 
                       [ELEVATION]                      nvarchar(20),
                       [URL]                            nvarchar(100)    NOT NULL
                       );
                 """
                 )           
cursor.commit()


# Start task timer
# startTime = datetime.now()

print("Transforming dataframe into list")
data_list = df.values.tolist() 

print("Inserting data into SQL")
cursor.fast_executemany = True
cursor.executemany("INSERT INTO " + Target + " ([NAME], [OTHER_NAMES], [STATE], [ZIP_CODE], [NEAREST_CITY], [ADRESS], [LAST_KNOW_STATUS], [PERMIT_REQUIRED], [MAX_V_WIDTH], [MOTORCYCLES_AND_DIRT_BIKES], [ATV],[SSV_AND_USV], [SUV_AND_JEEPS], [DUNE_BUGGY_AND_SAND_RAILS], [OWNERS], [FEES], [APPROX_SIZE], [TERRAIN_TYPE], [ELEVATION], [URL]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", data_list)
print(f'{len(data_list)} rows inserted to the {Target} table')
cursor.commit()
        
#Print task completion time: 
# print("Inserting data into SQL for " +str(y)+" completion time: " + str(datetime.now() - startTime))

cursor.close()










