# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
# import matplotlib.pyplot as plt
# import seaborn as sns

# import scipy.stats as stats
# import statsmodels
# import statsmodels.api as sm
# import statsmodels.formula.api as smf

from my_functions import registrations, dfmanipulations

# Connect to the SQL server
import pyodbc as pc


# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()

# =====================================================
# ----- Data Importation
# =====================================================

# Dataset in SQL
US_METEO = "dbo.MKTPL_US_METEO"
US_TRAILS = "dbo.MKTPL_US_TRAILS"
VEHICLE_REGISTRATION = "dbo.MKTPL_ORIGINAL_OWNER"



def import_from_sql(target):
    
    
    # Define the SQL server
    SQL_server = "cavlsqlpd2\pbi2"
    # Databases
    Database = "Market_Planning"
    
    print("Connecting to server: "+SQL_server)
    print("Connecting to database: " +Database)   
            
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';DATABASE='+Database+';Trusted_Connection=yes;')
    
    if target == VEHICLE_REGISTRATION:
        
        # Start task timer
        startTime = pd.Timestamp.now()
        
        # Dataset in SQL
        target = VEHICLE_REGISTRATION

        print("Gathering from: "+ target)
        df = pd.read_sql(f"""
                            SELECT
                            	Division,
                            	Model_number,
                            	Usage_code,
                            	Owner_No,
                            	LEFT(Postal_Code,5) as Postal_Code,
                            	City,
                            	Region,
                            	Registration_Date,
                            	CUSTOMER_NUMBER,
                            	REG_DEALER_NUMBER
                            FROM 
                                {target}
                            WHERE 
                                	Country = 'US' 
                                AND Division = '91' 
                                AND Registration_Date >= '2012-01-01' 
                                AND Region <> '' 
                                AND Region NOT IN ('AK', 'HI', 'GU', 'VI', 'PR', 'AS')
                            ORDER BY 
                            	Registration_Date
                            """
                          ,cnxn ,parse_dates=["Registration_Date"], index_col = "Registration_Date"
                         )
        
        
        # Set columns datatype 
        df["Division"] = df["Division"].astype(int)
        df["Model_number"] = df["Model_number"].astype(str)
        df["Usage_code"] = df["Usage_code"].astype(str)
        df["Owner_No"] = df["Owner_No"].astype(str)
        df["Postal_Code"] = df["Postal_Code"].astype(str)
        df["City"] = df["City"].astype(str)
        df["Region"] = df["Region"].astype(str).str.strip()
        df["CUSTOMER_NUMBER"] = df["CUSTOMER_NUMBER"].astype(str)
        df["REG_DEALER_NUMBER"] = df["REG_DEALER_NUMBER"].astype(str)
     
        
        #Print task completion time: 
        print("Gathering from: "+ target+" completion time: " + str(pd.Timestamp.now() - startTime))
    
    if target == US_METEO:
        
        # Start task timer
        startTime = pd.Timestamp.now()
        
        # Dataset in SQL
        target = US_METEO

        print("Gathering from: "+ target)
        df = pd.read_sql(f"""
                            SELECT 
                            	STATE_ID,
                            	YEAR([DATE]) AS REF_YEAR,
                            	MONTH([DATE]) AS REF_MONTH,
                            	AVG(PRCP) AS AVG_PRCP,
                                SUM(PRCP) AS TOT_PRCP,
                            	AVG(TAVG) AS TAVG,
                            	MAX(TMAX) AS TMAX,
                                AVG(TMAX) AS AVG_TMAX,
                            	MIN(TMIN) AS TMIN,
                                AVG(TMIN) AS AVG_TMIN
                            FROM 
                            	{target}
                            GROUP BY
                            	STATE_ID,
                            	YEAR([DATE]),
                            	MONTH([DATE])
                            ORDER BY
                            	REF_YEAR,
                            	REF_MONTH,
                                STATE_ID

                            """
                          ,cnxn
                         )
        
        # Create a date column that will be used as an index
        df["DATE"] = pd.to_datetime(df["REF_YEAR"].astype(str)+"-"+df["REF_MONTH"].astype(str))
        # Set the index as DATE
        df = df.set_index("DATE")
        
        
        df["STATE_ID"] = df["STATE_ID"].astype(str)
        df["REF_YEAR"] = df["REF_YEAR"].astype(str)
        df["REF_MONTH"] = df["REF_MONTH"].astype(str)
        
        
        #Print task completion time: 
        print("Gathering from: "+ target+" completion time: " + str(pd.Timestamp.now() - startTime))

    if target == US_TRAILS:
        
        # Start task timer
        startTime = pd.Timestamp.now()
        
        # Dataset in SQL
        target = US_TRAILS

        print("Gathering from: "+ target)
        df = pd.read_sql(f"""
                            SELECT
                            	*
                            FROM 
                                {target}

                            """
                          ,cnxn
                         )
       
        #Print task completion time: 
        print("Gathering from: "+ target+" completion time: " + str(pd.Timestamp.now() - startTime))
    return df


# Put data from SQL into dataframes

# Get all the vehicle registrations
VehicleRegistration = import_from_sql(VEHICLE_REGISTRATION)

# Get all the meteorological data
USMeteo = import_from_sql(US_METEO)

# Get all the us trails data
USTrails = import_from_sql(US_TRAILS)



# =====================================================
# ----- Correlation analysis
# =====================================================

# Get meteo data by date and state
USMeteo_Serie = USMeteo.groupby("STATE_ID").resample("M").mean()


# Get registrations by date and state
VehicleRegistration_Serie = reg.registrations_count(VehicleRegistration, freq = "M", column = "Region")
VehicleRegistration_Serie.index.names = ["STATE_ID", "DATE"]
VehicleRegistration_Serie = pd.DataFrame(VehicleRegistration_Serie, columns=["COUNT"])

# Create a main dataframe 
df = VehicleRegistration_Serie.merge(USMeteo_Serie, how = "left", on = ["STATE_ID", "DATE"])


test1 = df[(df.index.get_level_values("DATE").month >= 5) & (df.index.get_level_values("DATE").month < 10)]

test2 = df[(df.index.get_level_values("DATE").month < 5) | (df.index.get_level_values("DATE").month >= 10)]

test1.hist()
test2.hist()

results = mani.corr_results(df, "STATE_ID", graphs=False)

results1 = mani.corr_results(test1, "STATE_ID", graphs=True)

results2 = mani.corr_results(test2, "STATE_ID", graphs=False)



# Get the correlation and p-values for different lags
# lag négatif veut dire que pour une date de référence, on regarderait dans le futur

# results_minus_1 = mani.corr_results(mani.lag_columns(df, "STATE_ID", -1),"STATE_ID", graphs=False)
# results_minus_2 = mani.corr_results(mani.lag_columns(df, "STATE_ID", -2),"STATE_ID", graphs=False)
# results_minus_3 = mani.corr_results(mani.lag_columns(df, "STATE_ID", -3),"STATE_ID", graphs=False)

# # lag positif veut dire qu'on regarderait dans le passé
# results_plus_1 = mani.corr_results(mani.lag_columns(df, "STATE_ID", 1),"STATE_ID", graphs=False)
# results_plus_2 = mani.corr_results(mani.lag_columns(df, "STATE_ID", 2),"STATE_ID", graphs=False)
# results_plus_3 = mani.corr_results(mani.lag_columns(df, "STATE_ID", 3),"STATE_ID", graphs=False)



