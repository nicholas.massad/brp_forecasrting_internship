# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# import scipy.stats as stats
# from statsmodels.graphics.factorplots import interaction_plot
import statsmodels.api as sm
# import statsmodels.formula.api as smf

from our_functions import registrations, dfmanipulations, ImpExp

# Connect to the SQL server
import pyodbc as pc


# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

# =====================================================
# ----- Data Importation
# =====================================================

# Dataset in SQL
VEHICLE_REGISTRATION = "VR"
LEADS = "leads"


# Put data from SQL into dataframes
# Get all the vehicle registrations
VehicleRegistration = imp.import_predefined_from_sql(VEHICLE_REGISTRATION, min_date = "2010-01-01")

# Get all the industry data
LeadsRetail = imp.import_predefined_from_sql(LEADS)


# =====================================================
# ----- Data Preprocessing
# =====================================================

# Get BRP registrations by date and state
VehicleRegistration_Serie = reg.count_to_df(VehicleRegistration, freq = "M", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["BRP_COUNT"])


# Get total leads by state and date
LeadsTotal = reg.count_to_df(LeadsRetail, freq = "M", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["LEAD_TOTAL"])

# Get converted leads by state and dates
LeadsConverted = reg.count_to_df(LeadsRetail[LeadsRetail["CONVERSION_INDICATOR"].astype(int) == 1], freq = "M", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["LEAD_CONVERTED"])

# Create a dataframe in order to analyse convertions
df = VehicleRegistration_Serie.merge(LeadsConverted, how = "left", on = ["STATE_ID", "DATE"])
df = df.merge(LeadsTotal, how = "left", on = ["STATE_ID", "DATE"])

df["BRP_COUNT_LAGGED"] = 0
df["LEAD_TOTAL_AVG_2Y"] = 0
#df["LEAD_TOTAL_AVG_2Y_LAGGED"] = 0
for state in df.index.get_level_values("STATE_ID").unique():
    mask = df.index.get_level_values("STATE_ID") == state
    df["BRP_COUNT_LAGGED"][mask] = df["BRP_COUNT"][mask].shift(-12)
    df["LEAD_TOTAL_AVG_2Y"][mask] = (df["LEAD_TOTAL"][mask] + df["LEAD_TOTAL"][mask].shift(12))/2


df = reg.merge_categories(df, "STATE_ID", LeadsRetail, "LEAD_CATEGORY", rename_src_col = "LEAD_TOTAL")

df = reg.merge_categories(df, "STATE_ID", LeadsRetail[LeadsRetail["CONVERSION_INDICATOR"].astype(int) == 1], "LEAD_CATEGORY" ,rename_src_col = "LEAD_CONVERTED")


# Create a dataframe without extreme values
df3 = pd.DataFrame()

col = "LEAD_TOTAL"
#col = "LEAD_TOTAL_AVG_2Y"
#col = "LEAD_TOTAL_REQUEST A QUOTE"
for state in df.index.get_level_values("STATE_ID").unique():
    df2 = df.copy()
    df2 = df2[df2.index.get_level_values("STATE_ID") == state]
    
    mask = df2[col][df2.index.get_level_values("STATE_ID") == state]
    df2.drop(df2[mask > mask.mean()+(3*mask.std())].index, inplace =True)
    
    df3 = df3.append(df2)





# # We can see that there is almost no leads in 2015 and 2016.
# # This impact correlation a lot, if we use more recent years, the correlation improves a ton.
# reg.ts_plot(LeadsRetail, list_freq = "M", column = "STATE_ID", list_rows = "ALL")

# reg.seasonal_plot(LeadsRetail, list_freq = "M", column = "STATE_ID", list_rows = "ALL", palette = "bright", sequence = True)



# =====================================================
# ----- Correlation Analysis with extreme values
# =====================================================

# Exclude year 2021
df = df[df.index.get_level_values("DATE") < "2019-01-01"]
df["LEAD_TOTAL"] = df["LEAD_TOTAL"].apply(np.log)
df = df[df["LEAD_TOTAL"] > 0]


results_df = mani.corr_results(df[["BRP_COUNT_LAGGED", "LEAD_TOTAL"]].dropna(), "STATE_ID", graphs=False)
stats_df = results_df[(results_df["P_LEAD_TOTAL"] < 0.025) & (results_df["CORR_LEAD_TOTAL"] > 0.6)]
stats_df.count()
stats_df.max()


# Very good results
results_2y_df = mani.corr_results(df[["BRP_COUNT_LAGGED", "LEAD_TOTAL_AVG_2Y"]].dropna(), "STATE_ID", graphs=True)
stats_df = results_2y_df[(results_2y_df["P_LEAD_TOTAL_AVG_2Y"] < 0.025) & (results_2y_df["CORR_LEAD_TOTAL_AVG_2Y"] > 0.5)]
stats_df.count()
stats_df.max()


# =====================================================
# ----- Correlation Analysis without extreme values
# =====================================================


# Exclude year 2021
df3 = df3[df3.index.get_level_values("DATE") < "2019-01-01"]
df3["LEAD_TOTAL"] = df3["LEAD_TOTAL"].apply(np.log)
df3 = df3[df3["LEAD_TOTAL"] > 0]


results_df3 = mani.corr_results(df3[["BRP_COUNT_LAGGED", "LEAD_TOTAL"]].dropna(), "STATE_ID", graphs=False)
stats_df3 = results_df3[(results_df3["P_LEAD_TOTAL"] < 0.025) & (results_df3["CORR_LEAD_TOTAL"] > 0.6)]
stats_df3.count()
stats_df3.max()


# Very good results
results_2y_df3 = mani.corr_results(df3[["BRP_COUNT_LAGGED", "LEAD_TOTAL_AVG_2Y"]].dropna(), "STATE_ID", graphs=True)
stats_df3 = results_2y_df3[(results_2y_df3["P_LEAD_TOTAL_AVG_2Y"] < 0.025) & (results_2y_df3["CORR_LEAD_TOTAL_AVG_2Y"] > 0.5)]
stats_df3.count()
stats_df3.max()




# =====================================================
# ----- Density plot
# =====================================================

for state in df.index.get_level_values("STATE_ID").unique():
    dense = df["BRP_COUNT"][df.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.show()

for state in df.index.get_level_values("STATE_ID").unique():
    dense = df["LEAD_TOTAL"][df.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.show()
