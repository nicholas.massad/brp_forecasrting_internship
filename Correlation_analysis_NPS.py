# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 10:23:58 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
# import matplotlib.pyplot as plt
# import seaborn as sns
import scipy.stats as stats
# import scipy.stats as stats
# import statsmodels
# import statsmodels.api as sm
# import statsmodels.formula.api as smf

from my_functions import registrations, dfmanipulations

# Connect to the SQL server
import pyodbc as pc


# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

# Call functions
reg = registrations()
mani = dfmanipulations()

# =====================================================
# ----- Data Importation
# =====================================================


def import_from_sql():
    
    
    # Define the SQL server
    SQL_server = "cavlsqlpd2\pbi2"
    
    print("Connecting to server: "+SQL_server)
            
    # Connection data to SQL server
    cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';Trusted_Connection=yes;')
        
    # Start task timer
    startTime = pd.Timestamp.now()

    # 
    df = pd.read_sql(f"""
                        SELECT
                            ORIGIN.Division,
                        	ORIGIN.Serial_number,
                            ORIGIN.Model_number,
                            ORIGIN.Usage_code,
                        	ORIGIN.Owner_No,
                        	ORIGIN.Postal_Code,
                            ORIGIN.City,
                            ORIGIN.Region,
                            ORIGIN.CUSTOMER_NUMBER,
                        	ORIGIN.Registration_Date,
                        	ORIGIN.REG_DEALER_NUMBER,
                        	NPS.Survey_Score,
                            NPS.ResponseDate
                        FROM
                        	Market_Planning.dbo.MKTPL_ORIGINAL_OWNER ORIGIN
                        LEFT JOIN 
                        	NetDev.dbo.CSI_SURVEY_RESPONSES	SURVEY
                        ON	SURVEY.VIN__c = ORIGIN.Serial_number
                        LEFT JOIN 
                        	NetDev.dbo.NPS_GLOBAL_DATA NPS
                        ON	NPS.Survey_ID = SURVEY.[Name]
                        
                        WHERE
                        	ORIGIN.DIVISION = 91
                        AND	ORIGIN.Country = 'US'
                        AND ORIGIN.Registration_Date > '2015-07-08'
                        AND ORIGIN.Region <> '' 
                        AND ORIGIN.Region NOT IN ('AK', 'HI', 'GU', 'VI', 'PR', 'AS')
                        """
                      ,cnxn ,parse_dates=["Registration_Date"], index_col = "Registration_Date"
                     )
        
        
    # Set columns datatype 
    df["Division"] = df["Division"].astype(int)
    df["Model_number"] = df["Model_number"].astype(str)
    df["Usage_code"] = df["Usage_code"].astype(str)
    df["Owner_No"] = df["Owner_No"].astype(str)
    df["Postal_Code"] = df["Postal_Code"].astype(str)
    df["City"] = df["City"].astype(str)
    df["Region"] = df["Region"].astype(str).str.strip()
    df["CUSTOMER_NUMBER"] = df["CUSTOMER_NUMBER"].astype(str)
    df["REG_DEALER_NUMBER"] = df["REG_DEALER_NUMBER"].astype(str)
     
        
    #Print task completion time: 
    print("Completion time: " + str(pd.Timestamp.now() - startTime))
  
    return df

Surveys = import_from_sql()


# Surveys["ResponseLag"] = Surveys["ResponseDate"] - Surveys.index.get_level_values("Registration_Date")
# pd.to_timedelta(Surveys["ResponseLag"]).mean()
# max(Surveys["ResponseLag"])

# test = Surveys["ResponseDate"].dropna(how = "any", axis = 0)
# test= pd.DataFrame(test)

# test["ResponseLag"] = test["ResponseDate"] - test.index.get_level_values("Registration_Date")
# pd.to_timedelta(test["ResponseLag"]).mean()
# max(test["ResponseLag"])



# Surveys = Surveys.sort_index(axis = 0, level = "Registration_Date")
# Surveys_test = Surveys[Surveys["Survey_Score"].notnull()]

# VehicleRegistration_Serie = reg.registrations_count(Surveys_test, freq = "M", column = "REG_DEALER_NUMBER")
# VehicleRegistration_Serie.index.names = ["STATE_ID", "DATE"]
# VehicleRegistration_Serie = pd.DataFrame(VehicleRegistration_Serie, columns=["COUNT"])

# Surveys_Serie = Surveys_test.groupby("REG_DEALER_NUMBER").resample("M").mean()
# Surveys_Serie = Surveys_Serie["Survey_Score"]
# Surveys_Serie.index.names = ["STATE_ID", "DATE"]
# Surveys["ResponseDate"].dropna(how = "any", axis = 0)

# df = VehicleRegistration_Serie.merge(Surveys_Serie, how = "left", on = ["STATE_ID", "DATE"])

# df = df[df["Survey_Score"].notnull()]

# results = mani.corr_results(df, "STATE_ID", graphs=False)
# df = df.sort_values("COUNT", ascending = False)

# df = df[df["COUNT"]>=5]

# corr = stats.pearsonr(df["COUNT"],df["Survey_Score"])






