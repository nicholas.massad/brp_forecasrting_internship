# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 16:16:06 2021

@author: bernida2
"""

import pandas as pd

# Connect to the SQL server
import pyodbc as pc

filePath = "C:/Users/bernida2/Desktop/brp_raw/trails_data.csv"

# Download the dataframe
df = pd.read_csv(filePath, usecols=range(1,21), keep_default_na=False)

# Replace empty cells by None value
for i in df[df.columns]:
    for j in range(len(df[i])):
        if df[i][j] == "":
            df[i][j] = None
            


# Define the SQL server
SQL_server = "cavlsqlpd2\pbi2"
# Database
Database = "Market_Planning"

# Staging = "dbo.MKTPL_Z_US_METEO"

Target = "dbo.MKTPL_US_TRAILS"

print("Connecting to server: "+SQL_server)
print("Connecting to database: " +Database)    
# Connection data to SQL server
cnxn = pc.connect('DRIVER={SQL Server};SERVER='+SQL_server+';DATABASE='+Database+';Trusted_Connection=yes;')
cursor = cnxn.cursor()


print("Dropping table: "+Target)
cursor.execute("DROP TABLE IF EXISTS "+Target)
cursor.commit()
print("Creating table: "+Target)
cursor.execute(f"""
               CREATE TABLE 
                   {Target} (
                       [NAME]                           varchar(55)    NOT NULL, 
                       [OTHER_NAMES]                    nvarchar(100), 
                       [STATE]                          varchar(2)      NOT NULL, 
                       [ZIP_CODE]                       int, 
                       [NEAREST_CITY]                   varchar(50), 
                       [ADRESS]                         nvarchar(100), 
                       [LAST_KNOW_STATUS]               nvarchar(150), 
                       [PERMIT_REQUIRED]                nvarchar(8), 
                       [MAX_V_WIDTH]                    nvarchar(15), 
                       [MOTORCYCLES_AND_DIRT_BIKES]     nvarchar(15), 
                       [ATV]                            nvarchar(15), 
                       [SSV_AND_USV]                    nvarchar(15), 
                       [SUV_AND_JEEPS]                  nvarchar(15), 
                       [DUNE_BUGGY_AND_SAND_RAILS]      nvarchar(15), 
                       [OWNERS]                         nvarchar(100), 
                       [FEES]                           nvarchar(300), 
                       [APPROX_SIZE]                    nvarchar(100),
                       [TERRAIN_TYPE]                   nvarchar(100), 
                       [ELEVATION]                      nvarchar(20),
                       [URL]                            nvarchar(100)    NOT NULL
                       );
                 """
                 )           
cursor.commit()


# Start task timer
# startTime = datetime.now()

print("Transforming dataframe into list")
data_list = df.values.tolist() 

print("Inserting data into SQL")
cursor.fast_executemany = True
cursor.executemany("INSERT INTO " + Target + " ([NAME], [OTHER_NAMES], [STATE], [ZIP_CODE], [NEAREST_CITY], [ADRESS], [LAST_KNOW_STATUS], [PERMIT_REQUIRED], [MAX_V_WIDTH], [MOTORCYCLES_AND_DIRT_BIKES], [ATV],[SSV_AND_USV], [SUV_AND_JEEPS], [DUNE_BUGGY_AND_SAND_RAILS], [OWNERS], [FEES], [APPROX_SIZE], [TERRAIN_TYPE], [ELEVATION], [URL]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", data_list)
print(f'{len(data_list)} rows inserted to the {Target} table')
cursor.commit()
        
#Print task completion time: 
# print("Inserting data into SQL for " +str(y)+" completion time: " + str(datetime.now() - startTime))

cursor.close()


