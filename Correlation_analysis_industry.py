# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:11:17 2021

@author: bernida2
"""


# =====================================================
# ----- Librairies
# =====================================================

# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# import scipy.stats as stats
# from statsmodels.graphics.factorplots import interaction_plot
import statsmodels.api as sm
# import statsmodels.formula.api as smf

from our_functions import registrations, dfmanipulations, ImpExp

# =====================================================
# ----- Generic settings
# =====================================================

# Show all columns
pd.set_option('display.max_columns', None)

reg = registrations()
mani = dfmanipulations()
imp = ImpExp()

# =====================================================
# ----- Data Importation
# =====================================================

# Dataset in SQL
INDUSTRY = "ind"
VEHICLE_REGISTRATION = "VR"
DEALERS = "deal"


# Put data from SQL into dataframes

# Get all the vehicle registrations
VehicleRegistration = imp.import_predefined_from_sql(VEHICLE_REGISTRATION, min_date = "2015-01-01")

# Get all the industry data
IndustryRetail = imp.import_predefined_from_sql(INDUSTRY)

# Get active dealer infos
DealerInfo = imp.import_predefined_from_sql(DEALERS)



# =====================================================
# ----- Data Preprocessing
# =====================================================

# Get BRP registrations by date and state
VehicleRegistration_Serie = reg.count_to_df(VehicleRegistration, freq = "M", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["BRP_COUNT"])


# Get industry registrations by date and state
IndustryRetail_Serie = reg.count_to_df(IndustryRetail, freq = "M", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["INDUSTRY_COUNT"])


# Get dealers per state
Dealer_Serie = reg.count_to_df(DealerInfo, freq = "M", on_col = "STATE", index = ["STATE_ID", "DATE"], col = ["DEALER_COUNT"])


# Create a list of every states in the us
list_states = IndustryRetail["STATE_ID"].unique()
list_states.sort()



# Get the dealer count per state
for state in Dealer_Serie.index.get_level_values("STATE_ID").unique():
    z = Dealer_Serie[Dealer_Serie.index.get_level_values("STATE_ID") == state]
    for i in range(len(z.values)):
        if z.values[i][0] != 0:
            z.values[i][0] = z.values[i-1][0] + z.values[i][0]
        else: 
            z.values[i][0] = z.values[i-1][0]
    Dealer_Serie[Dealer_Serie.index.get_level_values("STATE_ID") == state] = z

# Deal with exeptions in the dealer count
for state in Dealer_Serie.index.get_level_values("STATE_ID").unique():
    z = Dealer_Serie[Dealer_Serie.index.get_level_values("STATE_ID") == state]
    if max(z.index.get_level_values("DATE")) < pd.to_datetime("2015-01-31"):
        to_append = z[z.index.get_level_values("DATE") == max(z.index.get_level_values("DATE"))].values[0][0]
        to_append = pd.DataFrame({"STATE_ID" : state, "DATE" : pd.to_datetime("2015-01-31"), "DEALER_COUNT" : [to_append]})
        to_append = to_append.set_index(["STATE_ID", "DATE"])
        Dealer_Serie = Dealer_Serie.append(to_append)


# Merge both series into a dataframe
df = IndustryRetail_Serie.merge(VehicleRegistration_Serie, how = "left", on = ["STATE_ID", "DATE"])
df = df.merge(Dealer_Serie, how = "left", on = ["STATE_ID", "DATE"])


# Nan values treatment for dealer_count
for state in df.index.get_level_values("STATE_ID").unique():
    z = df[df.index.get_level_values("STATE_ID") == state]["DEALER_COUNT"]
    for i in range(len(z.values)):
        if pd.isna(z.values[i]) == True:
            z.values[i] = z.values[i-1]
        else: 
            z.values[i] = z.values[i]
    for dates in z.index.get_level_values("DATE"):
        df.at[(df.index.get_level_values("STATE_ID") == state)&(df.index.get_level_values("DATE") == dates),"DEALER_COUNT"] = z.loc[(z.index.get_level_values("STATE_ID") == state) & (z.index.get_level_values("DATE") == dates)]


# Get the BRPs market share
df["MARKET_SHARE"] = df["BRP_COUNT"].divide(df["INDUSTRY_COUNT"])

# Get the number of units per dealer
df["UNITS_PER_DEALER"] = df["BRP_COUNT"].divide(df["DEALER_COUNT"])

df = df.fillna(0)

# Set up a second dataframe for data PRE-COVID
df2 = df[df.index.get_level_values("DATE") < "2020-01-01"]





# =====================================================
# ----- General industry plots
# =====================================================


sns.set_palette(sns.color_palette("bright"))


colors = {"Industry" : "blue", "KAWASAKI" : "limegreen", "ARCTIC CAT" : "red", "POLARIS" : "darkkhaki", "BRP" : "black", "HONDA" : "slategrey", "YAMAHA" : "violet"}


company = IndustryRetail["MANUFACTURER_NAME"].unique()
company = ["BRP", "POLARIS", "HONDA"]

# Get industry overview with COVID
# Trend = moving average on past 6 months
industry = reg.registrations_count(IndustryRetail, freq = "M")
decomposition = sm.tsa.seasonal_decompose(industry, model = "additive")
industry.plot(label = "Industry", c= "blue")
decomposition.trend.plot(label = "Industry_trend", c = "blue", linestyle = "--")
for cie in company:
    state = reg.registrations_count(IndustryRetail[IndustryRetail["MANUFACTURER_NAME"] == cie], freq = "M")
    decomposition = sm.tsa.seasonal_decompose(state, model = "additive")
    state.plot(label = cie, c = colors[cie])
    decomposition.trend.plot(label = cie + "_trend", c = colors[cie], linestyle = "--")
plt.legend(bbox_to_anchor = (1.05,1))
plt.show()

# Get industry by STATE overview with COVID
for states in list_states : 
    industry = reg.registrations_count(IndustryRetail[IndustryRetail["STATE_ID"] == states], freq = "M")
    decomposition = sm.tsa.seasonal_decompose(industry, model = "additive")
    ax = industry.plot(label = "Industry", c= "blue")
    ax_trend = decomposition.trend.plot(label = "Industry_trend", c = "blue", linestyle = "--")
    for cie in company:
        state = reg.registrations_count(IndustryRetail[(IndustryRetail["MANUFACTURER_NAME"] == cie) & (IndustryRetail["STATE_ID"] == states)], freq = "M")
        decomposition = sm.tsa.seasonal_decompose(state, model = "additive")
        ax = state.plot(label = cie, c = colors[cie])
        ax_trend = decomposition.trend.plot(label = cie + "_trend", c = colors[cie], linestyle = "--")
    plt.legend(bbox_to_anchor = (1.05,1))
    plt.title("State : " +states)
    plt.show()
    
    
# Get industry by STATE overview with COVID
for states in list_states : 
    industry = reg.registrations_count(IndustryRetail[IndustryRetail["STATE_ID"] == states], freq = "M")
    decomposition = sm.tsa.seasonal_decompose(industry, model = "additive")
    ax = industry.plot(label = "Industry", c= "blue")
    ax_trend = decomposition.trend.plot(label = "Industry_trend", c = "blue", linestyle = "--")
    for cie in company:
        state = reg.registrations_count(IndustryRetail[(IndustryRetail["MANUFACTURER_NAME"] == cie) & (IndustryRetail["STATE_ID"] == states)], freq = "M")
        decomposition = sm.tsa.seasonal_decompose(state, model = "additive")
        ax = state.plot(label = cie, c = colors[cie])
        ax_trend = decomposition.trend.plot(label = cie + "_trend", c = colors[cie], linestyle = "--")
    # Get the dealer count
    test = df["DEALER_COUNT"][df.index.get_level_values("STATE_ID") == states]
    test = test.reset_index("STATE_ID", drop = True)
    ax2 = test.plot(secondary_y = True, color = "yellow", ax = ax)
    ax2.set_ylim(0,100)
    ax.legend(bbox_to_anchor = (1.05,1))
    ax2.legend(bbox_to_anchor = (1.5,.4))
    plt.title("State : " +states)
    plt.show()



# Get industry overview PRE-COVID
industry = reg.registrations_count(IndustryRetail[IndustryRetail.index.get_level_values("RETAIL_DATE") < "2020-01-01"], freq = "M")
decomposition = sm.tsa.seasonal_decompose(industry, model = "additive")
industry.plot(label = "Industry", c= "blue")
decomposition.trend.plot(label = "Industry_trend", c = "blue", linestyle = "--")
for cie in company:
    state = reg.registrations_count(IndustryRetail[(IndustryRetail["MANUFACTURER_NAME"] == cie) & (IndustryRetail.index.get_level_values("RETAIL_DATE") < "2020-01-01")], freq = "M")
    decomposition = sm.tsa.seasonal_decompose(state, model = "additive")
    state.plot(label = cie, c = colors[cie])
    decomposition.trend.plot(label = cie + "_trend", c = colors[cie], linestyle = "--")
plt.legend(bbox_to_anchor = (1.05,1))
plt.show()




# Get industry by state overview PRE-COVID
for states in list_states : 
    industry = reg.registrations_count(IndustryRetail[(IndustryRetail["STATE_ID"] == states)&(IndustryRetail.index.get_level_values("RETAIL_DATE") < "2020-01-01")], freq = "M")
    decomposition = sm.tsa.seasonal_decompose(industry, model = "additive")
    ax = industry.plot(label = "Industry", c= "blue")
    ax_trend = decomposition.trend.plot(label = "Industry_trend", c = "blue", linestyle = "--")
    for cie in company:
        state = reg.registrations_count(IndustryRetail[(IndustryRetail["MANUFACTURER_NAME"] == cie) & (IndustryRetail["STATE_ID"] == states) &(IndustryRetail.index.get_level_values("RETAIL_DATE") < "2020-01-01")], freq = "M")
        decomposition = sm.tsa.seasonal_decompose(state, model = "additive")
        ax = state.plot(label = cie, c = colors[cie])
        ax_trend = decomposition.trend.plot(label = cie + "_trend", c = colors[cie], linestyle = "--")
    # Get the dealer count
    test = df["DEALER_COUNT"][(df.index.get_level_values("STATE_ID") == states)&(df.index.get_level_values("DATE") < "2020-01-01")]
    test = test.reset_index("STATE_ID", drop = True)
    ax2 = test.plot(secondary_y = True, color = "yellow", label = "Nbr Dealer")
    ax2.set_ylim(0,100)
    ax.legend(bbox_to_anchor = (1.05,1))
    ax2.legend(bbox_to_anchor = (1.36,.4))
    plt.title("State : " +states)
    plt.show()


# =====================================================
# ----- Correlation analysis (all dates)
# =====================================================



test = df[(df.index.get_level_values("STATE_ID") != "DE") & (df.index.get_level_values("STATE_ID") != "DC") & (df.index.get_level_values("STATE_ID") != "RI")]

results_MS = mani.corr_results(test[["MARKET_SHARE", "DEALER_COUNT"]], "STATE_ID",  plot_col = ["DEALER_COUNT"], graphs=True)

df[df.index.get_level_values("STATE_ID") != "DE"]

# =====================================================
# ----- Correlation analysis (before 2020)
# =====================================================

results_MS = mani.corr_results(df2, "STATE_ID",  plot_col = ["MARKET_SHARE"], graphs=True)





# =====================================================
# ----- Growth analysis per year
# =====================================================

def growth_industry_year():
    df = reg.count_to_df(IndustryRetail, freq = "Y", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["INDUSTRY_COUNT"])
    df = df[df.index.get_level_values("DATE") < "2021-01-01"]
    
    for states in list_states : 
        # Get the average growth of the industry in a each state for the past 2 years
        industry = df[df.index.get_level_values("STATE_ID") == states]["INDUSTRY_COUNT"]
        growth = 1+industry.pct_change(periods=1)
        avg_growth = growth.rolling(window = 2, min_periods = 2).mean()
        # Update values in the dataframe
        for dates in industry.index.get_level_values("DATE"):
            mask1 = (df.index.get_level_values("STATE_ID") == states) & (df.index.get_level_values("DATE") == dates)
            mask2 = (avg_growth.index.get_level_values("STATE_ID") == states) & (avg_growth.index.get_level_values("DATE") == dates)
            
            # Lag industry sales by 1 year
            df["INDUSTRY_COUNT_LAGGED"] = df["INDUSTRY_COUNT"].shift(-1)
            
            # Get avg growth for the past 2 years
            df.at[mask1,"INDUSTRY_GROWTH_AVG_2_YEARS"] = avg_growth.loc[mask2]
            
            # Get industry growth year over year
            df.at[mask1,"INDUSTRY_GROWTH_YOY"] = growth.loc[mask2]
            # Lag growth year over year
            df["INDUSTRY_GROWTH_YOY_LAGGED"] = df["INDUSTRY_GROWTH_YOY"].shift(-1)
    return df    
    

def growth_brp_year():
    df = reg.count_to_df(VehicleRegistration, freq = "Y", on_col = "STATE_ID", index = ["STATE_ID", "DATE"], col = ["BRP_COUNT"])
    df = df[df.index.get_level_values("DATE") < "2021-01-01"]
    
    for states in list_states : 
        # Get the average growth of the industry in a each state for the past 2 years
        brp = df[df.index.get_level_values("STATE_ID") == states]["BRP_COUNT"]
        growth = 1+brp.pct_change(periods=1)
        avg_growth = growth.rolling(window = 2, min_periods = 2).mean()
        # Update values in the dataframe
        for dates in brp.index.get_level_values("DATE"):
            mask1 = (df.index.get_level_values("STATE_ID") == states) & (df.index.get_level_values("DATE") == dates)
            mask2 = (avg_growth.index.get_level_values("STATE_ID") == states) & (avg_growth.index.get_level_values("DATE") == dates)
            
            # Lag BRP sales by 1 year
            df["BRP_COUNT_LAGGED"] = df["BRP_COUNT"].shift(-1)
            
            # Get avg growth for the past 2 years
            df.at[mask1,"BRP_GROWTH_AVG_2_YEARS"] = avg_growth.loc[mask2]
            
            # Get BRP growth year over year
            df.at[mask1,"BRP_GROWTH_YOY"] = growth.loc[mask2]
            # Lag growth year over year
            df["BRP_GROWTH_YOY_LAGGED"] = df["BRP_GROWTH_YOY"].shift(-1)
    return df    
    
Industry_Year = growth_industry_year()

BRP_Year = growth_brp_year()



df_Year = BRP_Year.merge(Industry_Year, how = "left", on = ["STATE_ID", "DATE"])


# Il y a des correlations, mais trop peu de points de référence
results_growth1 = mani.corr_results(df_Year[["BRP_GROWTH_YOY_LAGGED", "INDUSTRY_GROWTH_YOY"]].dropna(), "STATE_ID", graphs=True)
results_growth2 = mani.corr_results(df_Year[["BRP_GROWTH_YOY_LAGGED", "INDUSTRY_GROWTH_AVG_2_YEARS"]].dropna(), "STATE_ID", graphs=True)


results_growth2 = mani.corr_results(df_Year[["BRP_GROWTH_YOY_LAGGED", "INDUSTRY_GROWTH_YOY", "INDUSTRY_GROWTH_AVG_2_YEARS"]].dropna(), "STATE_ID", graphs=True)


# Revient à faire une méthode naïve
results_growth_BRP = mani.corr_results(df_Year[["BRP_GROWTH_YOY_LAGGED", "BRP_GROWTH_AVG_2_YEARS"]].dropna(), "STATE_ID", graphs=True)


# =====================================================
# ----- Growth analysis per month per state
# =====================================================

def growth_month():
    
    BRP = VehicleRegistration_Serie[:]
    BRP = BRP[BRP.index.get_level_values("DATE") < "2021-01-01"]
    
    INDUSTRY = IndustryRetail_Serie[:]
    INDUSTRY = INDUSTRY[INDUSTRY.index.get_level_values("DATE") < "2021-01-01"]
    
    
    # Create our dataframe
    df_MONTH = pd.DataFrame()
    
    # BRP sales per month
    df_MONTH["BRP_COUNT"] = BRP["BRP_COUNT"]
    
    # BRP lagged sales per month
    df_MONTH["BRP_COUNT_LAGGED"] = df_MONTH["BRP_COUNT"].shift(-12)
    
    # INDUSTRY sales per month
    df_MONTH["INDUSTRY_COUNT"] = INDUSTRY["INDUSTRY_COUNT"]
    
    for states in list_states : 
        print("Working on : "+states)
        brp = BRP[BRP.index.get_level_values("STATE_ID") == states]["BRP_COUNT"]
        growth_brp = 1+brp.pct_change(periods = 12)
    
            
        industry = INDUSTRY[INDUSTRY.index.get_level_values("STATE_ID") == states]["INDUSTRY_COUNT"]
        growth_industry = 1+industry.pct_change(periods = 12)
    
        # Update values in the dataframe
        for dates in brp.index.get_level_values("DATE"):
            mask1 = (df_MONTH.index.get_level_values("STATE_ID") == states) & (df_MONTH.index.get_level_values("DATE") == dates)
            mask2 = (growth_brp.index.get_level_values("STATE_ID") == states) & (growth_brp.index.get_level_values("DATE") == dates)
            

            # BRP sales growth compared with same month of last year
            df_MONTH.at[mask1, "BRP_GROWTH"] = growth_brp.loc[mask2]
            
            # INDUSTRY sales growth compared with same month of last year
            df_MONTH["BRP_GROWTH_LAGGED"] = df_MONTH["BRP_GROWTH"].shift(-12)
            
            try:
                # INDUSTRY sales growth compared with same month of last year
                df_MONTH.at[mask1, "INDUSTRY_GROWTH"] = growth_industry.loc[mask2]
            except:
                for dates in industry.index.get_level_values("DATE"):
                    mask1 = (df_MONTH.index.get_level_values("STATE_ID") == states) & (df_MONTH.index.get_level_values("DATE") == dates)
                    mask2 = (growth_industry.index.get_level_values("STATE_ID") == states) & (growth_industry.index.get_level_values("DATE") == dates)
                    df_MONTH.at[mask1, "INDUSTRY_GROWTH"] = growth_industry.loc[mask2]
                  
    # Replace infinite values by a Nan
    print("Replacing infinite values by NaNs")
    for col in df_MONTH.columns:
        df_MONTH[col] = df_MONTH[col].replace(np.inf,np.nan)
        
    return df_MONTH


df_MONTH_main = growth_month()

df_MONTH = df_MONTH_main.copy()

df_MONTH = df_MONTH[df_MONTH.index.get_level_values("DATE") < "2019-01-01"]


df_MONTH["BRP_COUNT_LAGGED"] = df_MONTH["BRP_COUNT_LAGGED"].apply(np.log)
df_MONTH = df_MONTH[df_MONTH["BRP_COUNT_LAGGED"]>=0]

# Est-ce que la croissance de l'industrie est indicatrice d'une croissance pour la prochaine année ? 
# Réponse : Pas vraiment
results_growth1 = mani.corr_results(df_MONTH[["BRP_GROWTH_LAGGED", "INDUSTRY_GROWTH"]].dropna(), "STATE_ID", graphs=True)
results_growth1["P_INDUSTRY_GROWTH"][(results_growth1["P_INDUSTRY_GROWTH"]< 0.05)&(abs(results_growth1["CORR_INDUSTRY_GROWTH"]) > 0.5)].count() 


# Est-ce que la croissance de l'industrie est indicatrice des ventes futures de BRP?
# Réponse : Pas vraiment
results_growth1 = mani.corr_results(df_MONTH[["BRP_COUNT_LAGGED", "INDUSTRY_GROWTH"]].dropna(), "STATE_ID", graphs=True)


# Est-ce que les ventes de l'industrie sont correlées avec les ventes de BRP ?
# Réponse : Oui en majorité. Meilleur quand on utilise ln(BRP_COUNT)
results = mani.corr_results(df_MONTH[["BRP_COUNT_LAGGED","INDUSTRY_COUNT"]].dropna(), "STATE_ID", graphs=False)

results["P_INDUSTRY_COUNT"][(results["P_INDUSTRY_COUNT"]< 0.05)&(abs(results["CORR_INDUSTRY_COUNT"]) > 0.6)].count() 





reg.ts_plot(VehicleRegistration, list_freq = "M", column = "STATE_ID", start = None, end = None, style = "line", theme = "darkgrid", palette = "crest", ci = False)

# Aucune correlation si on prend les ventes totales de l'industrie aux US




# =====================================================
# ----- Density plot
# =====================================================

for state in df_MONTH.index.get_level_values("STATE_ID").unique():
    dense = df_MONTH["BRP_COUNT_LAGGED"][df_MONTH.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.show()

for state in df_MONTH.index.get_level_values("STATE_ID").unique():
    dense = df_MONTH["INDUSTRY_GROWTH"][df_MONTH.index.get_level_values("STATE_ID") == state]
    dense.plot.density()
    plt.show()




# def growth_month_overall():
    
#     BRP = reg.registrations_count(VehicleRegistration, freq = "M")
#     BRP = BRP[BRP.index < "2021-01-01"]
    
#     INDUSTRY = reg.registrations_count(IndustryRetail, freq = "M")
#     INDUSTRY = INDUSTRY[INDUSTRY.index < "2021-01-01"]
    
    
#     # Create our dataframe
#     df_MONTH = pd.DataFrame()
    
#     # BRP sales per month
#     df_MONTH["BRP_COUNT"] = BRP
    
#     # INDUSTRY sales per month
#     df_MONTH["INDUSTRY_COUNT"] = INDUSTRY
    

#     #brp = BRP[BRP.index.get_level_values("STATE_ID") == states]["BRP_COUNT"]
#     growth_brp = 1+BRP.pct_change(periods = 12)

        
#     #industry = INDUSTRY[INDUSTRY.index.get_level_values("STATE_ID") == states]["INDUSTRY_COUNT"]
#     growth_industry = 1+INDUSTRY.pct_change(periods = 12)

#     # Update values in the dataframe
#     for dates in BRP.index:
#         mask1 = (df_MONTH.index == dates)
#         mask2 = (growth_brp.index == dates)
        
#         # # When values are NAN or infinite set them to 0
#         # if growth_brp.loc[mask2].isna() or np.isinf(growth_brp.loc[mask2]):
#         #     print("yes")
#         #     df_MONTH.at[mask1, "BRP_GROWTH"] = 0

#         # else:
#         # BRP sales growth compared with same month of last year
#         df_MONTH.at[mask1, "BRP_GROWTH"] = growth_brp.loc[mask2]
        
#         # INDUSTRY sales growth compared with same month of last year
#         df_MONTH["BRP_GROWTH_LAGGED"] = df_MONTH["BRP_GROWTH"].shift(-12)
        
#         try:
#             # INDUSTRY sales growth compared with same month of last year
#             df_MONTH.at[mask1, "INDUSTRY_GROWTH"] = growth_industry.loc[mask2]
#         except:
#             for dates in industry.index.get_level_values("DATE"):
#                 mask1 = (df_MONTH.index == dates)
#                 mask2 = (growth_industry.index== dates)
#                 df_MONTH.at[mask1, "INDUSTRY_GROWTH"] = growth_industry.loc[mask2]
                  
#     # Replace infinite values by a Nan
#     print("Replacing infinite values by NaNs")
#     for col in df_MONTH.columns:
#         df_MONTH[col] = df_MONTH[col].replace(np.inf,np.nan)
#     # df_MONTH["BRP_GROWTH"] = df_MONTH["BRP_GROWTH"].replace(np.inf,np.nan)
#     # df_MONTH["BRP_GROWTH_LAGGED"] = df_MONTH["BRP_GROWTH_LAGGED"].replace(np.inf,np.nan)
#     # df_MONTH["INDUSTRY_GROWTH"] = df_MONTH["INDUSTRY_GROWTH"].replace(np.inf,np.nan)
        
#     return df_MONTH


# df_MONTH = growth_month_overall()

# df_MONTH = df_MONTH.dropna()

# x = df_MONTH["INDUSTRY_GROWTH"] 
# y = df_MONTH["BRP_GROWTH_LAGGED"] 

# corr = stats.pearsonr(x,y)

# sns.scatterplot(x = x, y = y, palette = "crest", color = "black")
# m, b = np.polyfit(x, y, 1)

# plt.plot(x, m*x + b, color = "darkgoldenrod")

# plt.title("Monthly correlation between "+x.name+ " and "+y.name+ "\n Correlation :" + str(corr))

# plt.show()









